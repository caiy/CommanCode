"Bundle Set
set nocompatible " 设置vim为不兼容vi模式  
filetype off " required  
set rtp+=~/.vim/bundle/vundle/  
call vundle#rc()  
"let Vundle manage Vundle  

"Required!
Bundle 'gmarik/vundle'

""Bundles托管插件示例  
"代码在github上,使用github帐号/项目名，例如：  
""Bundle 'tpope/vim-fugitive'  
"代码在vimscript上，使用插件名称，插件名字可以在  
""http://vim-scripts.org/vim/scripts.html页面中查找，例如：  
"Bundle 'vimwiki'  
""非github的插件，使用其git地址，例如：  
"Bundle 'git://git.wincent.com/command-t.git'

"Your Bundles Here
Bundle 'altercation/vim-colors-solarized'	
"皮肤
Bundle 'ervandew/supertab'			
"朴素的自动补全
Bundle 'luochen1990/select-and-search'		
"快速搜索(v模式下选中按n即可搜索)
Bundle 'luochen1990/rainbow'			
"括号高亮插件
Bundle 'godlygeek/tabular'			
"自动对齐
Bundle 'tpope/vim-surround'			
"配对括号同时操作
Bundle 'AutoClose'				
"自动补全括号
Bundle 'taglist.vim'				
"函数树状显示
Bundle 'vim-airline/vim-airline'
"底部状态栏
Bundle 'octol/vim-cpp-enhanced-highlight'
"c++高亮
Bundle 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
"一个好用的目录树
Bundle 'chengzeyi/go-highlight.vim'
au BufNewFile,BufRead *.go setf go
"go highlight

filetype plugin indent on " required! vundle配置结束 
"Brief help  
":BundleList - list configured bundles  
":BundleInstall(!) - install(update) bundles  
":BundleSearch(!) foo - search(or refresh cache first) for foo  
":BundleClean(!) - 删除插件首先要在.vimrc中注释掉插件相应的行，再运行该命令    
"bundles Set End

"Commen Set
syntax enable
set nu 							""行号
set foldenable 					""允许折叠
set cursorline 					""突出显示当前行
set tabstop=4 					""tab长度4
set shiftwidth=4                ""tab长度4
set softtabstop=4               ""tab长度4
set nobackup 					""覆盖文件不备份
set autoindent 					""自动缩进
set showmatch 					""高亮显示匹配括号
set ignorecase 					""搜索忽视大小写
set smartindent 				""C程序自动缩进
set magic 						""启动正则表达式
"paste模式快捷键
:map <F2> :set paste<CR> 
"nopaste模式快捷键
:map <F3> :set nopaste<CR>
"nonu快捷键
map <F4> :set nonu<CR>
"nu快捷键
map <F5> :set nu<CR>
"
map <F6-t> :tabnew
"NerdTree设置
"autocmd vimenter * NerdTree
"启动时自动打开NerdTree
map <F6> :NERDTreeToggle<CR> 
let g:NERDTreeWinSize=20 "设置nerdtree宽度
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"↑用于退出时自动关闭nerdtree


if has("autocmd")
	autocmd FileType python setlocal expandtab
	autocmd FileType cpp setlocal expandtab
endif
"Commen Set End

"Theme Set
""colorscheme molokai

"Vim-airline set
set laststatus=2
set t_Co=256
let g:airline_powerline_fonts = 1

"select-and-search set
let g:select_and_search_active = 1 

"taglist set
let Tlist_Eixt_OnlyWindow=1
let Tlist_Enable_Fold_Column = 0
let Tlist_Show_One_File = 1

"Rainbow Parentheses Improved set
let g:rainbow_active = 1
let g:rainbow_conf = {
    \   'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick'],
    \   'ctermfgs': ['lightblue', 'lightyellow', 'lightcyan', 'lightmagenta'],
    \   'operators': '_,_',
    \   'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold'],
    \   'separately': {
    \       '*': {},
    \       'tex': {
    \           'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/'],
    \       },
    \       'lisp': {
    \           'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick', 'darkorchid3'],
    \       },
    \       'vim': {
    \           'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/', 'start=/{/ end=/}/ fold', 'start=/(/ end=/)/ containedin=vimFuncBody', 'start=/\[/ end=/\]/ containedin=vimFuncBody', 'start=/{/ end=/}/ fold containedin=vimFuncBody'],
    \       },
    \       'html': {
    \           'parentheses': ['start=/\v\<((area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr)[ >])@!\z([-_:a-zA-Z0-9]+)(\s+[-_:a-zA-Z0-9]+(\=("[^"]*"|'."'".'[^'."'".']*'."'".'|[^ '."'".'"><=`]*))?)*\>/ end=#</\z1># fold'],
    \       },
    \       'css': 0,
    \   }
    \}
