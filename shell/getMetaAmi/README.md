# Get the xSection, kFactor, FilterEff informations through the pyami

## Step1

use getXsec.sh to get infos between a mcChannel range. You may need to modify the command code for different production/MC tag/productionType to get your result.

Or more directly, if you know the sample's full name, you could write them in a file, like this one:

```bash
[username@server sampleList]$cat Higgs.txt 
#mc15_14TeV.341122.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH125_tautaull.merge.DAOD_TRUTH3.e5306_p2768
mc15_14TeV.341124.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH125_tautauhh.merge.DAOD_TRUTH3.e5306_p2768
mc15_14TeV.341156.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_tautaulh.merge.DAOD_TRUTH3.e5306_p2768
mc15_14TeV.341157.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_tautauhh.merge.DAOD_TRUTH3.e5306_p2768
mc15_14TeV.341155.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_tautaull.merge.DAOD_TRUTH3.e5306_p2768
```
You could use `#` to comment the sample you don't want to generate now.

The you can use getXsecFromFile.sh to get the infos in the file. You may need to modify the file location or the productionType.
## Step2
cd to 'txtTojson' folder. Make a build dictionary `mkdir build && cd build` and make, `cmake ../ && make` . Make a run dictionary and run the code `mkdir ../run`, `mv selector_ana ../run && cd ../run/`. `./selector_ana signal ${The raw file you generated in step1}`. 

To run the program, the first arguement means you want to generate signal(SUSY points) or backgrounds because the SUSY signal information is kind of different compared with SM backgrounds and you must specific it. The second arguement is the file you generated in step1. Then a metadata json file will be created.

Wainning!! Warnning!! Wainning!! Wainning!! Now the SUSY crossection in the ami is NOT accurate!!! So you should change them to the correct one generated by Resummino! Some of the SUSY metadeta is not right due to the issue in ami. There may be some SUSY MCIDs with multiple metadata information while they are totally arbitrary!! If you find some wierd information, please go to ami webPage to see which is correct one.
