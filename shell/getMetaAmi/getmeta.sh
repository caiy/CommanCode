#before source this scripe one should setup rucio first to get the ami access
source /afs/cern.ch/atlas/software/tools/pyAMI/setup.sh
folder="../sampleList/"
#rm result.log
#for textFile in Diboson.txt DY.txt Higgs.txt Top.txt Z.txt W.txt 
for textFile in W13.txt W13_2.txt
do
	subFile=`egrep "mc15_13TeV" $folder$textFile`
	for file in $subFile
	do
		out=`ami command SearchQuery -sql="SELECT physicsParameterVals.logicalDatasetName, physicsParameterVals.paramName, physicsParameterVals.paramValue FROM dataset,physicsParameterVals WHERE (dataset.logicalDatasetName like \'$file\' AND dataset.identifier = physicsParameterVals.datasetFK )" -project=mc15_001 -processingStep=production | grep -A 1 -E "crossSection|genFiltEff|actor"`
		echo $file $out >> result.log
		echo $file done!
	done
done

#rm insert.txt
#rq treename_regex.C
