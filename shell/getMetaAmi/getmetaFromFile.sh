#before source this scripe one should setup rucio first to get the ami access
folder="../../framework/run2Ditau/sampleList/"
project="mc16_001"

lsetup pyami
rm result.raw
#for textFile in Diboson.txt DY.txt Higgs.txt Top.txt Z.txt W.txt 
for textFile in sig.txt #bkg.txt
do
	subFile=`grep -v "#" $folder$textFile | egrep "mc16_13TeV"`
	for file in $subFile
	do
		out=`ami command SearchQuery -sql="SELECT physicsParameterVals.logicalDatasetName, physicsParameterVals.paramName, physicsParameterVals.paramValue FROM dataset,physicsParameterVals WHERE (dataset.logicalDatasetName like \'$file\' AND dataset.identifier = physicsParameterVals.datasetFK )" -project=$project -processingStep=production | grep -A 1 -E "crossSection|genFiltEff|actor"`
		echo $file $out >> result.raw
		echo $file done!
	done
done

#rm insert.txt
#rq treename_regex.C
