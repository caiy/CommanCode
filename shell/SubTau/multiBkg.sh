path1="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/SubTauRaw/mc/nominal/"
files1=`ls $path1|egrep ".*36110.*|.*410501.*"`

[ -e tmp ] || mkfifo tmp
exec 9<>tmp

for i in {1..6}
do
	echo >&9
done

for file in $files1
do
	read -u9
	{
	hadd "$file".root "$path1""$file"/*root*
	echo >&9
	}&
done
wait
rm -rf tmp
exec 9<&-
exec 9>&-
echo "rename samples"
mv user.lixia.lhtnp.mc16_13TeV.361108.PoPy8_Ztt.P3.e3601_s3126_r9364_r9315_p3195.cnom-v02_hist.root Ztt.root
mv user.lixia.lhtnp.mc16_13TeV.410501.PoPy8_A14_ttbar_hdamp258p75_nonallhad.P3.e5458_s3126_r9364_r9315_p3195.cnom-v02_hist.root ttbar.root
echo "hadd Zll....."
hadd Zll.root *361106*root *361107*root
rm *361106*root *361107*root
echo "hadd W......."
hadd W.root *36110*.root
echo "cleanning...."
rm *36110*root
echo "All success!"
