i=1
for file in $*
do
	echo $file
	if [[ ${file} =~ ".tex" ]];then
		TempFile="THISISTEMPFILE_"$file
		name=${TempFile/.tex}
		newName=${file/.tex}

		echo "Start Writing a new temp file named $TempFile"
		cp $file $TempFile
		sed -i '1i\\\begin{document}' $TempFile
		sed -i '1i\\\setlength\\textwidth{13.5cm}' $TempFile
		sed -i '1i\\\usepackage{graphicx,url, color}' $TempFile
		sed -i '1i\\\usepackage{booktabs, amsmath}' $TempFile
		sed -i '1i\\\documentclass[20pt]{article}' $TempFile
		sed -i '$a\\\end{document}' $TempFile

		echo "Start Converting $TempFile"
		pdflatex $TempFile
		mv "$name".pdf "$newName".pdf

		echo "Cleanning Up"
		rm "$name".*
		let i++
	fi
done

if [ $i -eq 1 ]; then
	echo "texToPdf: missing file operand"
	echo 'texToPdf: Convert tex files without "\begin{document} and "\end{document} in file to a pdf file. Only will process files end with .tex"'
	echo 'Usage: texToPdf [targetFiles].'
fi
