#define MetaInfo_cxx
#include "MetaInfo.h"
#include "PhyUtils.h"
#include "Utils.h"
#include <math.h>
#include <regex>
#include <vector>
#include <map>
#include <fstream>
#include <TSystem.h>
#include <TMath.h>
#include <iomanip>
#include <string>
#include <sstream>
#include <algorithm>
#include <TFile.h>
#include <TROOT.h>
#include <TStyle.h>
#include <cmath>
#include "json.hpp"

using namespace std;
using json = nlohmann::json;


void maincut(string file, int compaign);

json jOut;

void MetaInfo::Loop(int compaign)
{
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntries();

   Long64_t nbytes = 0, nb = 0;

	bool isSig = (compaign % 10) != 0;
	cout << isSig << endl;
	string MCcompaign = to_string(compaign / 10);

   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      if (0 == jentry%100000)
      	cout << jentry << " entry of " << nentries << "entries" << endl;
	
	  nb = fChain->GetEntry(jentry);   nbytes += nb;
	  
	  if(!isSig && (ProcessID != 0)) continue;
	  if(isSig && !(ProcessID == 206 || ProcessID == 207 || ProcessID == 125 || ProcessID == 127 || ProcessID == 157)) continue;
	  string number = to_string(mcChannelNumber);

	  if(!isSig){
		  bool hasBkgValue = !(jOut["Nnorm"]["0"][number][MCcompaign].empty());
		  if(!hasBkgValue){
			jOut["xSection"]["0"][number] = xSection;
			jOut["kFactor"][number] = kFactor;
			jOut["filterEff"][number] = FilterEfficiency;
			jOut["Nnorm"]["0"][number][MCcompaign] = TotalSumW;
		}else{
			double OldSumW = jOut["Nnorm"]["0"][number][MCcompaign];
			jOut["Nnorm"]["0"][number][MCcompaign] = TotalSumW + OldSumW;
		}
	  }else{
		  bool hasSigValue206 = !(jOut["Nnorm"]["206"][number][MCcompaign].empty());
		  bool hasSigValue207 = !(jOut["Nnorm"]["207"][number][MCcompaign].empty());
		  bool hasSigValue125 = !(jOut["Nnorm"]["125"][number][MCcompaign].empty());
		  bool hasSigValue127 = !(jOut["Nnorm"]["127"][number][MCcompaign].empty());
		  bool hasSigValue157 = !(jOut["Nnorm"]["157"][number][MCcompaign].empty());
		if(ProcessID == 206){
			if(!hasSigValue206){
				jOut["xSection"]["206"][number] = xSection;
				jOut["kFactor"][number] = kFactor;
				jOut["filterEff"][number] = FilterEfficiency;
				jOut["Nnorm"]["206"][number][MCcompaign] = TotalSumW;
			}else{
				double OldSumW = jOut["Nnorm"]["206"][number][MCcompaign];
				jOut["Nnorm"]["206"][number][MCcompaign] = TotalSumW + OldSumW;
			}
		}else if(ProcessID == 207){
			if(!hasSigValue207){
				jOut["xSection"]["207"][number] = xSection;
				jOut["kFactor"][number] = kFactor;
				jOut["filterEff"][number] = FilterEfficiency;
				jOut["Nnorm"]["207"][number][MCcompaign] = TotalSumW;
			}else{
				double OldSumW = jOut["Nnorm"]["207"][number][MCcompaign];
				jOut["Nnorm"]["207"][number][MCcompaign] = TotalSumW + OldSumW;
			}
		}else if(ProcessID == 125){
			if(!hasSigValue125){
				jOut["xSection"]["125"][number] = xSection;
				jOut["kFactor"][number] = kFactor;
				jOut["filterEff"][number] = FilterEfficiency;
				jOut["Nnorm"]["125"][number][MCcompaign] = TotalSumW;
			}else{
				double OldSumW = jOut["Nnorm"]["125"][number][MCcompaign];
				jOut["Nnorm"]["125"][number][MCcompaign] = TotalSumW + OldSumW;
			}
		}else if(ProcessID == 127){
			if(!hasSigValue127){
				jOut["xSection"]["127"][number] = xSection;
				jOut["kFactor"][number] = kFactor;
				jOut["filterEff"][number] = FilterEfficiency;
				jOut["Nnorm"]["127"][number][MCcompaign] = TotalSumW;
			}else{
				double OldSumW = jOut["Nnorm"]["127"][number][MCcompaign];
				jOut["Nnorm"]["127"][number][MCcompaign] = TotalSumW + OldSumW;
			}
		}else if(ProcessID == 157){
			if(!hasSigValue157){
				jOut["xSection"]["157"][number] = xSection;
				jOut["kFactor"][number] = kFactor;
				jOut["filterEff"][number] = FilterEfficiency;
				jOut["Nnorm"]["157"][number][MCcompaign] = TotalSumW;
			}else{
				double OldSumW = jOut["Nnorm"]["157"][number][MCcompaign];
				jOut["Nnorm"]["157"][number][MCcompaign] = TotalSumW + OldSumW;
			}
	    }
	  }
   }
}

int MainProcess(int argc, char* argv[])
{
    cout<<"*******************************Start**********************************"<<endl;

	string flag = argv[1];
	int Addvalue = 0;
	if(flag.find("sig") != string::npos){
		Addvalue = 1;
	}
	string pathDATA = argv[2];

	//compaign: 160 means mc16a background. 181 means mc16e signal
	vector<string> filesa = Utils::getFiles(regex(".*16a.*\\.root.*"),pathDATA); 
	for(auto file : filesa){
		maincut(file, 160 + Addvalue);
	}
	vector<string> filesd = Utils::getFiles(regex(".*16d.*\\.root.*"),pathDATA); 
	for(auto file : filesd){
		maincut(file, 170 + Addvalue);
	}
	vector<string> filese = Utils::getFiles(regex(".*16e.*\\.root.*"),pathDATA); 
	for(auto file : filese){
		maincut(file, 180 + Addvalue);
	}

	std::ofstream o(flag + "metadata.json");
	o << std::setw(4) << jOut << std::endl;

    std::cout << "******************E N D****************************************" << std::endl;

    return 0;
}

void maincut(string filename, int compaign)
{
	std::string chainname = "MetaDataTree";
	TChain *chain = new TChain(chainname.c_str());
	chain->Add(filename.c_str());

	//  /publicfs/atlas/atlasnew/SUSY/users/chenghj/rootfile/2tau/18DSv01.4/data15.root   -->   data15.root
	vector<string> newfileVec = Utils::splitStrBy(filename,'/');
	string newfile = newfileVec.back();

	MetaInfo* analysis = new MetaInfo(chain);

	std::cout << "**********Start cutting " << newfile << "*********" << std::endl;
	analysis->Loop(compaign);
	std::cout << "**********End of cutting " << newfile << "********" <<std::endl;

	delete analysis;
	delete chain;
}

