path1="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/Nom/Bkg/"
path2="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/Nom/DS/"
path3="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/Nom/C1C1/"
path4="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/Nom/C1N2/"
path5="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/Nom/Wh/"

path6="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/QCD/Bkg/"
path7="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/QCD/DS/"
path8="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/QCD/C1C1/"
path9="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/QCD/C1N2/"
path10="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/QCD/Wh/"

cd build/
rm -r *
cmake ../
make clean
make -j 10
mv mini_analysis ../run/
cd ../run/

./mini_analysis 1 $path1
./mini_analysis sig2 $path2
./mini_analysis sig3 $path3
./mini_analysis sig4 $path4
./mini_analysis sig5 $path5
./mini_analysis 6 $path6
./mini_analysis sig7 $path7
./mini_analysis sig8 $path8
./mini_analysis sig9 $path9
./mini_analysis sig10 $path10
cd ../
