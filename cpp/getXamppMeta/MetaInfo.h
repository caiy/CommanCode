//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Oct 24 19:36:01 2018 by ROOT version 6.10/04
// from TTree MetaDataTree/MetaData Tree for Small Analysis Ntuples
// found on file: /publicfs/atlas/atlasnew/SUSY/users/chenghj/rootfile/2tau/18DSv01.4/sigmc16a/StauStau_200_1.root
//////////////////////////////////////////////////////////

#ifndef MetaInfo_h
#define MetaInfo_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class MetaInfo {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Bool_t          isData;
   UInt_t          mcChannelNumber;
   UInt_t          runNumber;
   UInt_t          ProcessID;
   Double_t        xSection;
   Double_t        kFactor;
   Double_t        FilterEfficiency;
   Long64_t        TotalEvents;
   Double_t        TotalSumW;
   Double_t        TotalSumW2;
   Long64_t        ProcessedEvents;
   Double_t        prwLuminosity;

   // List of branches
   TBranch        *b_isData;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_ProcessID;   //!
   TBranch        *b_xSection;   //!
   TBranch        *b_kFactor;   //!
   TBranch        *b_FilterEfficiency;   //!
   TBranch        *b_TotalEvents;   //!
   TBranch        *b_TotalSumW;   //!
   TBranch        *b_TotalSumW2;   //!
   TBranch        *b_ProcessedEvents;   //!
   TBranch        *b_prwLuminosity;   //!

   MetaInfo(TTree *tree=0);
   virtual ~MetaInfo();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(int compaign);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef MetaInfo_cxx
MetaInfo::MetaInfo(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/publicfs/atlas/atlasnew/SUSY/users/chenghj/rootfile/2tau/18DSv01.4/sigmc16a/StauStau_200_1.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/publicfs/atlas/atlasnew/SUSY/users/chenghj/rootfile/2tau/18DSv01.4/sigmc16a/StauStau_200_1.root");
      }
      f->GetObject("MetaDataTree",tree);

   }
   Init(tree);
}

MetaInfo::~MetaInfo()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t MetaInfo::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t MetaInfo::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void MetaInfo::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("isData", &isData, &b_isData);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("ProcessID", &ProcessID, &b_ProcessID);
   fChain->SetBranchAddress("xSection", &xSection, &b_xSection);
   fChain->SetBranchAddress("kFactor", &kFactor, &b_kFactor);
   fChain->SetBranchAddress("FilterEfficiency", &FilterEfficiency, &b_FilterEfficiency);
   fChain->SetBranchAddress("TotalEvents", &TotalEvents, &b_TotalEvents);
   fChain->SetBranchAddress("TotalSumW", &TotalSumW, &b_TotalSumW);
   fChain->SetBranchAddress("TotalSumW2", &TotalSumW2, &b_TotalSumW2);
   fChain->SetBranchAddress("ProcessedEvents", &ProcessedEvents, &b_ProcessedEvents);
   fChain->SetBranchAddress("prwLuminosity", &prwLuminosity, &b_prwLuminosity);
   Notify();
}

Bool_t MetaInfo::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void MetaInfo::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t MetaInfo::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef MetaInfo_cxx
