#include "json.hpp"
#include "Utils.h"
#include <fstream>

using namespace std;
using json = nlohmann::json;


bool isSig;

void mergeJsonFile(std::string jsonFile, json& out)
{
    std::ifstream cFile(jsonFile);
    if(out.empty()){
        cFile >> out;
    } else{
        json tmp;
        cFile >> tmp;
        out.merge_patch(tmp);
    }   
}


int MainProcess(int argc, char* argv[])
{
	if(argc == 2){
		std::string filenames = argv[1];
		if(!(filenames == "-h" || filenames == "--help")){
			vector<string> files = Utils::splitStrBy(filenames,','); 
			if(files.size() > 0){
				json out;
				for(auto file : files){
					mergeJsonFile(file, out);
				}
				std::ofstream o("merge.json");
				o << std::setw(4) << out << std::endl;
				cout << "The output \"merge.json\" is created." <<endl;
			}
			return 0;
		}
	}
	cout << "Usage: ./mergeJson.x [Comma splitted files]" << endl
		<< "Merge json files into one file." << endl 
		<< "One could merge multiple files splitted them using comma, like \"file1,file2,file3...\"." << endl
		<< "These json files will be merged to one file \"merge.json\" using Json Merge Patch method. " << endl
		<< "BE CARE!!! In Json Merge Patch method, if there are two or more json files with the same final leaf key, the value of that key will be overridden" <<endl;
	return 0;
}

