#pragma once
#include <TChain.h>
#include <TFile.h>
#include <string>
#include <vector>
#include <iostream>
#include <glog/logging.h>


class RooClassMaker
{
public:
	RooClassMaker(std::string out = "selector");
	RooClassMaker(std::string filename, std::string out = "selector");
	RooClassMaker(std::string filename, std::string treename, std::string out = "selector");
	void InitLOG();
	int MakeClass();
	~RooClassMaker();

private:
	std::string treeName;
	std::vector<std::string> filename;
	std::string outName;
};

inline void RooClassMaker::InitLOG(){
	google::InitGoogleLogging("RooClassMaker");
	FLAGS_log_dir = ".\\LOG\\";
	google::SetLogDestination(google::GLOG_INFO, ".\\LOG\\RooClassMaker");
	google::SetLogFilenameExtension(".log");
	google::SetStderrLogging(google::GLOG_INFO);
	FLAGS_colorlogtostderr = true;
}

RooClassMaker::RooClassMaker(std::string out) {
	InitLOG();
	this->outName = out;
}

RooClassMaker::RooClassMaker(std::string filename, std::string out) {
	InitLOG();
	this->filename.emplace_back(filename);
	this->outName = out;
}

RooClassMaker::RooClassMaker(std::string filename, std::string treename, std::string out)
{
	InitLOG();
	this->filename.emplace_back(filename);
	this->treeName = treename;
	this->outName = out;
}

int RooClassMaker::MakeClass() {
	if (filename.empty()) {
		LOG(FATAL) << "No input root file! PLease use RooClassMaker->AddFile(\"fileName\") to add a file!";
		return -1;
	}else if(treeName.empty()){
		LOG(FATAL) << "No tree is selected! PLease use RooClassMaker->SetTree(\"treeName\") to select the input tree!";
		return -2;
	}
	TChain *chain = new TChain(treeName.c_str());
	for (auto file : filename )
	{
		chain->Add(file.c_str());
	}
	if (outName == "selector")
	{
		LOG(INFO) << "No output file name is choosed or you use the default name. Use the default name \"selector\"";
	}
	chain->MakeClass(outName.c_str());
	return 0;
}

RooClassMaker::~RooClassMaker()
{
	google::ShutdownGoogleLogging();
}
