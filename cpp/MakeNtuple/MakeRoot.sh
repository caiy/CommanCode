source Compile.sh
wait
#-------------------------run/R21--------------------------
filepath=/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/SubTau/V3/

###------------------------nocut------------------------
nohup ./tau_analysis -t TauNoCut -m signal -o run/R21/nocut/Ztt.root -i "$filepath"Ztt.root > run/R21/nocut/Ztt.log &
nohup ./tau_analysis -t TauNoCut -m signal -o run/R21/nocut/Top.root -i "$filepath"ttbar.root > run/R21/nocut/Top.log &
nohup ./tau_analysis -t TauNoCut -o run/R21/nocut/W.root -i "$filepath"W.root > run/R21/nocut/W.log &
nohup ./tau_analysis -t TauNoCut -o run/R21/nocut/Zll.root -i "$filepath"Zll.root > run/R21/nocut/Zll.log &
nohup ./tau_analysis -t TauNoCut -m data -o run/R21/nocut/data.root -i "$filepath"data.root > run/R21/nocut/data.log &

##------------------------loose------------------------
nohup ./tau_analysis -t TauLoose -m signal -o run/R21/loose/Ztt.root -i "$filepath"Ztt.root > run/R21/loose/Ztt.log &
nohup ./tau_analysis -t TauLoose -m signal -o run/R21/loose/Top.root -i "$filepath"ttbar.root > run/R21/loose/Top.log &
nohup ./tau_analysis -t TauLoose -o run/R21/loose/W.root -i "$filepath"W.root > run/R21/loose/W.log &
nohup ./tau_analysis -t TauLoose -o run/R21/loose/Zll.root -i "$filepath"Zll.root > run/R21/loose/Zll.log &
nohup ./tau_analysis -t TauLoose -m data -o run/R21/loose/data.root -i "$filepath"data.root > run/R21/loose/data.log &

##------------------------medium------------------------
nohup ./tau_analysis -m signal -o run/R21/medium/Ztt.root -i "$filepath"Ztt.root > run/R21/medium/Ztt.log &
nohup ./tau_analysis -m signal -o run/R21/medium/Top.root -i "$filepath"ttbar.root > run/R21/medium/Top.log &
nohup ./tau_analysis -o run/R21/medium/W.root -i "$filepath"W.root > run/R21/medium/W.log &
nohup ./tau_analysis -o run/R21/medium/Zll.root -i "$filepath"Zll.root > run/R21/medium/Zll.log &
nohup ./tau_analysis -m data -o run/R21/medium/data.root -i "$filepath"data.root > run/R21/medium/data.log &


###------------------------tight------------------------
nohup ./tau_analysis -t TauTight -m signal -o run/R21/tight/Ztt.root -i "$filepath"Ztt.root > run/R21/tight/Ztt.log &
nohup ./tau_analysis -t TauTight -m signal -o run/R21/tight/Top.root -i "$filepath"ttbar.root > run/R21/tight/Top.log &
nohup ./tau_analysis -t TauTight -o run/R21/tight/W.root -i "$filepath"W.root > run/R21/tight/W.log &
nohup ./tau_analysis -t TauTight -o run/R21/tight/Zll.root -i "$filepath"Zll.root > run/R21/tight/Zll.log &
nohup ./tau_analysis -t TauTight -m data -o run/R21/tight/data.root -i "$filepath"data.root > run/R21/tight/data.log &

