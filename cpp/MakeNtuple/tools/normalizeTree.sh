aim=$1

#todo: Make class uing macros
# root -l -b -q 'makeClass.C("'$aim','$path','$treename'")'

#remove C files because we don't need it
if [ -e "$aim".C ]; then
	rm "$aim".C
fi

#insert include files
sed -i '/#include <TFile.h>/a\#include "tauTree.h"' "$aim".h
sed -i '/#include <TFile.h>/a\#include "metadata.h"' "$aim".h

#extend this class to tauTree
sed -i 's/class '$aim'/& : public tauTree/' "$aim".h
sed -i '/TTree          \*fChain/d' "$aim".h
sed -i '/Int_t           fCurrent/d' "$aim".h
sed -i '/public :/a\   void updateCuts();' "$aim".h
sed -i '/Loop()/d' "$aim".h

#remove cxx definition
sed -i '/'"$aim"'_cxx/d' "$aim".h

#modify construction func
sed -i 's/ : fChain(0)/ /' "$aim".h
sed -i '/if (tree == 0)/i\   mc_channel_number = 0;' "$aim".h
sed -i '/if (tree == 0)/i\   \/\/set mc_channel_number to 0 initially to seprate data and mc;' "$aim".h
sed -i '/if (tree == 0)/i\   fChain = 0;' "$aim".h

#insert updateCuts()
sed -i '$a\void '$aim'::updateCuts() {' "$aim".h
sed -i '$a\    \/\/link the cut here' "$aim".h
sed -i '$a\}' "$aim".h

