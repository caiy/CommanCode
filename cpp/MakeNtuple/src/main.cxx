#include "TApplication.h"
#include "TROOT.h"
#include "TChain.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <ctime>
#include "tau.h"
#include "trees/R20Tree.h"
#include "trees/R21Tree.h"
#include "trees/AF2Tree.h"
#include "tauTree.h"
#include "tauSubClass.h"
#include "TTreeFormula.h"
#include <boost/program_options.hpp>

namespace po = boost::program_options;

static void splitCommaString(const std::string& names, std::vector<std::string>& result) {
	std::stringstream ss(names);
	while (ss.good()) {
		std::string substr;
		getline(ss, substr, ',');
		result.push_back(substr);
	}
}

int main(int argc, char* argv[]) {
	std::string outputName;
	double exceptionWei = 1;
	std::string chainname = "NOMINAL";

	tau* tauAna;
	tauTree* tree;

	po::options_description desc("Run SubTau varies on different Release(R20,R21 and AF2 is support currently)");
	desc.add_options()
		("help,h", "print this message and exit")
		("output,o", po::value<std::string>(&outputName)->default_value("output.root"), "Output name - if not supplied use name 'output.root'")
		("input-files,i", po::value< std::vector<std::string> >(), "Comma-separated list of input files")
		("tauMode,m", po::value<std::string>(), "Which kind of sample would like to run. 'signal', 'data' or 'normal'(default) is currently supported")
		("tauID,t", po::value<std::string>(), "Which tau quality you would like to use, TauNoCut, TauLoose, TauMedium(default) and TauTight is currently supported")
		("weight,w", po::value<double>(&exceptionWei), "Don't use default sample weight and define your own fill weight")
		("ntupleName,n",po::value<std::string>(&chainname), "Which tree you want to run, NOMINAL is default.")
		("release,r", po::value<std::string>(), "Which release tree you want to run, R20, R21(default) and AF2 is support currently.");

	po::positional_options_description p;
	p.add("input-files", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
	po::notify(vm);

	if (vm.count("help") || (vm.count("input-files") == 0)) {
		std::cout << desc << std::endl;
		return 1;
	}

	std::vector<std::string> inputFileNames;
	if (!vm.count("input-files"))
	{
		std::cerr << "***************************Error! No input file!**********************" << std::endl
				<<"Program terminated"<<std::endl;
		return -1;
	}
	for (const auto& fileNames : vm["input-files"].as<std::vector<std::string> >()) {
		splitCommaString(fileNames, inputFileNames);
	}

	TChain* chain = new TChain(chainname.c_str());
	std::cout << "Info: Files to analyze: ";
	for (const auto& fileName : inputFileNames) { 
		std::cout << fileName << " "; 
		chain->Add(fileName.c_str());
	}
	std::cout << std::endl;

	if (vm.count("tauMode"))
	{
		std::string mode = vm["tauMode"].as<std::string>();
		std::cout << "Info: Use tauMode " << mode << std::endl;
		if (mode == "normal"){
			tauAna = new tau(outputName);			
		}else if(mode == "signal"){
			tauAna = new signalTau(outputName);
		}else if (mode == "data") {
			tauAna = new dataTau(outputName);
		}else{
			std::cerr << "Error: Currently only support mode 'signal', 'data' or 'normal'(default)! Program terminated" << std::endl;
			return -2;
		}
	}else{
		std::cout << "Info: No tauMode is defined, use default tauMode: normal"<< std::endl;
		tauAna = new tau(outputName);
	}

	if (vm.count("release"))
	{
		std::string mode = vm["release"].as<std::string>();
		std::cout << "Info: Use release " << mode << std::endl;
		if (mode == "R21") {
			tree = new R21Tree(chain);
		}else if (mode == "R20") {
			tree = new R20Tree(chain);
		}else if (mode == "AF2") {
			tree = new AF2Tree(chain);
		}else {
			std::cerr << "Error: Currently only support mode 'R20', 'R21'(default) or 'AF2'! Program terminated" << std::endl;
			return -2;
		}
	}else {
		std::cout << "Info: No release is defined, use default release: R21" << std::endl;
		tree = new R21Tree(chain);
	}

	if (vm.count("weight")){
		std::cout << "Info: Use specified weight, value is: " << exceptionWei << std::endl;
		tauAna->setFillWeight(exceptionWei);
	}else{
		std::cout << "Info: Use default weight calculated from sample"<< std::endl;
	}
	
	std::string tauID = "TauMedium";
	if (vm.count("tauID")){
		std::string id = vm["tauID"].as<std::string>();
		if(id != "TauMedium" && id != "TauNoCut" && id != "TauLoose" && id != "TauTight"){
			std::cerr<<"Warnning: Invalid Working Point! You should choose Working point from TauNoCut, TauLoose, TauMedium, or TauTight!"<<std::endl
					<<"Choose back to defalt WP: TauMedium"<<std::endl;
		}else{
			tauID = id;
		}
	}
	std::cout << "Info: Using TauID: " << tauID <<std::endl;
	tauAna->SetTauWP(tauID);

	clock_t start,finish;
	std::cout << "*******************Start Analysis************************* " <<std::endl;
	start = clock();

	tree->setAnaClass(tauAna);
    tree->Loop();

	finish = clock();

	int TotalTime = (int) ((finish - start) / CLOCKS_PER_SEC);
	int hour = TotalTime / 3600;
	int minite = (TotalTime % 3600) / 60;
	int second = (TotalTime % 3600) % 60;

    std::cout << "********** End Analysis. Total run time is: "<<hour<<"h,"<<minite<<"m,"<<second<<"s"<<" ***********" << std::endl;

	return 0;
}
