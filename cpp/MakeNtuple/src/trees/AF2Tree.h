//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Apr  5 23:11:48 2018 by ROOT version 6.10/04
// from TTree NOMINAL/NOMINAL
// found on file: ../../../../../data/SubTau/V3/ZttAF2.root
//////////////////////////////////////////////////////////

#ifndef AF2Tree_h
#define AF2Tree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "metadata.h"
#include "../tauTree.h"

// Header file for the classes stored in the TTree if any.

class AF2Tree : public tauTree {
public :
   void updateCuts();

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          HLT_mu14_iloose_tau25_medium1_tracktwo;
   UInt_t          HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected;
   UInt_t          HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected;
   UInt_t          HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected;
   UInt_t          HLT_mu14_iloose_tau25_medium1_tracktwo_resurrected;
   UInt_t          HLT_mu14_iloose_tau35_medium1_tracktwo;
   UInt_t          HLT_mu14_ivarloose_tau25_medium1_tracktwo;
   UInt_t          HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected;
   UInt_t          HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected;
   UInt_t          HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected;
   UInt_t          HLT_mu14_ivarloose_tau25_medium1_tracktwo_resurrected;
   UInt_t          HLT_mu14_ivarloose_tau35_medium1_tracktwo;
   UInt_t          HLT_mu14_tau25_medium1_tracktwo;
   UInt_t          HLT_mu20_iloose_L1MU15;
   UInt_t          HLT_mu24_imedium;
   UInt_t          HLT_mu24_ivarmedium;
   UInt_t          HLT_mu26_imedium;
   UInt_t          HLT_mu26_ivarmedium;
   UInt_t          HLT_mu40;
   UInt_t          HLT_mu50;
   UInt_t          HLT_tau125_medium1_tracktwo_resurrected;
   UInt_t          HLT_tau160_medium1_tracktwo_resurrected;
   UInt_t          HLT_tau25_idperf_track_resurrected;
   UInt_t          HLT_tau25_idperf_tracktwo_resurrected;
   UInt_t          HLT_tau25_loose1_ptonly_resurrected;
   UInt_t          HLT_tau25_loose1_tracktwo_resurrected;
   UInt_t          HLT_tau25_medium1_mvonly_resurrected;
   UInt_t          HLT_tau25_medium1_ptonly_resurrected;
   UInt_t          HLT_tau25_medium1_track_resurrected;
   UInt_t          HLT_tau25_medium1_tracktwo_L1TAU12IL_resurrected;
   UInt_t          HLT_tau25_medium1_tracktwo_L1TAU12IT_resurrected;
   UInt_t          HLT_tau25_medium1_tracktwo_L1TAU12_resurrected;
   UInt_t          HLT_tau25_medium1_tracktwo_resurrected;
   UInt_t          HLT_tau25_perf_ptonly_resurrected;
   UInt_t          HLT_tau25_perf_track_resurrected;
   UInt_t          HLT_tau25_perf_tracktwo_resurrected;
   UInt_t          HLT_tau25_tight1_ptonly_resurrected;
   UInt_t          HLT_tau25_tight1_tracktwo_resurrected;
   UInt_t          HLT_tau35_medium1_tracktwo_resurrected;
   UInt_t          HLT_tau50_medium1_tracktwo_L1TAU12_resurrected;
   UInt_t          HLT_tau80_medium1_tracktwo_L1TAU60_resurrected;
   UInt_t          HLT_tau80_medium1_tracktwo_resurrected;
   UInt_t          L1_J25;
   UInt_t          L1_J25_resurrected;
   UInt_t          L1_TAU12IM_resurrected;
   UInt_t          L1_TAU12_resurrected;
   UInt_t          L1_TAU20IM_resurrected;
   UInt_t          L1_TAU20_resurrected;
   Float_t         NOMINAL_pileup_combined_weight;
   UInt_t          NOMINAL_pileup_random_run_number;
   Float_t         PRW_DATASF_1down_pileup_combined_weight;
   UInt_t          PRW_DATASF_1down_pileup_random_run_number;
   Float_t         PRW_DATASF_1up_pileup_combined_weight;
   UInt_t          PRW_DATASF_1up_pileup_random_run_number;
   Double_t        dilep_m;
   Int_t           event_clean_detector_core;
   ULong64_t       event_number;
   UInt_t          jet_0;
   Float_t         jet_0_FT_EFF_Eigen_B_0_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_0_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_0_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_0_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_1_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_1_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_1_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_1_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_2_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_2_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_2_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_B_2_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_0_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_0_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_0_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_0_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_1_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_1_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_1_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_1_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_2_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_2_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_2_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_C_2_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_0_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_0_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_0_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_0_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_1_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_1_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_1_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_1_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_2_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_2_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_2_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_2_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_3_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_3_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_3_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_3_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_4_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_4_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_4_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_Eigen_Light_4_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_extrapolation_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_extrapolation_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_extrapolation_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_extrapolation_1up_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_extrapolation_from_charm_1down_effSF_MVX;
   Float_t         jet_0_FT_EFF_extrapolation_from_charm_1down_ineffSF_MVX;
   Float_t         jet_0_FT_EFF_extrapolation_from_charm_1up_effSF_MVX;
   Float_t         jet_0_FT_EFF_extrapolation_from_charm_1up_ineffSF_MVX;
   Float_t         jet_0_JET_JvtEfficiency_1down_central_jets_effSF_JVT;
   Float_t         jet_0_JET_JvtEfficiency_1down_central_jets_ineffSF_JVT;
   Float_t         jet_0_JET_JvtEfficiency_1down_forward_jets_effSF_JVT;
   Float_t         jet_0_JET_JvtEfficiency_1down_forward_jets_ineffSF_JVT;
   Float_t         jet_0_JET_JvtEfficiency_1up_central_jets_effSF_JVT;
   Float_t         jet_0_JET_JvtEfficiency_1up_central_jets_ineffSF_JVT;
   Float_t         jet_0_JET_JvtEfficiency_1up_forward_jets_effSF_JVT;
   Float_t         jet_0_JET_JvtEfficiency_1up_forward_jets_ineffSF_JVT;
   Float_t         jet_0_JET_fJvtEfficiency_1down_central_jets_effSF_JVT;
   Float_t         jet_0_JET_fJvtEfficiency_1down_central_jets_ineffSF_JVT;
   Float_t         jet_0_JET_fJvtEfficiency_1down_forward_jets_effSF_JVT;
   Float_t         jet_0_JET_fJvtEfficiency_1down_forward_jets_ineffSF_JVT;
   Float_t         jet_0_JET_fJvtEfficiency_1up_central_jets_effSF_JVT;
   Float_t         jet_0_JET_fJvtEfficiency_1up_central_jets_ineffSF_JVT;
   Float_t         jet_0_JET_fJvtEfficiency_1up_forward_jets_effSF_JVT;
   Float_t         jet_0_JET_fJvtEfficiency_1up_forward_jets_ineffSF_JVT;
   Float_t         jet_0_NOMINAL_central_jets_effSF_JVT;
   Float_t         jet_0_NOMINAL_central_jets_ineffSF_JVT;
   Float_t         jet_0_NOMINAL_effSF_MVX;
   Float_t         jet_0_NOMINAL_forward_jets_effSF_JVT;
   Float_t         jet_0_NOMINAL_forward_jets_ineffSF_JVT;
   Float_t         jet_0_NOMINAL_ineffSF_MVX;
   Float_t         jet_0_et;
   Float_t         jet_0_eta;
   Float_t         jet_0_fjvt;
   Int_t           jet_0_flavorlabel;
   Int_t           jet_0_flavorlabel_cone;
   Int_t           jet_0_flavorlabel_part;
   Int_t           jet_0_is_Jvt_HS;
   Float_t         jet_0_jvt;
   Float_t         jet_0_m;
   Float_t         jet_0_mvx;
   Int_t           jet_0_mvx_tagged;
   Int_t           jet_0_mvx_tagweight;
   Float_t         jet_0_phi;
   Float_t         jet_0_pt;
   Float_t         jet_0_q;
   Float_t         jet_0_width;
   Float_t         jet_FT_EFF_Eigen_B_0_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_0_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_1_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_1_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_2_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_2_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_0_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_0_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_1_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_1_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_2_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_2_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_0_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_0_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_1_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_1_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_2_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_2_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_3_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_3_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_4_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_4_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_extrapolation_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_extrapolation_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_extrapolation_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_extrapolation_1up_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_MVX;
   Float_t         jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_MVX;
   Float_t         jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_MVX;
   Float_t         jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_MVX;
   Float_t         jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT;
   Float_t         jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT;
   Float_t         jet_JET_JvtEfficiency_1down_forward_jets_global_effSF_JVT;
   Float_t         jet_JET_JvtEfficiency_1down_forward_jets_global_ineffSF_JVT;
   Float_t         jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT;
   Float_t         jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT;
   Float_t         jet_JET_JvtEfficiency_1up_forward_jets_global_effSF_JVT;
   Float_t         jet_JET_JvtEfficiency_1up_forward_jets_global_ineffSF_JVT;
   Float_t         jet_JET_fJvtEfficiency_1down_central_jets_global_effSF_JVT;
   Float_t         jet_JET_fJvtEfficiency_1down_central_jets_global_ineffSF_JVT;
   Float_t         jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT;
   Float_t         jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT;
   Float_t         jet_JET_fJvtEfficiency_1up_central_jets_global_effSF_JVT;
   Float_t         jet_JET_fJvtEfficiency_1up_central_jets_global_ineffSF_JVT;
   Float_t         jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT;
   Float_t         jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT;
   Float_t         jet_NOMINAL_central_jets_global_effSF_JVT;
   Float_t         jet_NOMINAL_central_jets_global_ineffSF_JVT;
   Float_t         jet_NOMINAL_forward_jets_global_effSF_JVT;
   Float_t         jet_NOMINAL_forward_jets_global_ineffSF_JVT;
   Float_t         jet_NOMINAL_global_effSF_MVX;
   Float_t         jet_NOMINAL_global_ineffSF_MVX;
   UInt_t          lep_0;
   Float_t         lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient;
   Float_t         lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose;
   Float_t         lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient;
   Float_t         lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose;
   Float_t         lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient;
   Float_t         lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose;
   Float_t         lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient;
   Float_t         lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose;
   Float_t         lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;
   Float_t         lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;
   Float_t         lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;
   Float_t         lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient;
   Float_t         lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose;
   Float_t         lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient;
   Float_t         lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose;
   Float_t         lep_0_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11;
   Float_t         lep_0_NOMINAL_EleEffSF_offline_RecoTrk;
   Float_t         lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_0_NOMINAL_MuEffSF_IsoGradient;
   Float_t         lep_0_NOMINAL_MuEffSF_IsoLoose;
   Float_t         lep_0_NOMINAL_MuEffSF_Reco_QualMedium;
   Float_t         lep_0_NOMINAL_MuEffSF_TTVA;
   Float_t         lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient;
   Float_t         lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose;
   Float_t         lep_0_cluster_eta;
   Float_t         lep_0_cluster_eta_be2;
   Float_t         lep_0_et;
   Float_t         lep_0_eta;
   Int_t           lep_0_id_bad;
   Int_t           lep_0_id_loose;
   Int_t           lep_0_id_medium;
   Int_t           lep_0_id_tight;
   Int_t           lep_0_id_veryloose;
   UInt_t          lep_0_iso_FixedCutHighPtCaloOnly;
   UInt_t          lep_0_iso_FixedCutHighPtTrackOnly;
   UInt_t          lep_0_iso_FixedCutLoose;
   UInt_t          lep_0_iso_FixedCutTight;
   UInt_t          lep_0_iso_FixedCutTightTrackOnly;
   UInt_t          lep_0_iso_FixedCutTrackCone40;
   UInt_t          lep_0_iso_Gradient;
   UInt_t          lep_0_iso_GradientLoose;
   UInt_t          lep_0_iso_Loose;
   UInt_t          lep_0_iso_LooseTrackOnly;
   Float_t         lep_0_iso_etcone20;
   Float_t         lep_0_iso_etcone30;
   Float_t         lep_0_iso_etcone40;
   Float_t         lep_0_iso_ptcone20;
   Float_t         lep_0_iso_ptcone30;
   Float_t         lep_0_iso_ptcone40;
   Float_t         lep_0_iso_ptvarcone20;
   Float_t         lep_0_iso_ptvarcone30;
   Float_t         lep_0_iso_ptvarcone40;
   Float_t         lep_0_iso_topoetcone20;
   Float_t         lep_0_iso_topoetcone30;
   Float_t         lep_0_iso_topoetcone40;
   Float_t         lep_0_m;
   UInt_t          lep_0_matched;
   Float_t         lep_0_matched_et;
   Float_t         lep_0_matched_eta;
   Int_t           lep_0_matched_isHad;
   Float_t         lep_0_matched_m;
   Int_t           lep_0_matched_mother_pdgId;
   Int_t           lep_0_matched_mother_status;
   Int_t           lep_0_matched_origin;
   Int_t           lep_0_matched_pdgId;
   Float_t         lep_0_matched_phi;
   Float_t         lep_0_matched_pt;
   Float_t         lep_0_matched_q;
   Int_t           lep_0_matched_status;
   Int_t           lep_0_matched_type;
   Int_t           lep_0_muonAuthor;
   Int_t           lep_0_muonType;
   Float_t         lep_0_phi;
   Float_t         lep_0_pt;
   Float_t         lep_0_q;
   Float_t         lep_0_trk_d0;
   Float_t         lep_0_trk_d0_sig;
   Float_t         lep_0_trk_pt_error;
   Float_t         lep_0_trk_pvx_z0;
   Float_t         lep_0_trk_pvx_z0_sig;
   Float_t         lep_0_trk_pvx_z0_sintheta;
   Float_t         lep_0_trk_z0;
   Float_t         lep_0_trk_z0_sig;
   Float_t         lep_0_trk_z0_sintheta;
   UInt_t          lep_1;
   Float_t         lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient;
   Float_t         lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose;
   Float_t         lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient;
   Float_t         lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose;
   Float_t         lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient;
   Float_t         lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose;
   Float_t         lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient;
   Float_t         lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose;
   Float_t         lep_1_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;
   Float_t         lep_1_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;
   Float_t         lep_1_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;
   Float_t         lep_1_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient;
   Float_t         lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose;
   Float_t         lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient;
   Float_t         lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose;
   Float_t         lep_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11;
   Float_t         lep_1_NOMINAL_EleEffSF_offline_RecoTrk;
   Float_t         lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
   Float_t         lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
   Float_t         lep_1_NOMINAL_MuEffSF_IsoGradient;
   Float_t         lep_1_NOMINAL_MuEffSF_IsoLoose;
   Float_t         lep_1_NOMINAL_MuEffSF_Reco_QualMedium;
   Float_t         lep_1_NOMINAL_MuEffSF_TTVA;
   Float_t         lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient;
   Float_t         lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose;
   Float_t         lep_1_cluster_eta;
   Float_t         lep_1_cluster_eta_be2;
   Float_t         lep_1_et;
   Float_t         lep_1_eta;
   Int_t           lep_1_id_bad;
   Int_t           lep_1_id_loose;
   Int_t           lep_1_id_medium;
   Int_t           lep_1_id_tight;
   Int_t           lep_1_id_veryloose;
   UInt_t          lep_1_iso_FixedCutHighPtCaloOnly;
   UInt_t          lep_1_iso_FixedCutHighPtTrackOnly;
   UInt_t          lep_1_iso_FixedCutLoose;
   UInt_t          lep_1_iso_FixedCutTight;
   UInt_t          lep_1_iso_FixedCutTightTrackOnly;
   UInt_t          lep_1_iso_FixedCutTrackCone40;
   UInt_t          lep_1_iso_Gradient;
   UInt_t          lep_1_iso_GradientLoose;
   UInt_t          lep_1_iso_Loose;
   UInt_t          lep_1_iso_LooseTrackOnly;
   Float_t         lep_1_m;
   Int_t           lep_1_muonAuthor;
   Int_t           lep_1_muonType;
   Float_t         lep_1_phi;
   Float_t         lep_1_pt;
   Float_t         lep_1_q;
   Float_t         lep_1_trk_d0;
   Float_t         lep_1_trk_d0_sig;
   Float_t         lep_1_trk_pt_error;
   Float_t         lep_1_trk_pvx_z0;
   Float_t         lep_1_trk_pvx_z0_sig;
   Float_t         lep_1_trk_pvx_z0_sintheta;
   Float_t         lep_1_trk_z0;
   Float_t         lep_1_trk_z0_sig;
   Float_t         lep_1_trk_z0_sintheta;
   Int_t           lephad;
   Int_t           lephad_coll_approx;
   Float_t         lephad_coll_approx_m;
   Float_t         lephad_coll_approx_x0;
   Float_t         lephad_coll_approx_x1;
   Float_t         lephad_cosalpha;
   Float_t         lephad_deta;
   Float_t         lephad_dphi;
   Float_t         lephad_dpt;
   Float_t         lephad_dr;
   Float_t         lephad_eta;
   Int_t           lephad_met_bisect;
   Float_t         lephad_met_centrality;
   Float_t         lephad_met_lep0_cos_dphi;
   Float_t         lephad_met_lep1_cos_dphi;
   Float_t         lephad_met_min_dphi;
   Float_t         lephad_met_sum_cos_dphi;
   Float_t         lephad_mt_lep0_met;
   Float_t         lephad_mt_lep1_met;
   Float_t         lephad_phi;
   Float_t         lephad_ptx;
   Float_t         lephad_pty;
   Float_t         lephad_qxq;
   Float_t         lephad_scal_sum_pt;
   Float_t         lephad_tauRec_coll_approx;
   Float_t         lephad_tauRec_coll_approx_m;
   Float_t         lephad_tauRec_coll_approx_x0;
   Float_t         lephad_tauRec_coll_approx_x1;
   Float_t         lephad_tauRec_cosalpha;
   Float_t         lephad_tauRec_deta;
   Float_t         lephad_tauRec_dphi;
   Float_t         lephad_tauRec_dpt;
   Float_t         lephad_tauRec_dr;
   Float_t         lephad_tauRec_met_bisect;
   Float_t         lephad_tauRec_met_centrality;
   Float_t         lephad_tauRec_met_lep0_cos_dphi;
   Float_t         lephad_tauRec_met_lep1_cos_dphi;
   Float_t         lephad_tauRec_met_min_dphi;
   Float_t         lephad_tauRec_met_sum_cos_phi;
   Float_t         lephad_tauRec_mt_lep0_met;
   Float_t         lephad_tauRec_mt_lep1_met;
   Float_t         lephad_tauRec_scal_sum_pt;
   Float_t         lephad_tauRec_vect_sum_pt;
   Float_t         lephad_tauRec_vis_mass;
   Float_t         lephad_vect_sum_pt;
   Float_t         lephad_vis_mass;
   Float_t         matched_jet_0_eta;
   Float_t         matched_jet_0_m;
   Float_t         matched_jet_0_pdgid;
   Float_t         matched_jet_0_phi;
   Float_t         matched_jet_0_pt;
   UInt_t          mc_channel_number;
   Float_t         met_hpto_et;
   Float_t         met_hpto_etx;
   Float_t         met_hpto_ety;
   Float_t         met_hpto_phi;
   Float_t         met_more_met_et_ele;
   Float_t         met_more_met_et_jet;
   Float_t         met_more_met_et_muon;
   Float_t         met_more_met_et_pho;
   Float_t         met_more_met_et_soft;
   Float_t         met_more_met_et_tau;
   Float_t         met_more_met_phi_ele;
   Float_t         met_more_met_phi_jet;
   Float_t         met_more_met_phi_muon;
   Float_t         met_more_met_phi_pho;
   Float_t         met_more_met_phi_soft;
   Float_t         met_more_met_phi_tau;
   Float_t         met_more_met_sumet_ele;
   Float_t         met_more_met_sumet_jet;
   Float_t         met_more_met_sumet_muon;
   Float_t         met_more_met_sumet_pho;
   Float_t         met_more_met_sumet_soft;
   Float_t         met_more_met_sumet_tau;
   Float_t         met_reco_et;
   Float_t         met_reco_etx;
   Float_t         met_reco_ety;
   Float_t         met_reco_phi;
   Float_t         met_reco_sig;
   Float_t         met_reco_sig_tracks;
   Float_t         met_reco_sumet;
   Float_t         met_truth_et;
   Float_t         met_truth_etx;
   Float_t         met_truth_ety;
   Float_t         met_truth_phi;
   Float_t         met_truth_sig;
   Float_t         met_truth_sig_tracks;
   Float_t         met_truth_sumet;
   UInt_t          muTrigMatch_0_HLT_mu20_iloose_L1MU15;
   UInt_t          muTrigMatch_0_HLT_mu24_imedium;
   UInt_t          muTrigMatch_0_HLT_mu24_ivarmedium;
   UInt_t          muTrigMatch_0_HLT_mu26_imedium;
   UInt_t          muTrigMatch_0_HLT_mu26_ivarmedium;
   UInt_t          muTrigMatch_0_HLT_mu40;
   UInt_t          muTrigMatch_0_HLT_mu50;
   UInt_t          muTrigMatch_0_trigger_matched;
   Float_t         n_actual_int;
   Float_t         n_avg_int;
   Float_t         n_avg_int_cor;
   Int_t           n_bjets;
   Int_t           n_electrons;
   Int_t           n_electrons_met;
   Int_t           n_electrons_olr;
   Int_t           n_jets;
   Int_t           n_jets_30;
   Int_t           n_jets_40;
   Int_t           n_jets_bad;
   Int_t           n_jets_mc_hs;
   Int_t           n_jets_met;
   Int_t           n_jets_olr;
   Int_t           n_muons;
   Int_t           n_muons_met;
   Int_t           n_muons_olr;
   Int_t           n_photons;
   Int_t           n_photons_met;
   Int_t           n_photons_olr;
   Int_t           n_pvx;
   Int_t           n_taus;
   Int_t           n_taus_loose;
   Int_t           n_taus_medium;
   Int_t           n_taus_met;
   Int_t           n_taus_olr;
   Int_t           n_taus_tight;
   UInt_t          n_truth_gluon_jets;
   UInt_t          n_truth_jets;
   UInt_t          n_truth_jets_pt20_eta45;
   UInt_t          n_truth_quark_jets;
   Int_t           n_vx;
   UInt_t          run_number;
   UInt_t          tau_0;
   Float_t         tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad;
   Float_t         tau_0_NOMINAL_TauEffSF_JetBDTloose;
   Float_t         tau_0_NOMINAL_TauEffSF_JetBDTmedium;
   Float_t         tau_0_NOMINAL_TauEffSF_JetBDTtight;
   Float_t         tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron;
   Float_t         tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron;
   Float_t         tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron;
   Float_t         tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron;
   Float_t         tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron;
   Float_t         tau_0_NOMINAL_TauEffSF_reco;
   Float_t         tau_0_NOMINAL_TauEffSF_selection;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDTPlusVeto_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDT_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDTPlusVeto_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDT_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_VeryLooseLlhEleOLR_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDTPlusVeto_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDT_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDTPlusVeto_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDT_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_VeryLooseLlhEleOLR_electron;
   Float_t         tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTloose;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTmedium;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTtight;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTloose;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTmedium;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTtight;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTloose;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTmedium;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTtight;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTloose;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTmedium;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTtight;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco;
   Float_t         tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection;
   Float_t         tau_0_allTrk_eta;
   UInt_t          tau_0_allTrk_n;
   Float_t         tau_0_allTrk_phi;
   Float_t         tau_0_allTrk_pt;
   UInt_t          tau_0_decay_mode;
   Float_t         tau_0_ele_BDTEleScoreTrans_run2;
   UInt_t          tau_0_ele_bdt_loose;
   UInt_t          tau_0_ele_bdt_medium;
   Float_t         tau_0_ele_bdt_score;
   UInt_t          tau_0_ele_bdt_tight;
   Float_t         tau_0_ele_match_lhscore;
   UInt_t          tau_0_ele_olr_pass;
   Float_t         tau_0_eleid_AbsDEtaLeadTrk;
   Float_t         tau_0_eleid_AbsDPhiLeadTrk;
   Float_t         tau_0_eleid_AbsEtaLeadTrk;
   Float_t         tau_0_eleid_EMFracAtEMScale;
   Float_t         tau_0_eleid_EMFracFixed;
   Float_t         tau_0_eleid_PSSFraction;
   Float_t         tau_0_eleid_centFrac;
   Float_t         tau_0_eleid_etHotShotWinOverPtLeadTrk;
   Float_t         tau_0_eleid_etOverPtLeadTrk;
   Float_t         tau_0_eleid_hadLeakEt;
   Float_t         tau_0_eleid_hadLeakFracFixed;
   Float_t         tau_0_eleid_isolFrac;
   Float_t         tau_0_eleid_leadTrackProbHT;
   Float_t         tau_0_eleid_secMaxStripEt;
   Float_t         tau_0_eleid_secMaxStripEtOverPtLeadTrk;
   Float_t         tau_0_et;
   Float_t         tau_0_eta;
   Float_t         tau_0_id_ChPiEMEOverCaloEME;
   Float_t         tau_0_id_EMPOverTrkSysP;
   Float_t         tau_0_id_SumPtTrkFrac;
   Float_t         tau_0_id_centFrac;
   Float_t         tau_0_id_dRmax;
   Float_t         tau_0_id_etOverPtLeadTrk;
   Float_t         tau_0_id_innerTrkAvgDist;
   Float_t         tau_0_id_ipSigLeadTrk;
   Float_t         tau_0_id_mEflowApprox;
   Float_t         tau_0_id_massTrkSys;
   Float_t         tau_0_id_ptIntermediateAxis;
   Float_t         tau_0_id_ptRatioEflowApprox;
   Float_t         tau_0_id_trFlightPathSig;
   UInt_t          tau_0_jet_bdt_loose;
   UInt_t          tau_0_jet_bdt_medium;
   Float_t         tau_0_jet_bdt_score;
   Float_t         tau_0_jet_bdt_score_trans;
   UInt_t          tau_0_jet_bdt_tight;
   Float_t         tau_0_jet_width;
   Double_t        tau_0_leadTrk_d0;
   Double_t        tau_0_leadTrk_d0_sig;
   Float_t         tau_0_leadTrk_eta;
   Float_t         tau_0_leadTrk_phi;
   Float_t         tau_0_leadTrk_pt;
   Double_t        tau_0_leadTrk_pvx_z0;
   Double_t        tau_0_leadTrk_pvx_z0_sig;
   Double_t        tau_0_leadTrk_pvx_z0_sintheta;
   Double_t        tau_0_leadTrk_z0;
   Double_t        tau_0_leadTrk_z0_sig;
   Double_t        tau_0_leadTrk_z0_sintheta;
   Float_t         tau_0_m;
   Float_t         tau_0_mvx;
   Int_t           tau_0_mvx_tagged;
   UInt_t          tau_0_n_all_tracks;
   UInt_t          tau_0_n_charged_tracks;
   UInt_t          tau_0_n_conversion_tracks;
   UInt_t          tau_0_n_core_tracks;
   UInt_t          tau_0_n_failTrackFilter_tracks;
   UInt_t          tau_0_n_fake_tracks;
   UInt_t          tau_0_n_isolation_tracks;
   UInt_t          tau_0_n_modified_isolation_tracks;
   UInt_t          tau_0_n_old_tracks;
   UInt_t          tau_0_n_passTrkSelectionTight_tracks;
   UInt_t          tau_0_n_passTrkSelector_tracks;
   UInt_t          tau_0_n_unclassified_tracks;
   UInt_t          tau_0_n_wide_tracks;
   Float_t         tau_0_phi;
   Float_t         tau_0_pt;
   Float_t         tau_0_q;
   Float_t         tau_0_sub_ConstituentBased_eta;
   Float_t         tau_0_sub_ConstituentBased_m;
   Float_t         tau_0_sub_ConstituentBased_phi;
   Float_t         tau_0_sub_ConstituentBased_pt;
   Float_t         tau_0_sub_PanTauCellBased_eta;
   Float_t         tau_0_sub_PanTauCellBased_m;
   Float_t         tau_0_sub_PanTauCellBased_phi;
   Float_t         tau_0_sub_PanTauCellBased_pt;
   Float_t         tau_0_sub_PanTau_BDTValue_1p0n_vs_1p1n;
   Float_t         tau_0_sub_PanTau_BDTValue_1p1n_vs_1pXn;
   Float_t         tau_0_sub_PanTau_BDTValue_3p0n_vs_3pXn;
   Int_t           tau_0_sub_PanTau_BDTVar_Basic_NNeutralConsts;
   Float_t         tau_0_sub_PanTau_BDTVar_Charged_HLV_SumM;
   Float_t         tau_0_sub_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt;
   Float_t         tau_0_sub_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts;
   Float_t         tau_0_sub_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged;
   Float_t         tau_0_sub_PanTau_BDTVar_Neutral_HLV_SumM;
   Float_t         tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1;
   Float_t         tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2;
   Float_t         tau_0_sub_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts;
   Float_t         tau_0_sub_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts;
   Float_t         tau_0_sub_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed;
   Int_t           tau_0_sub_PanTau_DecayMode;
   Int_t           tau_0_sub_PanTau_DecayModeProto;
   Int_t           tau_0_sub_PanTau_isPanTauCandidate;
   Float_t         tau_0_sub_ParticleFlowCombined_eta;
   Float_t         tau_0_sub_ParticleFlowCombined_m;
   Float_t         tau_0_sub_ParticleFlowCombined_phi;
   Float_t         tau_0_sub_ParticleFlowCombined_pt;
   Float_t         tau_0_sub_ctrk0_cellBased_eta;
   Float_t         tau_0_sub_ctrk0_cellBased_phi;
   Float_t         tau_0_sub_ctrk0_cellBased_pt;
   Float_t         tau_0_sub_ctrk1_cellBased_eta;
   Float_t         tau_0_sub_ctrk1_cellBased_phi;
   Float_t         tau_0_sub_ctrk1_cellBased_pt;
   Float_t         tau_0_sub_ctrk2_cellBased_eta;
   Float_t         tau_0_sub_ctrk2_cellBased_phi;
   Float_t         tau_0_sub_ctrk2_cellBased_pt;
   Int_t           tau_0_sub_n_charged;
   Int_t           tau_0_sub_n_neutral;
   Float_t         tau_0_sub_neu0_bdtPi0Score;
   Float_t         tau_0_sub_neu0_cellBased_CENTER_LAMBDA;
   Float_t         tau_0_sub_neu0_cellBased_DELTA_PHI;
   Float_t         tau_0_sub_neu0_cellBased_DELTA_THETA;
   Float_t         tau_0_sub_neu0_cellBased_EM1CoreFrac;
   Float_t         tau_0_sub_neu0_cellBased_ENG_FRAC_CORE;
   Float_t         tau_0_sub_neu0_cellBased_ENG_FRAC_EM;
   Float_t         tau_0_sub_neu0_cellBased_ENG_FRAC_MAX;
   Float_t         tau_0_sub_neu0_cellBased_FIRST_ETA;
   Float_t         tau_0_sub_neu0_cellBased_LATERAL;
   Float_t         tau_0_sub_neu0_cellBased_LONGITUDINAL;
   Int_t           tau_0_sub_neu0_cellBased_NHitsInEM1;
   Int_t           tau_0_sub_neu0_cellBased_NPosECells_EM1;
   Int_t           tau_0_sub_neu0_cellBased_NPosECells_EM2;
   Int_t           tau_0_sub_neu0_cellBased_NPosECells_PS;
   Float_t         tau_0_sub_neu0_cellBased_SECOND_ENG_DENS;
   Float_t         tau_0_sub_neu0_cellBased_SECOND_LAMBDA;
   Float_t         tau_0_sub_neu0_cellBased_SECOND_R;
   Float_t         tau_0_sub_neu0_cellBased_asymmetryInEM1WRTTrk;
   Float_t         tau_0_sub_neu0_cellBased_energy_EM1;
   Float_t         tau_0_sub_neu0_cellBased_energy_EM2;
   Float_t         tau_0_sub_neu0_cellBased_eta;
   Float_t         tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM1;
   Float_t         tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM2;
   Float_t         tau_0_sub_neu0_cellBased_phi;
   Float_t         tau_0_sub_neu0_cellBased_pt;
   Float_t         tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM1;
   Float_t         tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM2;
   Float_t         tau_0_sub_neu1_bdtPi0Score;
   Float_t         tau_0_sub_neu1_cellBased_CENTER_LAMBDA;
   Float_t         tau_0_sub_neu1_cellBased_DELTA_PHI;
   Float_t         tau_0_sub_neu1_cellBased_DELTA_THETA;
   Float_t         tau_0_sub_neu1_cellBased_EM1CoreFrac;
   Float_t         tau_0_sub_neu1_cellBased_ENG_FRAC_CORE;
   Float_t         tau_0_sub_neu1_cellBased_ENG_FRAC_EM;
   Float_t         tau_0_sub_neu1_cellBased_ENG_FRAC_MAX;
   Float_t         tau_0_sub_neu1_cellBased_FIRST_ETA;
   Float_t         tau_0_sub_neu1_cellBased_LATERAL;
   Float_t         tau_0_sub_neu1_cellBased_LONGITUDINAL;
   Int_t           tau_0_sub_neu1_cellBased_NHitsInEM1;
   Int_t           tau_0_sub_neu1_cellBased_NPosECells_EM1;
   Int_t           tau_0_sub_neu1_cellBased_NPosECells_EM2;
   Int_t           tau_0_sub_neu1_cellBased_NPosECells_PS;
   Float_t         tau_0_sub_neu1_cellBased_SECOND_ENG_DENS;
   Float_t         tau_0_sub_neu1_cellBased_SECOND_LAMBDA;
   Float_t         tau_0_sub_neu1_cellBased_SECOND_R;
   Float_t         tau_0_sub_neu1_cellBased_asymmetryInEM1WRTTrk;
   Float_t         tau_0_sub_neu1_cellBased_energy_EM1;
   Float_t         tau_0_sub_neu1_cellBased_energy_EM2;
   Float_t         tau_0_sub_neu1_cellBased_eta;
   Float_t         tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM1;
   Float_t         tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM2;
   Float_t         tau_0_sub_neu1_cellBased_phi;
   Float_t         tau_0_sub_neu1_cellBased_pt;
   Float_t         tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM1;
   Float_t         tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM2;
   Float_t         tau_0_tauRec_eta;
   Float_t         tau_0_tauRec_m;
   Float_t         tau_0_tauRec_phi;
   Float_t         tau_0_tauRec_pt;
   Double_t        tau_0_trig1_HLT_ChPiEMEOverCaloEME;
   Double_t        tau_0_trig1_HLT_ChPiEMEOverCaloEMECorrected;
   Double_t        tau_0_trig1_HLT_EMPOverTrkSysP;
   Double_t        tau_0_trig1_HLT_EMPOverTrkSysPCorrected;
   Double_t        tau_0_trig1_HLT_SumPtTrkFrac;
   Double_t        tau_0_trig1_HLT_SumPtTrkFracCorrected;
   Double_t        tau_0_trig1_HLT_centFrac;
   Double_t        tau_0_trig1_HLT_centFracCorrected;
   Double_t        tau_0_trig1_HLT_dRmax;
   Double_t        tau_0_trig1_HLT_dRmaxCorrected;
   Double_t        tau_0_trig1_HLT_eraw;
   Double_t        tau_0_trig1_HLT_etOverPtLeadTrk;
   Double_t        tau_0_trig1_HLT_etOverPtLeadTrkCorrected;
   Double_t        tau_0_trig1_HLT_eta;
   Double_t        tau_0_trig1_HLT_etraw;
   Double_t        tau_0_trig1_HLT_innerTrkAvgDist;
   Double_t        tau_0_trig1_HLT_innerTrkAvgDistCorrected;
   Double_t        tau_0_trig1_HLT_ipSigLeadTrk;
   Double_t        tau_0_trig1_HLT_ipSigLeadTrkCorrected;
   Int_t           tau_0_trig1_HLT_jet_bdt_loose;
   Int_t           tau_0_trig1_HLT_jet_bdt_medium;
   Double_t        tau_0_trig1_HLT_jet_bdt_score;
   Int_t           tau_0_trig1_HLT_jet_bdt_tight;
   Double_t        tau_0_trig1_HLT_m;
   Double_t        tau_0_trig1_HLT_mEflowApprox;
   Double_t        tau_0_trig1_HLT_mEflowApproxCorrected;
   Double_t        tau_0_trig1_HLT_massTrkSys;
   Double_t        tau_0_trig1_HLT_massTrkSysCorrected;
   Int_t           tau_0_trig1_HLT_matched;
   Int_t           tau_0_trig1_HLT_n_charged_tracks;
   Int_t           tau_0_trig1_HLT_n_isolation_tracks;
   Double_t        tau_0_trig1_HLT_phi;
   Double_t        tau_0_trig1_HLT_pt;
   Double_t        tau_0_trig1_HLT_ptRatioEflowApprox;
   Double_t        tau_0_trig1_HLT_ptRatioEflowApproxCorrected;
   Double_t        tau_0_trig1_HLT_trFlightPathSig;
   Double_t        tau_0_trig1_HLT_trFlightPathSigCorrected;
   Double_t        tau_0_trig2_PreselTrig_eta;
   Double_t        tau_0_trig2_PreselTrig_m;
   Int_t           tau_0_trig2_PreselTrig_matched;
   Int_t           tau_0_trig2_PreselTrig_n_charged_tracks;
   Int_t           tau_0_trig2_PreselTrig_n_isolation_tracks;
   Double_t        tau_0_trig2_PreselTrig_phi;
   Double_t        tau_0_trig2_PreselTrig_pt;
   UInt_t          tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I;
   UInt_t          tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25;
   UInt_t          tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM;
   UInt_t          tau_0_trig_HLT_mu14_iloose_tau35_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I;
   UInt_t          tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25;
   UInt_t          tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM;
   UInt_t          tau_0_trig_HLT_mu14_ivarloose_tau35_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_tau125_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_tau160_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_tau25_idperf_track;
   UInt_t          tau_0_trig_HLT_tau25_idperf_tracktwo;
   UInt_t          tau_0_trig_HLT_tau25_loose1_ptonly;
   UInt_t          tau_0_trig_HLT_tau25_loose1_tracktwo;
   UInt_t          tau_0_trig_HLT_tau25_medium1_mvonly;
   UInt_t          tau_0_trig_HLT_tau25_medium1_ptonly;
   UInt_t          tau_0_trig_HLT_tau25_medium1_track;
   UInt_t          tau_0_trig_HLT_tau25_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12;
   UInt_t          tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IL;
   UInt_t          tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IT;
   UInt_t          tau_0_trig_HLT_tau25_perf_ptonly;
   UInt_t          tau_0_trig_HLT_tau25_perf_track;
   UInt_t          tau_0_trig_HLT_tau25_perf_tracktwo;
   UInt_t          tau_0_trig_HLT_tau25_tight1_ptonly;
   UInt_t          tau_0_trig_HLT_tau25_tight1_tracktwo;
   UInt_t          tau_0_trig_HLT_tau35_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_tau50_medium1_tracktwo_L1TAU12;
   UInt_t          tau_0_trig_HLT_tau80_medium1_tracktwo;
   UInt_t          tau_0_trig_HLT_tau80_medium1_tracktwo_L1TAU60;
   UInt_t          tau_0_trig_L1_J25;
   UInt_t          tau_0_trig_L1_TAU12;
   UInt_t          tau_0_trig_L1_TAU12IM;
   UInt_t          tau_0_trig_L1_TAU20;
   UInt_t          tau_0_trig_L1_TAU20IM;
   UInt_t          tau_0_trig_trigger_matched;
   Int_t           tau_0_trk_multi_ckt4iso_n;
   Int_t           tau_0_trk_multi_cw_dr60_d04_n_l5;
   Int_t           tau_0_trk_multi_cw_dr60_d04_n_lp5;
   Int_t           tau_0_trk_multi_cw_dr60_d04_n_t5;
   Int_t           tau_0_trk_multi_cw_dr60_n_l5;
   Int_t           tau_0_trk_multi_cw_dr60_n_lp5;
   Int_t           tau_0_trk_multi_cw_dr60_n_t5;
   Int_t           tau_0_trk_multi_cw_dr80_d04_n_l5;
   Int_t           tau_0_trk_multi_cw_dr80_d04_n_lp5;
   Int_t           tau_0_trk_multi_cw_dr80_d04_n_t5;
   Int_t           tau_0_trk_multi_cw_dr80_n_l5;
   Int_t           tau_0_trk_multi_cw_dr80_n_lp5;
   Int_t           tau_0_trk_multi_cw_dr80_n_t5;
   Int_t           tau_0_trk_multi_cws_dr60_d04_n_l5;
   Int_t           tau_0_trk_multi_cws_dr60_d04_n_lp5;
   Int_t           tau_0_trk_multi_cws_dr60_d04_n_t5;
   Int_t           tau_0_trk_multi_cws_dr60_n_l5;
   Int_t           tau_0_trk_multi_cws_dr60_n_lp5;
   Int_t           tau_0_trk_multi_cws_dr60_n_t5;
   Int_t           tau_0_trk_multi_cws_dr80_d04_n_l5;
   Int_t           tau_0_trk_multi_cws_dr80_d04_n_lp5;
   Int_t           tau_0_trk_multi_cws_dr80_d04_n_t5;
   Int_t           tau_0_trk_multi_cws_dr80_n_l5;
   Int_t           tau_0_trk_multi_cws_dr80_n_lp5;
   Int_t           tau_0_trk_multi_cws_dr80_n_t5;
   UInt_t          tau_0_trk_multi_diag_n_had_tracks;
   UInt_t          tau_0_trk_multi_diag_n_spu_cv_tracks;
   UInt_t          tau_0_trk_multi_diag_n_spu_fk_tracks;
   UInt_t          tau_0_trk_multi_diag_n_spu_pu_tracks;
   UInt_t          tau_0_trk_multi_diag_n_spu_tracks;
   UInt_t          tau_0_trk_multi_diag_n_spu_uc_tracks;
   UInt_t          tau_0_trk_multi_diag_n_spu_ue_tracks;
   UInt_t          tau_0_trk_multi_diag_n_tau_tracks;
   Float_t         tau_0_trk_multi_diag_purity;
   UInt_t          tau_0_truth;
   UInt_t          tau_0_truth_classifierParticleOrigin;
   UInt_t          tau_0_truth_classifierParticleType;
   Int_t           tau_0_truth_decay_mode;
   Float_t         tau_0_truth_et;
   Float_t         tau_0_truth_eta;
   Float_t         tau_0_truth_eta_vis;
   Double_t        tau_0_truth_eta_vis_charged;
   Double_t        tau_0_truth_eta_vis_neutral;
   Int_t           tau_0_truth_isEle;
   Int_t           tau_0_truth_isHadTau;
   Int_t           tau_0_truth_isJet;
   Int_t           tau_0_truth_isMuon;
   Int_t           tau_0_truth_isTau;
   Float_t         tau_0_truth_m;
   Float_t         tau_0_truth_m_vis;
   Double_t        tau_0_truth_m_vis_charged;
   Double_t        tau_0_truth_m_vis_neutral;
   Int_t           tau_0_truth_mother_pdgId;
   Int_t           tau_0_truth_mother_status;
   Int_t           tau_0_truth_n_charged;
   Int_t           tau_0_truth_n_charged_pion;
   Int_t           tau_0_truth_n_neutral;
   Int_t           tau_0_truth_n_neutral_pion;
   Int_t           tau_0_truth_origin;
   Int_t           tau_0_truth_pdgId;
   Float_t         tau_0_truth_phi;
   Float_t         tau_0_truth_phi_vis;
   Double_t        tau_0_truth_phi_vis_charged;
   Double_t        tau_0_truth_phi_vis_neutral;
   Float_t         tau_0_truth_pt;
   Float_t         tau_0_truth_pt_vis;
   Double_t        tau_0_truth_pt_vis_charged;
   Double_t        tau_0_truth_pt_vis_neutral;
   Float_t         tau_0_truth_pz;
   Float_t         tau_0_truth_q;
   Int_t           tau_0_truth_status;
   Int_t           tau_0_truth_type;
   UInt_t          tau_1;
   Float_t         tau_1_allTrk_eta;
   UInt_t          tau_1_allTrk_n;
   Float_t         tau_1_allTrk_phi;
   Float_t         tau_1_allTrk_pt;
   UInt_t          tau_1_decay_mode;
   Float_t         tau_1_ele_BDTEleScoreTrans_run2;
   UInt_t          tau_1_ele_bdt_loose;
   UInt_t          tau_1_ele_bdt_medium;
   Float_t         tau_1_ele_bdt_score;
   UInt_t          tau_1_ele_bdt_tight;
   Float_t         tau_1_ele_match_lhscore;
   UInt_t          tau_1_ele_olr_pass;
   Float_t         tau_1_et;
   Float_t         tau_1_eta;
   UInt_t          tau_1_jet_bdt_loose;
   UInt_t          tau_1_jet_bdt_medium;
   Float_t         tau_1_jet_bdt_score;
   Float_t         tau_1_jet_bdt_score_trans;
   UInt_t          tau_1_jet_bdt_tight;
   Float_t         tau_1_jet_width;
   Float_t         tau_1_leadTrk_eta;
   Float_t         tau_1_leadTrk_phi;
   Float_t         tau_1_leadTrk_pt;
   Float_t         tau_1_m;
   Float_t         tau_1_mvx;
   Int_t           tau_1_mvx_tagged;
   UInt_t          tau_1_n_all_tracks;
   UInt_t          tau_1_n_charged_tracks;
   UInt_t          tau_1_n_conversion_tracks;
   UInt_t          tau_1_n_core_tracks;
   UInt_t          tau_1_n_failTrackFilter_tracks;
   UInt_t          tau_1_n_fake_tracks;
   UInt_t          tau_1_n_isolation_tracks;
   UInt_t          tau_1_n_modified_isolation_tracks;
   UInt_t          tau_1_n_old_tracks;
   UInt_t          tau_1_n_passTrkSelectionTight_tracks;
   UInt_t          tau_1_n_passTrkSelector_tracks;
   UInt_t          tau_1_n_unclassified_tracks;
   UInt_t          tau_1_n_wide_tracks;
   Float_t         tau_1_phi;
   Float_t         tau_1_pt;
   Float_t         tau_1_q;
   Float_t         tau_1_tauRec_eta;
   Float_t         tau_1_tauRec_m;
   Float_t         tau_1_tauRec_phi;
   Float_t         tau_1_tauRec_pt;
   Double_t        tau_tes_alpha_pt_shift;
   Double_t        weight_mc;
   Double_t        weight_total;

   // List of branches
   TBranch        *b_HLT_mu14_iloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected;   //!
   TBranch        *b_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected;   //!
   TBranch        *b_HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected;   //!
   TBranch        *b_HLT_mu14_iloose_tau25_medium1_tracktwo_resurrected;   //!
   TBranch        *b_HLT_mu14_iloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_HLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected;   //!
   TBranch        *b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected;   //!
   TBranch        *b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected;   //!
   TBranch        *b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_resurrected;   //!
   TBranch        *b_HLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_HLT_mu14_tau25_medium1_tracktwo;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu24_imedium;   //!
   TBranch        *b_HLT_mu24_ivarmedium;   //!
   TBranch        *b_HLT_mu26_imedium;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_mu40;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_tau125_medium1_tracktwo_resurrected;   //!
   TBranch        *b_HLT_tau160_medium1_tracktwo_resurrected;   //!
   TBranch        *b_HLT_tau25_idperf_track_resurrected;   //!
   TBranch        *b_HLT_tau25_idperf_tracktwo_resurrected;   //!
   TBranch        *b_HLT_tau25_loose1_ptonly_resurrected;   //!
   TBranch        *b_HLT_tau25_loose1_tracktwo_resurrected;   //!
   TBranch        *b_HLT_tau25_medium1_mvonly_resurrected;   //!
   TBranch        *b_HLT_tau25_medium1_ptonly_resurrected;   //!
   TBranch        *b_HLT_tau25_medium1_track_resurrected;   //!
   TBranch        *b_HLT_tau25_medium1_tracktwo_L1TAU12IL_resurrected;   //!
   TBranch        *b_HLT_tau25_medium1_tracktwo_L1TAU12IT_resurrected;   //!
   TBranch        *b_HLT_tau25_medium1_tracktwo_L1TAU12_resurrected;   //!
   TBranch        *b_HLT_tau25_medium1_tracktwo_resurrected;   //!
   TBranch        *b_HLT_tau25_perf_ptonly_resurrected;   //!
   TBranch        *b_HLT_tau25_perf_track_resurrected;   //!
   TBranch        *b_HLT_tau25_perf_tracktwo_resurrected;   //!
   TBranch        *b_HLT_tau25_tight1_ptonly_resurrected;   //!
   TBranch        *b_HLT_tau25_tight1_tracktwo_resurrected;   //!
   TBranch        *b_HLT_tau35_medium1_tracktwo_resurrected;   //!
   TBranch        *b_HLT_tau50_medium1_tracktwo_L1TAU12_resurrected;   //!
   TBranch        *b_HLT_tau80_medium1_tracktwo_L1TAU60_resurrected;   //!
   TBranch        *b_HLT_tau80_medium1_tracktwo_resurrected;   //!
   TBranch        *b_L1_J25;   //!
   TBranch        *b_L1_J25_resurrected;   //!
   TBranch        *b_L1_TAU12IM_resurrected;   //!
   TBranch        *b_L1_TAU12_resurrected;   //!
   TBranch        *b_L1_TAU20IM_resurrected;   //!
   TBranch        *b_L1_TAU20_resurrected;   //!
   TBranch        *b_NOMINAL_pileup_combined_weight;   //!
   TBranch        *b_NOMINAL_pileup_random_run_number;   //!
   TBranch        *b_PRW_DATASF_1down_pileup_combined_weight;   //!
   TBranch        *b_PRW_DATASF_1down_pileup_random_run_number;   //!
   TBranch        *b_PRW_DATASF_1up_pileup_combined_weight;   //!
   TBranch        *b_PRW_DATASF_1up_pileup_random_run_number;   //!
   TBranch        *b_dilep_m;   //!
   TBranch        *b_event_clean_detector_core;   //!
   TBranch        *b_event_number;   //!
   TBranch        *b_jet_0;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_0_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_0_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_0_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_0_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_1_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_1_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_1_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_1_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_2_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_2_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_2_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_B_2_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_0_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_0_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_0_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_0_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_1_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_1_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_1_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_1_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_2_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_2_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_2_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_C_2_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_0_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_0_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_0_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_0_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_1_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_1_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_1_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_1_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_2_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_2_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_2_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_2_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_3_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_3_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_3_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_3_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_4_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_4_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_4_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_Eigen_Light_4_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_extrapolation_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_extrapolation_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_extrapolation_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_extrapolation_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_extrapolation_from_charm_1down_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_extrapolation_from_charm_1down_ineffSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_extrapolation_from_charm_1up_effSF_MVX;   //!
   TBranch        *b_jet_0_FT_EFF_extrapolation_from_charm_1up_ineffSF_MVX;   //!
   TBranch        *b_jet_0_JET_JvtEfficiency_1down_central_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_JET_JvtEfficiency_1down_central_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_JET_JvtEfficiency_1down_forward_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_JET_JvtEfficiency_1down_forward_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_JET_JvtEfficiency_1up_central_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_JET_JvtEfficiency_1up_central_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_JET_JvtEfficiency_1up_forward_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_JET_JvtEfficiency_1up_forward_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_JET_fJvtEfficiency_1down_central_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_JET_fJvtEfficiency_1down_central_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_JET_fJvtEfficiency_1down_forward_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_JET_fJvtEfficiency_1down_forward_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_JET_fJvtEfficiency_1up_central_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_JET_fJvtEfficiency_1up_central_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_JET_fJvtEfficiency_1up_forward_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_JET_fJvtEfficiency_1up_forward_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_NOMINAL_central_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_NOMINAL_central_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_NOMINAL_effSF_MVX;   //!
   TBranch        *b_jet_0_NOMINAL_forward_jets_effSF_JVT;   //!
   TBranch        *b_jet_0_NOMINAL_forward_jets_ineffSF_JVT;   //!
   TBranch        *b_jet_0_NOMINAL_ineffSF_MVX;   //!
   TBranch        *b_jet_0_et;   //!
   TBranch        *b_jet_0_eta;   //!
   TBranch        *b_jet_0_fjvt;   //!
   TBranch        *b_jet_0_flavorlabel;   //!
   TBranch        *b_jet_0_flavorlabel_cone;   //!
   TBranch        *b_jet_0_flavorlabel_part;   //!
   TBranch        *b_jet_0_is_Jvt_HS;   //!
   TBranch        *b_jet_0_jvt;   //!
   TBranch        *b_jet_0_m;   //!
   TBranch        *b_jet_0_mvx;   //!
   TBranch        *b_jet_0_mvx_tagged;   //!
   TBranch        *b_jet_0_mvx_tagweight;   //!
   TBranch        *b_jet_0_phi;   //!
   TBranch        *b_jet_0_pt;   //!
   TBranch        *b_jet_0_q;   //!
   TBranch        *b_jet_0_width;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_0_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_0_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_1_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_1_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_2_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_2_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_0_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_0_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_1_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_1_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_2_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_2_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_0_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_0_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_1_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_1_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_2_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_2_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_3_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_3_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_4_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_4_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_extrapolation_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_extrapolation_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_extrapolation_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_extrapolation_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_MVX;   //!
   TBranch        *b_jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_MVX;   //!
   TBranch        *b_jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_JET_JvtEfficiency_1down_forward_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_JET_JvtEfficiency_1down_forward_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_JET_JvtEfficiency_1up_forward_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_JET_JvtEfficiency_1up_forward_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_JET_fJvtEfficiency_1down_central_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_JET_fJvtEfficiency_1down_central_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_JET_fJvtEfficiency_1up_central_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_JET_fJvtEfficiency_1up_central_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_NOMINAL_central_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_NOMINAL_central_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_NOMINAL_forward_jets_global_effSF_JVT;   //!
   TBranch        *b_jet_NOMINAL_forward_jets_global_ineffSF_JVT;   //!
   TBranch        *b_jet_NOMINAL_global_effSF_MVX;   //!
   TBranch        *b_jet_NOMINAL_global_ineffSF_MVX;   //!
   TBranch        *b_lep_0;   //!
   TBranch        *b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;   //!
   TBranch        *b_lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;   //!
   TBranch        *b_lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;   //!
   TBranch        *b_lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient;   //!
   TBranch        *b_lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose;   //!
   TBranch        *b_lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient;   //!
   TBranch        *b_lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose;   //!
   TBranch        *b_lep_0_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11;   //!
   TBranch        *b_lep_0_NOMINAL_EleEffSF_offline_RecoTrk;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_0_NOMINAL_MuEffSF_TTVA;   //!
   TBranch        *b_lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient;   //!
   TBranch        *b_lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose;   //!
   TBranch        *b_lep_0_cluster_eta;   //!
   TBranch        *b_lep_0_cluster_eta_be2;   //!
   TBranch        *b_lep_0_et;   //!
   TBranch        *b_lep_0_eta;   //!
   TBranch        *b_lep_0_id_bad;   //!
   TBranch        *b_lep_0_id_loose;   //!
   TBranch        *b_lep_0_id_medium;   //!
   TBranch        *b_lep_0_id_tight;   //!
   TBranch        *b_lep_0_id_veryloose;   //!
   TBranch        *b_lep_0_iso_FixedCutHighPtCaloOnly;   //!
   TBranch        *b_lep_0_iso_FixedCutHighPtTrackOnly;   //!
   TBranch        *b_lep_0_iso_FixedCutLoose;   //!
   TBranch        *b_lep_0_iso_FixedCutTight;   //!
   TBranch        *b_lep_0_iso_FixedCutTightTrackOnly;   //!
   TBranch        *b_lep_0_iso_FixedCutTrackCone40;   //!
   TBranch        *b_lep_0_iso_Gradient;   //!
   TBranch        *b_lep_0_iso_GradientLoose;   //!
   TBranch        *b_lep_0_iso_Loose;   //!
   TBranch        *b_lep_0_iso_LooseTrackOnly;   //!
   TBranch        *b_lep_0_iso_etcone20;   //!
   TBranch        *b_lep_0_iso_etcone30;   //!
   TBranch        *b_lep_0_iso_etcone40;   //!
   TBranch        *b_lep_0_iso_ptcone20;   //!
   TBranch        *b_lep_0_iso_ptcone30;   //!
   TBranch        *b_lep_0_iso_ptcone40;   //!
   TBranch        *b_lep_0_iso_ptvarcone20;   //!
   TBranch        *b_lep_0_iso_ptvarcone30;   //!
   TBranch        *b_lep_0_iso_ptvarcone40;   //!
   TBranch        *b_lep_0_iso_topoetcone20;   //!
   TBranch        *b_lep_0_iso_topoetcone30;   //!
   TBranch        *b_lep_0_iso_topoetcone40;   //!
   TBranch        *b_lep_0_m;   //!
   TBranch        *b_lep_0_matched;   //!
   TBranch        *b_lep_0_matched_et;   //!
   TBranch        *b_lep_0_matched_eta;   //!
   TBranch        *b_lep_0_matched_isHad;   //!
   TBranch        *b_lep_0_matched_m;   //!
   TBranch        *b_lep_0_matched_mother_pdgId;   //!
   TBranch        *b_lep_0_matched_mother_status;   //!
   TBranch        *b_lep_0_matched_origin;   //!
   TBranch        *b_lep_0_matched_pdgId;   //!
   TBranch        *b_lep_0_matched_phi;   //!
   TBranch        *b_lep_0_matched_pt;   //!
   TBranch        *b_lep_0_matched_q;   //!
   TBranch        *b_lep_0_matched_status;   //!
   TBranch        *b_lep_0_matched_type;   //!
   TBranch        *b_lep_0_muonAuthor;   //!
   TBranch        *b_lep_0_muonType;   //!
   TBranch        *b_lep_0_phi;   //!
   TBranch        *b_lep_0_pt;   //!
   TBranch        *b_lep_0_q;   //!
   TBranch        *b_lep_0_trk_d0;   //!
   TBranch        *b_lep_0_trk_d0_sig;   //!
   TBranch        *b_lep_0_trk_pt_error;   //!
   TBranch        *b_lep_0_trk_pvx_z0;   //!
   TBranch        *b_lep_0_trk_pvx_z0_sig;   //!
   TBranch        *b_lep_0_trk_pvx_z0_sintheta;   //!
   TBranch        *b_lep_0_trk_z0;   //!
   TBranch        *b_lep_0_trk_z0_sig;   //!
   TBranch        *b_lep_0_trk_z0_sintheta;   //!
   TBranch        *b_lep_1;   //!
   TBranch        *b_lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_1_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;   //!
   TBranch        *b_lep_1_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;   //!
   TBranch        *b_lep_1_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;   //!
   TBranch        *b_lep_1_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient;   //!
   TBranch        *b_lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose;   //!
   TBranch        *b_lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient;   //!
   TBranch        *b_lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose;   //!
   TBranch        *b_lep_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11;   //!
   TBranch        *b_lep_1_NOMINAL_EleEffSF_offline_RecoTrk;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_IsoGradient;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_IsoLoose;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_Reco_QualMedium;   //!
   TBranch        *b_lep_1_NOMINAL_MuEffSF_TTVA;   //!
   TBranch        *b_lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient;   //!
   TBranch        *b_lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose;   //!
   TBranch        *b_lep_1_cluster_eta;   //!
   TBranch        *b_lep_1_cluster_eta_be2;   //!
   TBranch        *b_lep_1_et;   //!
   TBranch        *b_lep_1_eta;   //!
   TBranch        *b_lep_1_id_bad;   //!
   TBranch        *b_lep_1_id_loose;   //!
   TBranch        *b_lep_1_id_medium;   //!
   TBranch        *b_lep_1_id_tight;   //!
   TBranch        *b_lep_1_id_veryloose;   //!
   TBranch        *b_lep_1_iso_FixedCutHighPtCaloOnly;   //!
   TBranch        *b_lep_1_iso_FixedCutHighPtTrackOnly;   //!
   TBranch        *b_lep_1_iso_FixedCutLoose;   //!
   TBranch        *b_lep_1_iso_FixedCutTight;   //!
   TBranch        *b_lep_1_iso_FixedCutTightTrackOnly;   //!
   TBranch        *b_lep_1_iso_FixedCutTrackCone40;   //!
   TBranch        *b_lep_1_iso_Gradient;   //!
   TBranch        *b_lep_1_iso_GradientLoose;   //!
   TBranch        *b_lep_1_iso_Loose;   //!
   TBranch        *b_lep_1_iso_LooseTrackOnly;   //!
   TBranch        *b_lep_1_m;   //!
   TBranch        *b_lep_1_muonAuthor;   //!
   TBranch        *b_lep_1_muonType;   //!
   TBranch        *b_lep_1_phi;   //!
   TBranch        *b_lep_1_pt;   //!
   TBranch        *b_lep_1_q;   //!
   TBranch        *b_lep_1_trk_d0;   //!
   TBranch        *b_lep_1_trk_d0_sig;   //!
   TBranch        *b_lep_1_trk_pt_error;   //!
   TBranch        *b_lep_1_trk_pvx_z0;   //!
   TBranch        *b_lep_1_trk_pvx_z0_sig;   //!
   TBranch        *b_lep_1_trk_pvx_z0_sintheta;   //!
   TBranch        *b_lep_1_trk_z0;   //!
   TBranch        *b_lep_1_trk_z0_sig;   //!
   TBranch        *b_lep_1_trk_z0_sintheta;   //!
   TBranch        *b_lephad;   //!
   TBranch        *b_lephad_coll_approx;   //!
   TBranch        *b_lephad_coll_approx_m;   //!
   TBranch        *b_lephad_coll_approx_x0;   //!
   TBranch        *b_lephad_coll_approx_x1;   //!
   TBranch        *b_lephad_cosalpha;   //!
   TBranch        *b_lephad_deta;   //!
   TBranch        *b_lephad_dphi;   //!
   TBranch        *b_lephad_dpt;   //!
   TBranch        *b_lephad_dr;   //!
   TBranch        *b_lephad_eta;   //!
   TBranch        *b_lephad_met_bisect;   //!
   TBranch        *b_lephad_met_centrality;   //!
   TBranch        *b_lephad_met_lep0_cos_dphi;   //!
   TBranch        *b_lephad_met_lep1_cos_dphi;   //!
   TBranch        *b_lephad_met_min_dphi;   //!
   TBranch        *b_lephad_met_sum_cos_dphi;   //!
   TBranch        *b_lephad_mt_lep0_met;   //!
   TBranch        *b_lephad_mt_lep1_met;   //!
   TBranch        *b_lephad_phi;   //!
   TBranch        *b_lephad_ptx;   //!
   TBranch        *b_lephad_pty;   //!
   TBranch        *b_lephad_qxq;   //!
   TBranch        *b_lephad_scal_sum_pt;   //!
   TBranch        *b_lephad_tauRec_coll_approx;   //!
   TBranch        *b_lephad_tauRec_coll_approx_m;   //!
   TBranch        *b_lephad_tauRec_coll_approx_x0;   //!
   TBranch        *b_lephad_tauRec_coll_approx_x1;   //!
   TBranch        *b_lephad_tauRec_cosalpha;   //!
   TBranch        *b_lephad_tauRec_deta;   //!
   TBranch        *b_lephad_tauRec_dphi;   //!
   TBranch        *b_lephad_tauRec_dpt;   //!
   TBranch        *b_lephad_tauRec_dr;   //!
   TBranch        *b_lephad_tauRec_met_bisect;   //!
   TBranch        *b_lephad_tauRec_met_centrality;   //!
   TBranch        *b_lephad_tauRec_met_lep0_cos_dphi;   //!
   TBranch        *b_lephad_tauRec_met_lep1_cos_dphi;   //!
   TBranch        *b_lephad_tauRec_met_min_dphi;   //!
   TBranch        *b_lephad_tauRec_met_sum_cos_phi;   //!
   TBranch        *b_lephad_tauRec_mt_lep0_met;   //!
   TBranch        *b_lephad_tauRec_mt_lep1_met;   //!
   TBranch        *b_lephad_tauRec_scal_sum_pt;   //!
   TBranch        *b_lephad_tauRec_vect_sum_pt;   //!
   TBranch        *b_lephad_tauRec_vis_mass;   //!
   TBranch        *b_lephad_vect_sum_pt;   //!
   TBranch        *b_lephad_vis_mass;   //!
   TBranch        *b_matched_jet_0_eta;   //!
   TBranch        *b_matched_jet_0_m;   //!
   TBranch        *b_matched_jet_0_pdgid;   //!
   TBranch        *b_matched_jet_0_phi;   //!
   TBranch        *b_matched_jet_0_pt;   //!
   TBranch        *b_mc_channel_number;   //!
   TBranch        *b_met_hpto_et;   //!
   TBranch        *b_met_hpto_etx;   //!
   TBranch        *b_met_hpto_ety;   //!
   TBranch        *b_met_hpto_phi;   //!
   TBranch        *b_met_more_met_et_ele;   //!
   TBranch        *b_met_more_met_et_jet;   //!
   TBranch        *b_met_more_met_et_muon;   //!
   TBranch        *b_met_more_met_et_pho;   //!
   TBranch        *b_met_more_met_et_soft;   //!
   TBranch        *b_met_more_met_et_tau;   //!
   TBranch        *b_met_more_met_phi_ele;   //!
   TBranch        *b_met_more_met_phi_jet;   //!
   TBranch        *b_met_more_met_phi_muon;   //!
   TBranch        *b_met_more_met_phi_pho;   //!
   TBranch        *b_met_more_met_phi_soft;   //!
   TBranch        *b_met_more_met_phi_tau;   //!
   TBranch        *b_met_more_met_sumet_ele;   //!
   TBranch        *b_met_more_met_sumet_jet;   //!
   TBranch        *b_met_more_met_sumet_muon;   //!
   TBranch        *b_met_more_met_sumet_pho;   //!
   TBranch        *b_met_more_met_sumet_soft;   //!
   TBranch        *b_met_more_met_sumet_tau;   //!
   TBranch        *b_met_reco_et;   //!
   TBranch        *b_met_reco_etx;   //!
   TBranch        *b_met_reco_ety;   //!
   TBranch        *b_met_reco_phi;   //!
   TBranch        *b_met_reco_sig;   //!
   TBranch        *b_met_reco_sig_tracks;   //!
   TBranch        *b_met_reco_sumet;   //!
   TBranch        *b_met_truth_et;   //!
   TBranch        *b_met_truth_etx;   //!
   TBranch        *b_met_truth_ety;   //!
   TBranch        *b_met_truth_phi;   //!
   TBranch        *b_met_truth_sig;   //!
   TBranch        *b_met_truth_sig_tracks;   //!
   TBranch        *b_met_truth_sumet;   //!
   TBranch        *b_muTrigMatch_0_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_muTrigMatch_0_HLT_mu24_imedium;   //!
   TBranch        *b_muTrigMatch_0_HLT_mu24_ivarmedium;   //!
   TBranch        *b_muTrigMatch_0_HLT_mu26_imedium;   //!
   TBranch        *b_muTrigMatch_0_HLT_mu26_ivarmedium;   //!
   TBranch        *b_muTrigMatch_0_HLT_mu40;   //!
   TBranch        *b_muTrigMatch_0_HLT_mu50;   //!
   TBranch        *b_muTrigMatch_0_trigger_matched;   //!
   TBranch        *b_n_actual_int;   //!
   TBranch        *b_n_avg_int;   //!
   TBranch        *b_n_avg_int_cor;   //!
   TBranch        *b_n_bjets;   //!
   TBranch        *b_n_electrons;   //!
   TBranch        *b_n_electrons_met;   //!
   TBranch        *b_n_electrons_olr;   //!
   TBranch        *b_n_jets;   //!
   TBranch        *b_n_jets_30;   //!
   TBranch        *b_n_jets_40;   //!
   TBranch        *b_n_jets_bad;   //!
   TBranch        *b_n_jets_mc_hs;   //!
   TBranch        *b_n_jets_met;   //!
   TBranch        *b_n_jets_olr;   //!
   TBranch        *b_n_muons;   //!
   TBranch        *b_n_muons_met;   //!
   TBranch        *b_n_muons_olr;   //!
   TBranch        *b_n_photons;   //!
   TBranch        *b_n_photons_met;   //!
   TBranch        *b_n_photons_olr;   //!
   TBranch        *b_n_pvx;   //!
   TBranch        *b_n_taus;   //!
   TBranch        *b_n_taus_loose;   //!
   TBranch        *b_n_taus_medium;   //!
   TBranch        *b_n_taus_met;   //!
   TBranch        *b_n_taus_olr;   //!
   TBranch        *b_n_taus_tight;   //!
   TBranch        *b_n_truth_gluon_jets;   //!
   TBranch        *b_n_truth_jets;   //!
   TBranch        *b_n_truth_jets_pt20_eta45;   //!
   TBranch        *b_n_truth_quark_jets;   //!
   TBranch        *b_n_vx;   //!
   TBranch        *b_run_number;   //!
   TBranch        *b_tau_0;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_JetBDTloose;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_JetBDTmedium;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_JetBDTtight;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_reco;   //!
   TBranch        *b_tau_0_NOMINAL_TauEffSF_selection;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDTPlusVeto_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDT_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDTPlusVeto_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDT_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_VeryLooseLlhEleOLR_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDTPlusVeto_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDT_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDTPlusVeto_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDT_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_VeryLooseLlhEleOLR_electron;   //!
   TBranch        *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTloose;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTmedium;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTtight;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTloose;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTmedium;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTtight;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTloose;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTmedium;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTtight;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTloose;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTmedium;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTtight;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco;   //!
   TBranch        *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection;   //!
   TBranch        *b_tau_0_allTrk_eta;   //!
   TBranch        *b_tau_0_allTrk_n;   //!
   TBranch        *b_tau_0_allTrk_phi;   //!
   TBranch        *b_tau_0_allTrk_pt;   //!
   TBranch        *b_tau_0_decay_mode;   //!
   TBranch        *b_tau_0_ele_BDTEleScoreTrans_run2;   //!
   TBranch        *b_tau_0_ele_bdt_loose;   //!
   TBranch        *b_tau_0_ele_bdt_medium;   //!
   TBranch        *b_tau_0_ele_bdt_score;   //!
   TBranch        *b_tau_0_ele_bdt_tight;   //!
   TBranch        *b_tau_0_ele_match_lhscore;   //!
   TBranch        *b_tau_0_ele_olr_pass;   //!
   TBranch        *b_tau_0_eleid_AbsDEtaLeadTrk;   //!
   TBranch        *b_tau_0_eleid_AbsDPhiLeadTrk;   //!
   TBranch        *b_tau_0_eleid_AbsEtaLeadTrk;   //!
   TBranch        *b_tau_0_eleid_EMFracAtEMScale;   //!
   TBranch        *b_tau_0_eleid_EMFracFixed;   //!
   TBranch        *b_tau_0_eleid_PSSFraction;   //!
   TBranch        *b_tau_0_eleid_centFrac;   //!
   TBranch        *b_tau_0_eleid_etHotShotWinOverPtLeadTrk;   //!
   TBranch        *b_tau_0_eleid_etOverPtLeadTrk;   //!
   TBranch        *b_tau_0_eleid_hadLeakEt;   //!
   TBranch        *b_tau_0_eleid_hadLeakFracFixed;   //!
   TBranch        *b_tau_0_eleid_isolFrac;   //!
   TBranch        *b_tau_0_eleid_leadTrackProbHT;   //!
   TBranch        *b_tau_0_eleid_secMaxStripEt;   //!
   TBranch        *b_tau_0_eleid_secMaxStripEtOverPtLeadTrk;   //!
   TBranch        *b_tau_0_et;   //!
   TBranch        *b_tau_0_eta;   //!
   TBranch        *b_tau_0_id_ChPiEMEOverCaloEME;   //!
   TBranch        *b_tau_0_id_EMPOverTrkSysP;   //!
   TBranch        *b_tau_0_id_SumPtTrkFrac;   //!
   TBranch        *b_tau_0_id_centFrac;   //!
   TBranch        *b_tau_0_id_dRmax;   //!
   TBranch        *b_tau_0_id_etOverPtLeadTrk;   //!
   TBranch        *b_tau_0_id_innerTrkAvgDist;   //!
   TBranch        *b_tau_0_id_ipSigLeadTrk;   //!
   TBranch        *b_tau_0_id_mEflowApprox;   //!
   TBranch        *b_tau_0_id_massTrkSys;   //!
   TBranch        *b_tau_0_id_ptIntermediateAxis;   //!
   TBranch        *b_tau_0_id_ptRatioEflowApprox;   //!
   TBranch        *b_tau_0_id_trFlightPathSig;   //!
   TBranch        *b_tau_0_jet_bdt_loose;   //!
   TBranch        *b_tau_0_jet_bdt_medium;   //!
   TBranch        *b_tau_0_jet_bdt_score;   //!
   TBranch        *b_tau_0_jet_bdt_score_trans;   //!
   TBranch        *b_tau_0_jet_bdt_tight;   //!
   TBranch        *b_tau_0_jet_width;   //!
   TBranch        *b_tau_0_leadTrk_d0;   //!
   TBranch        *b_tau_0_leadTrk_d0_sig;   //!
   TBranch        *b_tau_0_leadTrk_eta;   //!
   TBranch        *b_tau_0_leadTrk_phi;   //!
   TBranch        *b_tau_0_leadTrk_pt;   //!
   TBranch        *b_tau_0_leadTrk_pvx_z0;   //!
   TBranch        *b_tau_0_leadTrk_pvx_z0_sig;   //!
   TBranch        *b_tau_0_leadTrk_pvx_z0_sintheta;   //!
   TBranch        *b_tau_0_leadTrk_z0;   //!
   TBranch        *b_tau_0_leadTrk_z0_sig;   //!
   TBranch        *b_tau_0_leadTrk_z0_sintheta;   //!
   TBranch        *b_tau_0_m;   //!
   TBranch        *b_tau_0_mvx;   //!
   TBranch        *b_tau_0_mvx_tagged;   //!
   TBranch        *b_tau_0_n_all_tracks;   //!
   TBranch        *b_tau_0_n_charged_tracks;   //!
   TBranch        *b_tau_0_n_conversion_tracks;   //!
   TBranch        *b_tau_0_n_core_tracks;   //!
   TBranch        *b_tau_0_n_failTrackFilter_tracks;   //!
   TBranch        *b_tau_0_n_fake_tracks;   //!
   TBranch        *b_tau_0_n_isolation_tracks;   //!
   TBranch        *b_tau_0_n_modified_isolation_tracks;   //!
   TBranch        *b_tau_0_n_old_tracks;   //!
   TBranch        *b_tau_0_n_passTrkSelectionTight_tracks;   //!
   TBranch        *b_tau_0_n_passTrkSelector_tracks;   //!
   TBranch        *b_tau_0_n_unclassified_tracks;   //!
   TBranch        *b_tau_0_n_wide_tracks;   //!
   TBranch        *b_tau_0_phi;   //!
   TBranch        *b_tau_0_pt;   //!
   TBranch        *b_tau_0_q;   //!
   TBranch        *b_tau_0_sub_ConstituentBased_eta;   //!
   TBranch        *b_tau_0_sub_ConstituentBased_m;   //!
   TBranch        *b_tau_0_sub_ConstituentBased_phi;   //!
   TBranch        *b_tau_0_sub_ConstituentBased_pt;   //!
   TBranch        *b_tau_0_sub_PanTauCellBased_eta;   //!
   TBranch        *b_tau_0_sub_PanTauCellBased_m;   //!
   TBranch        *b_tau_0_sub_PanTauCellBased_phi;   //!
   TBranch        *b_tau_0_sub_PanTauCellBased_pt;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTValue_1p0n_vs_1p1n;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTValue_1p1n_vs_1pXn;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTValue_3p0n_vs_3pXn;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Basic_NNeutralConsts;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Charged_HLV_SumM;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Neutral_HLV_SumM;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts;   //!
   TBranch        *b_tau_0_sub_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed;   //!
   TBranch        *b_tau_0_sub_PanTau_DecayMode;   //!
   TBranch        *b_tau_0_sub_PanTau_DecayModeProto;   //!
   TBranch        *b_tau_0_sub_PanTau_isPanTauCandidate;   //!
   TBranch        *b_tau_0_sub_ParticleFlowCombined_eta;   //!
   TBranch        *b_tau_0_sub_ParticleFlowCombined_m;   //!
   TBranch        *b_tau_0_sub_ParticleFlowCombined_phi;   //!
   TBranch        *b_tau_0_sub_ParticleFlowCombined_pt;   //!
   TBranch        *b_tau_0_sub_ctrk0_cellBased_eta;   //!
   TBranch        *b_tau_0_sub_ctrk0_cellBased_phi;   //!
   TBranch        *b_tau_0_sub_ctrk0_cellBased_pt;   //!
   TBranch        *b_tau_0_sub_ctrk1_cellBased_eta;   //!
   TBranch        *b_tau_0_sub_ctrk1_cellBased_phi;   //!
   TBranch        *b_tau_0_sub_ctrk1_cellBased_pt;   //!
   TBranch        *b_tau_0_sub_ctrk2_cellBased_eta;   //!
   TBranch        *b_tau_0_sub_ctrk2_cellBased_phi;   //!
   TBranch        *b_tau_0_sub_ctrk2_cellBased_pt;   //!
   TBranch        *b_tau_0_sub_n_charged;   //!
   TBranch        *b_tau_0_sub_n_neutral;   //!
   TBranch        *b_tau_0_sub_neu0_bdtPi0Score;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_CENTER_LAMBDA;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_DELTA_PHI;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_DELTA_THETA;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_EM1CoreFrac;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_ENG_FRAC_CORE;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_ENG_FRAC_EM;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_ENG_FRAC_MAX;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_FIRST_ETA;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_LATERAL;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_LONGITUDINAL;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_NHitsInEM1;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_NPosECells_EM1;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_NPosECells_EM2;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_NPosECells_PS;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_SECOND_ENG_DENS;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_SECOND_LAMBDA;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_SECOND_R;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_asymmetryInEM1WRTTrk;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_energy_EM1;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_energy_EM2;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_eta;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM1;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM2;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_phi;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_pt;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM1;   //!
   TBranch        *b_tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM2;   //!
   TBranch        *b_tau_0_sub_neu1_bdtPi0Score;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_CENTER_LAMBDA;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_DELTA_PHI;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_DELTA_THETA;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_EM1CoreFrac;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_ENG_FRAC_CORE;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_ENG_FRAC_EM;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_ENG_FRAC_MAX;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_FIRST_ETA;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_LATERAL;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_LONGITUDINAL;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_NHitsInEM1;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_NPosECells_EM1;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_NPosECells_EM2;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_NPosECells_PS;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_SECOND_ENG_DENS;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_SECOND_LAMBDA;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_SECOND_R;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_asymmetryInEM1WRTTrk;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_energy_EM1;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_energy_EM2;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_eta;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM1;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM2;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_phi;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_pt;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM1;   //!
   TBranch        *b_tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM2;   //!
   TBranch        *b_tau_0_tauRec_eta;   //!
   TBranch        *b_tau_0_tauRec_m;   //!
   TBranch        *b_tau_0_tauRec_phi;   //!
   TBranch        *b_tau_0_tauRec_pt;   //!
   TBranch        *b_tau_0_trig1_HLT_ChPiEMEOverCaloEME;   //!
   TBranch        *b_tau_0_trig1_HLT_ChPiEMEOverCaloEMECorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_EMPOverTrkSysP;   //!
   TBranch        *b_tau_0_trig1_HLT_EMPOverTrkSysPCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_SumPtTrkFrac;   //!
   TBranch        *b_tau_0_trig1_HLT_SumPtTrkFracCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_centFrac;   //!
   TBranch        *b_tau_0_trig1_HLT_centFracCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_dRmax;   //!
   TBranch        *b_tau_0_trig1_HLT_dRmaxCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_eraw;   //!
   TBranch        *b_tau_0_trig1_HLT_etOverPtLeadTrk;   //!
   TBranch        *b_tau_0_trig1_HLT_etOverPtLeadTrkCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_eta;   //!
   TBranch        *b_tau_0_trig1_HLT_etraw;   //!
   TBranch        *b_tau_0_trig1_HLT_innerTrkAvgDist;   //!
   TBranch        *b_tau_0_trig1_HLT_innerTrkAvgDistCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_ipSigLeadTrk;   //!
   TBranch        *b_tau_0_trig1_HLT_ipSigLeadTrkCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_jet_bdt_loose;   //!
   TBranch        *b_tau_0_trig1_HLT_jet_bdt_medium;   //!
   TBranch        *b_tau_0_trig1_HLT_jet_bdt_score;   //!
   TBranch        *b_tau_0_trig1_HLT_jet_bdt_tight;   //!
   TBranch        *b_tau_0_trig1_HLT_m;   //!
   TBranch        *b_tau_0_trig1_HLT_mEflowApprox;   //!
   TBranch        *b_tau_0_trig1_HLT_mEflowApproxCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_massTrkSys;   //!
   TBranch        *b_tau_0_trig1_HLT_massTrkSysCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_matched;   //!
   TBranch        *b_tau_0_trig1_HLT_n_charged_tracks;   //!
   TBranch        *b_tau_0_trig1_HLT_n_isolation_tracks;   //!
   TBranch        *b_tau_0_trig1_HLT_phi;   //!
   TBranch        *b_tau_0_trig1_HLT_pt;   //!
   TBranch        *b_tau_0_trig1_HLT_ptRatioEflowApprox;   //!
   TBranch        *b_tau_0_trig1_HLT_ptRatioEflowApproxCorrected;   //!
   TBranch        *b_tau_0_trig1_HLT_trFlightPathSig;   //!
   TBranch        *b_tau_0_trig1_HLT_trFlightPathSigCorrected;   //!
   TBranch        *b_tau_0_trig2_PreselTrig_eta;   //!
   TBranch        *b_tau_0_trig2_PreselTrig_m;   //!
   TBranch        *b_tau_0_trig2_PreselTrig_matched;   //!
   TBranch        *b_tau_0_trig2_PreselTrig_n_charged_tracks;   //!
   TBranch        *b_tau_0_trig2_PreselTrig_n_isolation_tracks;   //!
   TBranch        *b_tau_0_trig2_PreselTrig_phi;   //!
   TBranch        *b_tau_0_trig2_PreselTrig_pt;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_iloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM;   //!
   TBranch        *b_tau_0_trig_HLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_idperf_track;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_idperf_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_loose1_ptonly;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_loose1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_medium1_mvonly;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_medium1_ptonly;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_medium1_track;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IL;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IT;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_perf_ptonly;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_perf_track;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_perf_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_tight1_ptonly;   //!
   TBranch        *b_tau_0_trig_HLT_tau25_tight1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_tau_0_trig_HLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_tau_0_trig_HLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_tau_0_trig_L1_J25;   //!
   TBranch        *b_tau_0_trig_L1_TAU12;   //!
   TBranch        *b_tau_0_trig_L1_TAU12IM;   //!
   TBranch        *b_tau_0_trig_L1_TAU20;   //!
   TBranch        *b_tau_0_trig_L1_TAU20IM;   //!
   TBranch        *b_tau_0_trig_trigger_matched;   //!
   TBranch        *b_tau_0_trk_multi_ckt4iso_n;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr60_d04_n_l5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr60_d04_n_lp5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr60_d04_n_t5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr60_n_l5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr60_n_lp5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr60_n_t5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr80_d04_n_l5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr80_d04_n_lp5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr80_d04_n_t5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr80_n_l5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr80_n_lp5;   //!
   TBranch        *b_tau_0_trk_multi_cw_dr80_n_t5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr60_d04_n_l5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr60_d04_n_lp5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr60_d04_n_t5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr60_n_l5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr60_n_lp5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr60_n_t5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr80_d04_n_l5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr80_d04_n_lp5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr80_d04_n_t5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr80_n_l5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr80_n_lp5;   //!
   TBranch        *b_tau_0_trk_multi_cws_dr80_n_t5;   //!
   TBranch        *b_tau_0_trk_multi_diag_n_had_tracks;   //!
   TBranch        *b_tau_0_trk_multi_diag_n_spu_cv_tracks;   //!
   TBranch        *b_tau_0_trk_multi_diag_n_spu_fk_tracks;   //!
   TBranch        *b_tau_0_trk_multi_diag_n_spu_pu_tracks;   //!
   TBranch        *b_tau_0_trk_multi_diag_n_spu_tracks;   //!
   TBranch        *b_tau_0_trk_multi_diag_n_spu_uc_tracks;   //!
   TBranch        *b_tau_0_trk_multi_diag_n_spu_ue_tracks;   //!
   TBranch        *b_tau_0_trk_multi_diag_n_tau_tracks;   //!
   TBranch        *b_tau_0_trk_multi_diag_purity;   //!
   TBranch        *b_tau_0_truth;   //!
   TBranch        *b_tau_0_truth_classifierParticleOrigin;   //!
   TBranch        *b_tau_0_truth_classifierParticleType;   //!
   TBranch        *b_tau_0_truth_decay_mode;   //!
   TBranch        *b_tau_0_truth_et;   //!
   TBranch        *b_tau_0_truth_eta;   //!
   TBranch        *b_tau_0_truth_eta_vis;   //!
   TBranch        *b_tau_0_truth_eta_vis_charged;   //!
   TBranch        *b_tau_0_truth_eta_vis_neutral;   //!
   TBranch        *b_tau_0_truth_isEle;   //!
   TBranch        *b_tau_0_truth_isHadTau;   //!
   TBranch        *b_tau_0_truth_isJet;   //!
   TBranch        *b_tau_0_truth_isMuon;   //!
   TBranch        *b_tau_0_truth_isTau;   //!
   TBranch        *b_tau_0_truth_m;   //!
   TBranch        *b_tau_0_truth_m_vis;   //!
   TBranch        *b_tau_0_truth_m_vis_charged;   //!
   TBranch        *b_tau_0_truth_m_vis_neutral;   //!
   TBranch        *b_tau_0_truth_mother_pdgId;   //!
   TBranch        *b_tau_0_truth_mother_status;   //!
   TBranch        *b_tau_0_truth_n_charged;   //!
   TBranch        *b_tau_0_truth_n_charged_pion;   //!
   TBranch        *b_tau_0_truth_n_neutral;   //!
   TBranch        *b_tau_0_truth_n_neutral_pion;   //!
   TBranch        *b_tau_0_truth_origin;   //!
   TBranch        *b_tau_0_truth_pdgId;   //!
   TBranch        *b_tau_0_truth_phi;   //!
   TBranch        *b_tau_0_truth_phi_vis;   //!
   TBranch        *b_tau_0_truth_phi_vis_charged;   //!
   TBranch        *b_tau_0_truth_phi_vis_neutral;   //!
   TBranch        *b_tau_0_truth_pt;   //!
   TBranch        *b_tau_0_truth_pt_vis;   //!
   TBranch        *b_tau_0_truth_pt_vis_charged;   //!
   TBranch        *b_tau_0_truth_pt_vis_neutral;   //!
   TBranch        *b_tau_0_truth_pz;   //!
   TBranch        *b_tau_0_truth_q;   //!
   TBranch        *b_tau_0_truth_status;   //!
   TBranch        *b_tau_0_truth_type;   //!
   TBranch        *b_tau_1;   //!
   TBranch        *b_tau_1_allTrk_eta;   //!
   TBranch        *b_tau_1_allTrk_n;   //!
   TBranch        *b_tau_1_allTrk_phi;   //!
   TBranch        *b_tau_1_allTrk_pt;   //!
   TBranch        *b_tau_1_decay_mode;   //!
   TBranch        *b_tau_1_ele_BDTEleScoreTrans_run2;   //!
   TBranch        *b_tau_1_ele_bdt_loose;   //!
   TBranch        *b_tau_1_ele_bdt_medium;   //!
   TBranch        *b_tau_1_ele_bdt_score;   //!
   TBranch        *b_tau_1_ele_bdt_tight;   //!
   TBranch        *b_tau_1_ele_match_lhscore;   //!
   TBranch        *b_tau_1_ele_olr_pass;   //!
   TBranch        *b_tau_1_et;   //!
   TBranch        *b_tau_1_eta;   //!
   TBranch        *b_tau_1_jet_bdt_loose;   //!
   TBranch        *b_tau_1_jet_bdt_medium;   //!
   TBranch        *b_tau_1_jet_bdt_score;   //!
   TBranch        *b_tau_1_jet_bdt_score_trans;   //!
   TBranch        *b_tau_1_jet_bdt_tight;   //!
   TBranch        *b_tau_1_jet_width;   //!
   TBranch        *b_tau_1_leadTrk_eta;   //!
   TBranch        *b_tau_1_leadTrk_phi;   //!
   TBranch        *b_tau_1_leadTrk_pt;   //!
   TBranch        *b_tau_1_m;   //!
   TBranch        *b_tau_1_mvx;   //!
   TBranch        *b_tau_1_mvx_tagged;   //!
   TBranch        *b_tau_1_n_all_tracks;   //!
   TBranch        *b_tau_1_n_charged_tracks;   //!
   TBranch        *b_tau_1_n_conversion_tracks;   //!
   TBranch        *b_tau_1_n_core_tracks;   //!
   TBranch        *b_tau_1_n_failTrackFilter_tracks;   //!
   TBranch        *b_tau_1_n_fake_tracks;   //!
   TBranch        *b_tau_1_n_isolation_tracks;   //!
   TBranch        *b_tau_1_n_modified_isolation_tracks;   //!
   TBranch        *b_tau_1_n_old_tracks;   //!
   TBranch        *b_tau_1_n_passTrkSelectionTight_tracks;   //!
   TBranch        *b_tau_1_n_passTrkSelector_tracks;   //!
   TBranch        *b_tau_1_n_unclassified_tracks;   //!
   TBranch        *b_tau_1_n_wide_tracks;   //!
   TBranch        *b_tau_1_phi;   //!
   TBranch        *b_tau_1_pt;   //!
   TBranch        *b_tau_1_q;   //!
   TBranch        *b_tau_1_tauRec_eta;   //!
   TBranch        *b_tau_1_tauRec_m;   //!
   TBranch        *b_tau_1_tauRec_phi;   //!
   TBranch        *b_tau_1_tauRec_pt;   //!
   TBranch        *b_tau_tes_alpha_pt_shift;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_total;   //!

   AF2Tree(TTree *tree=0);
   virtual ~AF2Tree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   int treeVersion;
};

#endif

AF2Tree::AF2Tree(TTree *tree, int version)  
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   mc_channel_number = 0;
   //set mc_channel_number to 0 initially to seprate data and mc;
   fChain = 0;
   treeVersion = version;
   Init(tree);
}

AF2Tree::~AF2Tree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t AF2Tree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t AF2Tree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void AF2Tree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("HLT_mu14_iloose_tau25_medium1_tracktwo", &HLT_mu14_iloose_tau25_medium1_tracktwo, &b_HLT_mu14_iloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected", &HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected, &b_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected);
   fChain->SetBranchAddress("HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected", &HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected, &b_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected);
   fChain->SetBranchAddress("HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected", &HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected, &b_HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected);
   fChain->SetBranchAddress("HLT_mu14_iloose_tau25_medium1_tracktwo_resurrected", &HLT_mu14_iloose_tau25_medium1_tracktwo_resurrected, &b_HLT_mu14_iloose_tau25_medium1_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_mu14_iloose_tau35_medium1_tracktwo", &HLT_mu14_iloose_tau35_medium1_tracktwo, &b_HLT_mu14_iloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("HLT_mu14_ivarloose_tau25_medium1_tracktwo", &HLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_HLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected", &HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected, &b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25_resurrected);
   fChain->SetBranchAddress("HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected", &HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected, &b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_resurrected);
   fChain->SetBranchAddress("HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected", &HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected, &b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_resurrected);
   fChain->SetBranchAddress("HLT_mu14_ivarloose_tau25_medium1_tracktwo_resurrected", &HLT_mu14_ivarloose_tau25_medium1_tracktwo_resurrected, &b_HLT_mu14_ivarloose_tau25_medium1_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_mu14_ivarloose_tau35_medium1_tracktwo", &HLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_HLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("HLT_mu14_tau25_medium1_tracktwo", &HLT_mu14_tau25_medium1_tracktwo, &b_HLT_mu14_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu24_imedium", &HLT_mu24_imedium, &b_HLT_mu24_imedium);
   fChain->SetBranchAddress("HLT_mu24_ivarmedium", &HLT_mu24_ivarmedium, &b_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("HLT_mu26_imedium", &HLT_mu26_imedium, &b_HLT_mu26_imedium);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_mu40", &HLT_mu40, &b_HLT_mu40);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_tau125_medium1_tracktwo_resurrected", &HLT_tau125_medium1_tracktwo_resurrected, &b_HLT_tau125_medium1_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_tau160_medium1_tracktwo_resurrected", &HLT_tau160_medium1_tracktwo_resurrected, &b_HLT_tau160_medium1_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_tau25_idperf_track_resurrected", &HLT_tau25_idperf_track_resurrected, &b_HLT_tau25_idperf_track_resurrected);
   fChain->SetBranchAddress("HLT_tau25_idperf_tracktwo_resurrected", &HLT_tau25_idperf_tracktwo_resurrected, &b_HLT_tau25_idperf_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_tau25_loose1_ptonly_resurrected", &HLT_tau25_loose1_ptonly_resurrected, &b_HLT_tau25_loose1_ptonly_resurrected);
   fChain->SetBranchAddress("HLT_tau25_loose1_tracktwo_resurrected", &HLT_tau25_loose1_tracktwo_resurrected, &b_HLT_tau25_loose1_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_tau25_medium1_mvonly_resurrected", &HLT_tau25_medium1_mvonly_resurrected, &b_HLT_tau25_medium1_mvonly_resurrected);
   fChain->SetBranchAddress("HLT_tau25_medium1_ptonly_resurrected", &HLT_tau25_medium1_ptonly_resurrected, &b_HLT_tau25_medium1_ptonly_resurrected);
   fChain->SetBranchAddress("HLT_tau25_medium1_track_resurrected", &HLT_tau25_medium1_track_resurrected, &b_HLT_tau25_medium1_track_resurrected);
   fChain->SetBranchAddress("HLT_tau25_medium1_tracktwo_L1TAU12IL_resurrected", &HLT_tau25_medium1_tracktwo_L1TAU12IL_resurrected, &b_HLT_tau25_medium1_tracktwo_L1TAU12IL_resurrected);
   fChain->SetBranchAddress("HLT_tau25_medium1_tracktwo_L1TAU12IT_resurrected", &HLT_tau25_medium1_tracktwo_L1TAU12IT_resurrected, &b_HLT_tau25_medium1_tracktwo_L1TAU12IT_resurrected);
   fChain->SetBranchAddress("HLT_tau25_medium1_tracktwo_L1TAU12_resurrected", &HLT_tau25_medium1_tracktwo_L1TAU12_resurrected, &b_HLT_tau25_medium1_tracktwo_L1TAU12_resurrected);
   fChain->SetBranchAddress("HLT_tau25_medium1_tracktwo_resurrected", &HLT_tau25_medium1_tracktwo_resurrected, &b_HLT_tau25_medium1_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_tau25_perf_ptonly_resurrected", &HLT_tau25_perf_ptonly_resurrected, &b_HLT_tau25_perf_ptonly_resurrected);
   fChain->SetBranchAddress("HLT_tau25_perf_track_resurrected", &HLT_tau25_perf_track_resurrected, &b_HLT_tau25_perf_track_resurrected);
   fChain->SetBranchAddress("HLT_tau25_perf_tracktwo_resurrected", &HLT_tau25_perf_tracktwo_resurrected, &b_HLT_tau25_perf_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_tau25_tight1_ptonly_resurrected", &HLT_tau25_tight1_ptonly_resurrected, &b_HLT_tau25_tight1_ptonly_resurrected);
   fChain->SetBranchAddress("HLT_tau25_tight1_tracktwo_resurrected", &HLT_tau25_tight1_tracktwo_resurrected, &b_HLT_tau25_tight1_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_tau35_medium1_tracktwo_resurrected", &HLT_tau35_medium1_tracktwo_resurrected, &b_HLT_tau35_medium1_tracktwo_resurrected);
   fChain->SetBranchAddress("HLT_tau50_medium1_tracktwo_L1TAU12_resurrected", &HLT_tau50_medium1_tracktwo_L1TAU12_resurrected, &b_HLT_tau50_medium1_tracktwo_L1TAU12_resurrected);
   fChain->SetBranchAddress("HLT_tau80_medium1_tracktwo_L1TAU60_resurrected", &HLT_tau80_medium1_tracktwo_L1TAU60_resurrected, &b_HLT_tau80_medium1_tracktwo_L1TAU60_resurrected);
   fChain->SetBranchAddress("HLT_tau80_medium1_tracktwo_resurrected", &HLT_tau80_medium1_tracktwo_resurrected, &b_HLT_tau80_medium1_tracktwo_resurrected);
   fChain->SetBranchAddress("L1_J25", &L1_J25, &b_L1_J25);
   fChain->SetBranchAddress("L1_J25_resurrected", &L1_J25_resurrected, &b_L1_J25_resurrected);
   fChain->SetBranchAddress("L1_TAU12IM_resurrected", &L1_TAU12IM_resurrected, &b_L1_TAU12IM_resurrected);
   fChain->SetBranchAddress("L1_TAU12_resurrected", &L1_TAU12_resurrected, &b_L1_TAU12_resurrected);
   fChain->SetBranchAddress("L1_TAU20IM_resurrected", &L1_TAU20IM_resurrected, &b_L1_TAU20IM_resurrected);
   fChain->SetBranchAddress("L1_TAU20_resurrected", &L1_TAU20_resurrected, &b_L1_TAU20_resurrected);
   fChain->SetBranchAddress("NOMINAL_pileup_combined_weight", &NOMINAL_pileup_combined_weight, &b_NOMINAL_pileup_combined_weight);
   fChain->SetBranchAddress("NOMINAL_pileup_random_run_number", &NOMINAL_pileup_random_run_number, &b_NOMINAL_pileup_random_run_number);
   fChain->SetBranchAddress("PRW_DATASF_1down_pileup_combined_weight", &PRW_DATASF_1down_pileup_combined_weight, &b_PRW_DATASF_1down_pileup_combined_weight);
   fChain->SetBranchAddress("PRW_DATASF_1down_pileup_random_run_number", &PRW_DATASF_1down_pileup_random_run_number, &b_PRW_DATASF_1down_pileup_random_run_number);
   fChain->SetBranchAddress("PRW_DATASF_1up_pileup_combined_weight", &PRW_DATASF_1up_pileup_combined_weight, &b_PRW_DATASF_1up_pileup_combined_weight);
   fChain->SetBranchAddress("PRW_DATASF_1up_pileup_random_run_number", &PRW_DATASF_1up_pileup_random_run_number, &b_PRW_DATASF_1up_pileup_random_run_number);
   fChain->SetBranchAddress("dilep_m", &dilep_m, &b_dilep_m);
   fChain->SetBranchAddress("event_clean_detector_core", &event_clean_detector_core, &b_event_clean_detector_core);
   fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
   fChain->SetBranchAddress("jet_0", &jet_0, &b_jet_0);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_0_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_B_0_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_B_0_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_0_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_B_0_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_B_0_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_0_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_B_0_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_B_0_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_0_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_B_0_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_B_0_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_1_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_B_1_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_B_1_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_1_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_B_1_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_B_1_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_1_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_B_1_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_B_1_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_1_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_B_1_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_B_1_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_2_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_B_2_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_B_2_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_2_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_B_2_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_B_2_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_2_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_B_2_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_B_2_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_B_2_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_B_2_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_B_2_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_0_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_C_0_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_C_0_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_0_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_C_0_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_C_0_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_0_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_C_0_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_C_0_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_0_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_C_0_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_C_0_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_1_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_C_1_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_C_1_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_1_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_C_1_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_C_1_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_1_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_C_1_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_C_1_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_1_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_C_1_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_C_1_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_2_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_C_2_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_C_2_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_2_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_C_2_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_C_2_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_2_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_C_2_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_C_2_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_C_2_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_C_2_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_C_2_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_0_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_0_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_0_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_0_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_0_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_0_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_0_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_0_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_0_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_0_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_0_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_0_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_1_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_1_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_1_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_1_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_1_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_1_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_1_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_1_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_1_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_1_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_1_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_1_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_2_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_2_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_2_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_2_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_2_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_2_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_2_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_2_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_2_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_2_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_2_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_2_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_3_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_3_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_3_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_3_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_3_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_3_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_3_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_3_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_3_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_3_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_3_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_3_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_4_1down_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_4_1down_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_4_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_4_1down_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_4_1down_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_4_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_4_1up_effSF_MVX", &jet_0_FT_EFF_Eigen_Light_4_1up_effSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_4_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_Eigen_Light_4_1up_ineffSF_MVX", &jet_0_FT_EFF_Eigen_Light_4_1up_ineffSF_MVX, &b_jet_0_FT_EFF_Eigen_Light_4_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_extrapolation_1down_effSF_MVX", &jet_0_FT_EFF_extrapolation_1down_effSF_MVX, &b_jet_0_FT_EFF_extrapolation_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_extrapolation_1down_ineffSF_MVX", &jet_0_FT_EFF_extrapolation_1down_ineffSF_MVX, &b_jet_0_FT_EFF_extrapolation_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_extrapolation_1up_effSF_MVX", &jet_0_FT_EFF_extrapolation_1up_effSF_MVX, &b_jet_0_FT_EFF_extrapolation_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_extrapolation_1up_ineffSF_MVX", &jet_0_FT_EFF_extrapolation_1up_ineffSF_MVX, &b_jet_0_FT_EFF_extrapolation_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_extrapolation_from_charm_1down_effSF_MVX", &jet_0_FT_EFF_extrapolation_from_charm_1down_effSF_MVX, &b_jet_0_FT_EFF_extrapolation_from_charm_1down_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_extrapolation_from_charm_1down_ineffSF_MVX", &jet_0_FT_EFF_extrapolation_from_charm_1down_ineffSF_MVX, &b_jet_0_FT_EFF_extrapolation_from_charm_1down_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_extrapolation_from_charm_1up_effSF_MVX", &jet_0_FT_EFF_extrapolation_from_charm_1up_effSF_MVX, &b_jet_0_FT_EFF_extrapolation_from_charm_1up_effSF_MVX);
   fChain->SetBranchAddress("jet_0_FT_EFF_extrapolation_from_charm_1up_ineffSF_MVX", &jet_0_FT_EFF_extrapolation_from_charm_1up_ineffSF_MVX, &b_jet_0_FT_EFF_extrapolation_from_charm_1up_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_JET_JvtEfficiency_1down_central_jets_effSF_JVT", &jet_0_JET_JvtEfficiency_1down_central_jets_effSF_JVT, &b_jet_0_JET_JvtEfficiency_1down_central_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_JvtEfficiency_1down_central_jets_ineffSF_JVT", &jet_0_JET_JvtEfficiency_1down_central_jets_ineffSF_JVT, &b_jet_0_JET_JvtEfficiency_1down_central_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_JvtEfficiency_1down_forward_jets_effSF_JVT", &jet_0_JET_JvtEfficiency_1down_forward_jets_effSF_JVT, &b_jet_0_JET_JvtEfficiency_1down_forward_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_JvtEfficiency_1down_forward_jets_ineffSF_JVT", &jet_0_JET_JvtEfficiency_1down_forward_jets_ineffSF_JVT, &b_jet_0_JET_JvtEfficiency_1down_forward_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_JvtEfficiency_1up_central_jets_effSF_JVT", &jet_0_JET_JvtEfficiency_1up_central_jets_effSF_JVT, &b_jet_0_JET_JvtEfficiency_1up_central_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_JvtEfficiency_1up_central_jets_ineffSF_JVT", &jet_0_JET_JvtEfficiency_1up_central_jets_ineffSF_JVT, &b_jet_0_JET_JvtEfficiency_1up_central_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_JvtEfficiency_1up_forward_jets_effSF_JVT", &jet_0_JET_JvtEfficiency_1up_forward_jets_effSF_JVT, &b_jet_0_JET_JvtEfficiency_1up_forward_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_JvtEfficiency_1up_forward_jets_ineffSF_JVT", &jet_0_JET_JvtEfficiency_1up_forward_jets_ineffSF_JVT, &b_jet_0_JET_JvtEfficiency_1up_forward_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_fJvtEfficiency_1down_central_jets_effSF_JVT", &jet_0_JET_fJvtEfficiency_1down_central_jets_effSF_JVT, &b_jet_0_JET_fJvtEfficiency_1down_central_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_fJvtEfficiency_1down_central_jets_ineffSF_JVT", &jet_0_JET_fJvtEfficiency_1down_central_jets_ineffSF_JVT, &b_jet_0_JET_fJvtEfficiency_1down_central_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_fJvtEfficiency_1down_forward_jets_effSF_JVT", &jet_0_JET_fJvtEfficiency_1down_forward_jets_effSF_JVT, &b_jet_0_JET_fJvtEfficiency_1down_forward_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_fJvtEfficiency_1down_forward_jets_ineffSF_JVT", &jet_0_JET_fJvtEfficiency_1down_forward_jets_ineffSF_JVT, &b_jet_0_JET_fJvtEfficiency_1down_forward_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_fJvtEfficiency_1up_central_jets_effSF_JVT", &jet_0_JET_fJvtEfficiency_1up_central_jets_effSF_JVT, &b_jet_0_JET_fJvtEfficiency_1up_central_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_fJvtEfficiency_1up_central_jets_ineffSF_JVT", &jet_0_JET_fJvtEfficiency_1up_central_jets_ineffSF_JVT, &b_jet_0_JET_fJvtEfficiency_1up_central_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_fJvtEfficiency_1up_forward_jets_effSF_JVT", &jet_0_JET_fJvtEfficiency_1up_forward_jets_effSF_JVT, &b_jet_0_JET_fJvtEfficiency_1up_forward_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_JET_fJvtEfficiency_1up_forward_jets_ineffSF_JVT", &jet_0_JET_fJvtEfficiency_1up_forward_jets_ineffSF_JVT, &b_jet_0_JET_fJvtEfficiency_1up_forward_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_NOMINAL_central_jets_effSF_JVT", &jet_0_NOMINAL_central_jets_effSF_JVT, &b_jet_0_NOMINAL_central_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_NOMINAL_central_jets_ineffSF_JVT", &jet_0_NOMINAL_central_jets_ineffSF_JVT, &b_jet_0_NOMINAL_central_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_NOMINAL_effSF_MVX", &jet_0_NOMINAL_effSF_MVX, &b_jet_0_NOMINAL_effSF_MVX);
   fChain->SetBranchAddress("jet_0_NOMINAL_forward_jets_effSF_JVT", &jet_0_NOMINAL_forward_jets_effSF_JVT, &b_jet_0_NOMINAL_forward_jets_effSF_JVT);
   fChain->SetBranchAddress("jet_0_NOMINAL_forward_jets_ineffSF_JVT", &jet_0_NOMINAL_forward_jets_ineffSF_JVT, &b_jet_0_NOMINAL_forward_jets_ineffSF_JVT);
   fChain->SetBranchAddress("jet_0_NOMINAL_ineffSF_MVX", &jet_0_NOMINAL_ineffSF_MVX, &b_jet_0_NOMINAL_ineffSF_MVX);
   fChain->SetBranchAddress("jet_0_et", &jet_0_et, &b_jet_0_et);
   fChain->SetBranchAddress("jet_0_eta", &jet_0_eta, &b_jet_0_eta);
   fChain->SetBranchAddress("jet_0_fjvt", &jet_0_fjvt, &b_jet_0_fjvt);
   fChain->SetBranchAddress("jet_0_flavorlabel", &jet_0_flavorlabel, &b_jet_0_flavorlabel);
   fChain->SetBranchAddress("jet_0_flavorlabel_cone", &jet_0_flavorlabel_cone, &b_jet_0_flavorlabel_cone);
   fChain->SetBranchAddress("jet_0_flavorlabel_part", &jet_0_flavorlabel_part, &b_jet_0_flavorlabel_part);
   fChain->SetBranchAddress("jet_0_is_Jvt_HS", &jet_0_is_Jvt_HS, &b_jet_0_is_Jvt_HS);
   fChain->SetBranchAddress("jet_0_jvt", &jet_0_jvt, &b_jet_0_jvt);
   fChain->SetBranchAddress("jet_0_m", &jet_0_m, &b_jet_0_m);
   fChain->SetBranchAddress("jet_0_mvx", &jet_0_mvx, &b_jet_0_mvx);
   fChain->SetBranchAddress("jet_0_mvx_tagged", &jet_0_mvx_tagged, &b_jet_0_mvx_tagged);
   fChain->SetBranchAddress("jet_0_mvx_tagweight", &jet_0_mvx_tagweight, &b_jet_0_mvx_tagweight);
   fChain->SetBranchAddress("jet_0_phi", &jet_0_phi, &b_jet_0_phi);
   fChain->SetBranchAddress("jet_0_pt", &jet_0_pt, &b_jet_0_pt);
   fChain->SetBranchAddress("jet_0_q", &jet_0_q, &b_jet_0_q);
   fChain->SetBranchAddress("jet_0_width", &jet_0_width, &b_jet_0_width);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_B_0_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_B_0_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_B_0_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_B_0_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_B_1_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_B_1_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_B_1_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_B_1_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_2_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_B_2_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_B_2_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_2_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_B_2_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_B_2_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_C_0_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_C_0_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_C_0_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_C_0_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_C_1_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_C_1_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_C_1_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_C_1_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_2_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_C_2_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_C_2_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_2_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_C_2_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_C_2_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_0_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_0_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_0_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_0_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_1_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_1_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_1_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_1_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_2_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_2_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_2_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_2_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_2_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_2_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_3_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_3_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_3_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_3_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_3_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_3_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_4_1down_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_4_1down_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_4_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_4_1up_global_effSF_MVX", &jet_FT_EFF_Eigen_Light_4_1up_global_effSF_MVX, &b_jet_FT_EFF_Eigen_Light_4_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_MVX", &jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_MVX, &b_jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_extrapolation_1down_global_effSF_MVX", &jet_FT_EFF_extrapolation_1down_global_effSF_MVX, &b_jet_FT_EFF_extrapolation_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_extrapolation_1down_global_ineffSF_MVX", &jet_FT_EFF_extrapolation_1down_global_ineffSF_MVX, &b_jet_FT_EFF_extrapolation_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_extrapolation_1up_global_effSF_MVX", &jet_FT_EFF_extrapolation_1up_global_effSF_MVX, &b_jet_FT_EFF_extrapolation_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_extrapolation_1up_global_ineffSF_MVX", &jet_FT_EFF_extrapolation_1up_global_ineffSF_MVX, &b_jet_FT_EFF_extrapolation_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_MVX", &jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_MVX, &b_jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_MVX", &jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_MVX, &b_jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_MVX", &jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_MVX, &b_jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_MVX);
   fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_MVX", &jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_MVX, &b_jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_MVX);
   fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT", &jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT, &b_jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT", &jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT, &b_jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_forward_jets_global_effSF_JVT", &jet_JET_JvtEfficiency_1down_forward_jets_global_effSF_JVT, &b_jet_JET_JvtEfficiency_1down_forward_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_forward_jets_global_ineffSF_JVT", &jet_JET_JvtEfficiency_1down_forward_jets_global_ineffSF_JVT, &b_jet_JET_JvtEfficiency_1down_forward_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT", &jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT, &b_jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT", &jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT, &b_jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_forward_jets_global_effSF_JVT", &jet_JET_JvtEfficiency_1up_forward_jets_global_effSF_JVT, &b_jet_JET_JvtEfficiency_1up_forward_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_forward_jets_global_ineffSF_JVT", &jet_JET_JvtEfficiency_1up_forward_jets_global_ineffSF_JVT, &b_jet_JET_JvtEfficiency_1up_forward_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_central_jets_global_effSF_JVT", &jet_JET_fJvtEfficiency_1down_central_jets_global_effSF_JVT, &b_jet_JET_fJvtEfficiency_1down_central_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_central_jets_global_ineffSF_JVT", &jet_JET_fJvtEfficiency_1down_central_jets_global_ineffSF_JVT, &b_jet_JET_fJvtEfficiency_1down_central_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT", &jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT, &b_jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT", &jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT, &b_jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_central_jets_global_effSF_JVT", &jet_JET_fJvtEfficiency_1up_central_jets_global_effSF_JVT, &b_jet_JET_fJvtEfficiency_1up_central_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_central_jets_global_ineffSF_JVT", &jet_JET_fJvtEfficiency_1up_central_jets_global_ineffSF_JVT, &b_jet_JET_fJvtEfficiency_1up_central_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT", &jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT, &b_jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT", &jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT, &b_jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_effSF_JVT", &jet_NOMINAL_central_jets_global_effSF_JVT, &b_jet_NOMINAL_central_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_ineffSF_JVT", &jet_NOMINAL_central_jets_global_ineffSF_JVT, &b_jet_NOMINAL_central_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_effSF_JVT", &jet_NOMINAL_forward_jets_global_effSF_JVT, &b_jet_NOMINAL_forward_jets_global_effSF_JVT);
   fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_ineffSF_JVT", &jet_NOMINAL_forward_jets_global_ineffSF_JVT, &b_jet_NOMINAL_forward_jets_global_ineffSF_JVT);
   fChain->SetBranchAddress("jet_NOMINAL_global_effSF_MVX", &jet_NOMINAL_global_effSF_MVX, &b_jet_NOMINAL_global_effSF_MVX);
   fChain->SetBranchAddress("jet_NOMINAL_global_ineffSF_MVX", &jet_NOMINAL_global_ineffSF_MVX, &b_jet_NOMINAL_global_ineffSF_MVX);
   fChain->SetBranchAddress("lep_0", &lep_0, &b_lep_0);
   fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient", &lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient, &b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose", &lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose, &b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient", &lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient, &b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose", &lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose, &b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient", &lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient, &b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose", &lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose, &b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient", &lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient, &b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose", &lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose, &b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium, &b_lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium, &b_lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium, &b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium, &b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium, &b_lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium, &b_lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium, &b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium, &b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA, &b_lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA, &b_lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA, &b_lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA, &b_lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient", &lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient, &b_lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient);
   fChain->SetBranchAddress("lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose", &lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose, &b_lep_0_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose);
   fChain->SetBranchAddress("lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient", &lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient, &b_lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient);
   fChain->SetBranchAddress("lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose", &lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose, &b_lep_0_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose);
   fChain->SetBranchAddress("lep_0_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11", &lep_0_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11, &b_lep_0_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11);
   fChain->SetBranchAddress("lep_0_NOMINAL_EleEffSF_offline_RecoTrk", &lep_0_NOMINAL_EleEffSF_offline_RecoTrk, &b_lep_0_NOMINAL_EleEffSF_offline_RecoTrk);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_IsoGradient", &lep_0_NOMINAL_MuEffSF_IsoGradient, &b_lep_0_NOMINAL_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_IsoLoose", &lep_0_NOMINAL_MuEffSF_IsoLoose, &b_lep_0_NOMINAL_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_Reco_QualMedium", &lep_0_NOMINAL_MuEffSF_Reco_QualMedium, &b_lep_0_NOMINAL_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_TTVA", &lep_0_NOMINAL_MuEffSF_TTVA, &b_lep_0_NOMINAL_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient", &lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient, &b_lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient);
   fChain->SetBranchAddress("lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose", &lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose, &b_lep_0_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose);
   fChain->SetBranchAddress("lep_0_cluster_eta", &lep_0_cluster_eta, &b_lep_0_cluster_eta);
   fChain->SetBranchAddress("lep_0_cluster_eta_be2", &lep_0_cluster_eta_be2, &b_lep_0_cluster_eta_be2);
   fChain->SetBranchAddress("lep_0_et", &lep_0_et, &b_lep_0_et);
   fChain->SetBranchAddress("lep_0_eta", &lep_0_eta, &b_lep_0_eta);
   fChain->SetBranchAddress("lep_0_id_bad", &lep_0_id_bad, &b_lep_0_id_bad);
   fChain->SetBranchAddress("lep_0_id_loose", &lep_0_id_loose, &b_lep_0_id_loose);
   fChain->SetBranchAddress("lep_0_id_medium", &lep_0_id_medium, &b_lep_0_id_medium);
   fChain->SetBranchAddress("lep_0_id_tight", &lep_0_id_tight, &b_lep_0_id_tight);
   fChain->SetBranchAddress("lep_0_id_veryloose", &lep_0_id_veryloose, &b_lep_0_id_veryloose);
   fChain->SetBranchAddress("lep_0_iso_FixedCutHighPtCaloOnly", &lep_0_iso_FixedCutHighPtCaloOnly, &b_lep_0_iso_FixedCutHighPtCaloOnly);
   fChain->SetBranchAddress("lep_0_iso_FixedCutHighPtTrackOnly", &lep_0_iso_FixedCutHighPtTrackOnly, &b_lep_0_iso_FixedCutHighPtTrackOnly);
   fChain->SetBranchAddress("lep_0_iso_FixedCutLoose", &lep_0_iso_FixedCutLoose, &b_lep_0_iso_FixedCutLoose);
   fChain->SetBranchAddress("lep_0_iso_FixedCutTight", &lep_0_iso_FixedCutTight, &b_lep_0_iso_FixedCutTight);
   fChain->SetBranchAddress("lep_0_iso_FixedCutTightTrackOnly", &lep_0_iso_FixedCutTightTrackOnly, &b_lep_0_iso_FixedCutTightTrackOnly);
   fChain->SetBranchAddress("lep_0_iso_FixedCutTrackCone40", &lep_0_iso_FixedCutTrackCone40, &b_lep_0_iso_FixedCutTrackCone40);
   fChain->SetBranchAddress("lep_0_iso_Gradient", &lep_0_iso_Gradient, &b_lep_0_iso_Gradient);
   fChain->SetBranchAddress("lep_0_iso_GradientLoose", &lep_0_iso_GradientLoose, &b_lep_0_iso_GradientLoose);
   fChain->SetBranchAddress("lep_0_iso_Loose", &lep_0_iso_Loose, &b_lep_0_iso_Loose);
   fChain->SetBranchAddress("lep_0_iso_LooseTrackOnly", &lep_0_iso_LooseTrackOnly, &b_lep_0_iso_LooseTrackOnly);
   fChain->SetBranchAddress("lep_0_iso_etcone20", &lep_0_iso_etcone20, &b_lep_0_iso_etcone20);
   fChain->SetBranchAddress("lep_0_iso_etcone30", &lep_0_iso_etcone30, &b_lep_0_iso_etcone30);
   fChain->SetBranchAddress("lep_0_iso_etcone40", &lep_0_iso_etcone40, &b_lep_0_iso_etcone40);
   fChain->SetBranchAddress("lep_0_iso_ptcone20", &lep_0_iso_ptcone20, &b_lep_0_iso_ptcone20);
   fChain->SetBranchAddress("lep_0_iso_ptcone30", &lep_0_iso_ptcone30, &b_lep_0_iso_ptcone30);
   fChain->SetBranchAddress("lep_0_iso_ptcone40", &lep_0_iso_ptcone40, &b_lep_0_iso_ptcone40);
   fChain->SetBranchAddress("lep_0_iso_ptvarcone20", &lep_0_iso_ptvarcone20, &b_lep_0_iso_ptvarcone20);
   fChain->SetBranchAddress("lep_0_iso_ptvarcone30", &lep_0_iso_ptvarcone30, &b_lep_0_iso_ptvarcone30);
   fChain->SetBranchAddress("lep_0_iso_ptvarcone40", &lep_0_iso_ptvarcone40, &b_lep_0_iso_ptvarcone40);
   fChain->SetBranchAddress("lep_0_iso_topoetcone20", &lep_0_iso_topoetcone20, &b_lep_0_iso_topoetcone20);
   fChain->SetBranchAddress("lep_0_iso_topoetcone30", &lep_0_iso_topoetcone30, &b_lep_0_iso_topoetcone30);
   fChain->SetBranchAddress("lep_0_iso_topoetcone40", &lep_0_iso_topoetcone40, &b_lep_0_iso_topoetcone40);
   fChain->SetBranchAddress("lep_0_m", &lep_0_m, &b_lep_0_m);
   fChain->SetBranchAddress("lep_0_matched", &lep_0_matched, &b_lep_0_matched);
   fChain->SetBranchAddress("lep_0_matched_et", &lep_0_matched_et, &b_lep_0_matched_et);
   fChain->SetBranchAddress("lep_0_matched_eta", &lep_0_matched_eta, &b_lep_0_matched_eta);
   fChain->SetBranchAddress("lep_0_matched_isHad", &lep_0_matched_isHad, &b_lep_0_matched_isHad);
   fChain->SetBranchAddress("lep_0_matched_m", &lep_0_matched_m, &b_lep_0_matched_m);
   fChain->SetBranchAddress("lep_0_matched_mother_pdgId", &lep_0_matched_mother_pdgId, &b_lep_0_matched_mother_pdgId);
   fChain->SetBranchAddress("lep_0_matched_mother_status", &lep_0_matched_mother_status, &b_lep_0_matched_mother_status);
   fChain->SetBranchAddress("lep_0_matched_origin", &lep_0_matched_origin, &b_lep_0_matched_origin);
   fChain->SetBranchAddress("lep_0_matched_pdgId", &lep_0_matched_pdgId, &b_lep_0_matched_pdgId);
   fChain->SetBranchAddress("lep_0_matched_phi", &lep_0_matched_phi, &b_lep_0_matched_phi);
   fChain->SetBranchAddress("lep_0_matched_pt", &lep_0_matched_pt, &b_lep_0_matched_pt);
   fChain->SetBranchAddress("lep_0_matched_q", &lep_0_matched_q, &b_lep_0_matched_q);
   fChain->SetBranchAddress("lep_0_matched_status", &lep_0_matched_status, &b_lep_0_matched_status);
   fChain->SetBranchAddress("lep_0_matched_type", &lep_0_matched_type, &b_lep_0_matched_type);
   fChain->SetBranchAddress("lep_0_muonAuthor", &lep_0_muonAuthor, &b_lep_0_muonAuthor);
   fChain->SetBranchAddress("lep_0_muonType", &lep_0_muonType, &b_lep_0_muonType);
   fChain->SetBranchAddress("lep_0_phi", &lep_0_phi, &b_lep_0_phi);
   fChain->SetBranchAddress("lep_0_pt", &lep_0_pt, &b_lep_0_pt);
   fChain->SetBranchAddress("lep_0_q", &lep_0_q, &b_lep_0_q);
   fChain->SetBranchAddress("lep_0_trk_d0", &lep_0_trk_d0, &b_lep_0_trk_d0);
   fChain->SetBranchAddress("lep_0_trk_d0_sig", &lep_0_trk_d0_sig, &b_lep_0_trk_d0_sig);
   fChain->SetBranchAddress("lep_0_trk_pt_error", &lep_0_trk_pt_error, &b_lep_0_trk_pt_error);
   fChain->SetBranchAddress("lep_0_trk_pvx_z0", &lep_0_trk_pvx_z0, &b_lep_0_trk_pvx_z0);
   fChain->SetBranchAddress("lep_0_trk_pvx_z0_sig", &lep_0_trk_pvx_z0_sig, &b_lep_0_trk_pvx_z0_sig);
   fChain->SetBranchAddress("lep_0_trk_pvx_z0_sintheta", &lep_0_trk_pvx_z0_sintheta, &b_lep_0_trk_pvx_z0_sintheta);
   fChain->SetBranchAddress("lep_0_trk_z0", &lep_0_trk_z0, &b_lep_0_trk_z0);
   fChain->SetBranchAddress("lep_0_trk_z0_sig", &lep_0_trk_z0_sig, &b_lep_0_trk_z0_sig);
   fChain->SetBranchAddress("lep_0_trk_z0_sintheta", &lep_0_trk_z0_sintheta, &b_lep_0_trk_z0_sintheta);
   fChain->SetBranchAddress("lep_1", &lep_1, &b_lep_1);
   fChain->SetBranchAddress("lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient", &lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient, &b_lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose", &lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose, &b_lep_1_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient", &lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient, &b_lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose", &lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose, &b_lep_1_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient", &lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient, &b_lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose", &lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose, &b_lep_1_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient", &lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient, &b_lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose", &lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose, &b_lep_1_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_1_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium", &lep_1_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium, &b_lep_1_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium", &lep_1_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium, &b_lep_1_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium", &lep_1_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium, &b_lep_1_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium", &lep_1_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium, &b_lep_1_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium", &lep_1_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium, &b_lep_1_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium", &lep_1_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium, &b_lep_1_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium", &lep_1_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium, &b_lep_1_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium", &lep_1_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium, &b_lep_1_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA", &lep_1_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA, &b_lep_1_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA", &lep_1_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA, &b_lep_1_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA", &lep_1_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA, &b_lep_1_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA", &lep_1_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA, &b_lep_1_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient", &lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient, &b_lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolGradient);
   fChain->SetBranchAddress("lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose", &lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose, &b_lep_1_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v11_isolLoose);
   fChain->SetBranchAddress("lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient", &lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient, &b_lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient);
   fChain->SetBranchAddress("lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose", &lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose, &b_lep_1_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose);
   fChain->SetBranchAddress("lep_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11", &lep_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11, &b_lep_1_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v11);
   fChain->SetBranchAddress("lep_1_NOMINAL_EleEffSF_offline_RecoTrk", &lep_1_NOMINAL_EleEffSF_offline_RecoTrk, &b_lep_1_NOMINAL_EleEffSF_offline_RecoTrk);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone", &lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone, &b_lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_NOMINAL_MuEffSF_HLT_mu26_imedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone", &lep_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone, &b_lep_1_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_IsoGradient", &lep_1_NOMINAL_MuEffSF_IsoGradient, &b_lep_1_NOMINAL_MuEffSF_IsoGradient);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_IsoLoose", &lep_1_NOMINAL_MuEffSF_IsoLoose, &b_lep_1_NOMINAL_MuEffSF_IsoLoose);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_Reco_QualMedium", &lep_1_NOMINAL_MuEffSF_Reco_QualMedium, &b_lep_1_NOMINAL_MuEffSF_Reco_QualMedium);
   fChain->SetBranchAddress("lep_1_NOMINAL_MuEffSF_TTVA", &lep_1_NOMINAL_MuEffSF_TTVA, &b_lep_1_NOMINAL_MuEffSF_TTVA);
   fChain->SetBranchAddress("lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient", &lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient, &b_lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolGradient);
   fChain->SetBranchAddress("lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose", &lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose, &b_lep_1_NOMINAL_efficiency_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v11_isolLoose);
   fChain->SetBranchAddress("lep_1_cluster_eta", &lep_1_cluster_eta, &b_lep_1_cluster_eta);
   fChain->SetBranchAddress("lep_1_cluster_eta_be2", &lep_1_cluster_eta_be2, &b_lep_1_cluster_eta_be2);
   fChain->SetBranchAddress("lep_1_et", &lep_1_et, &b_lep_1_et);
   fChain->SetBranchAddress("lep_1_eta", &lep_1_eta, &b_lep_1_eta);
   fChain->SetBranchAddress("lep_1_id_bad", &lep_1_id_bad, &b_lep_1_id_bad);
   fChain->SetBranchAddress("lep_1_id_loose", &lep_1_id_loose, &b_lep_1_id_loose);
   fChain->SetBranchAddress("lep_1_id_medium", &lep_1_id_medium, &b_lep_1_id_medium);
   fChain->SetBranchAddress("lep_1_id_tight", &lep_1_id_tight, &b_lep_1_id_tight);
   fChain->SetBranchAddress("lep_1_id_veryloose", &lep_1_id_veryloose, &b_lep_1_id_veryloose);
   fChain->SetBranchAddress("lep_1_iso_FixedCutHighPtCaloOnly", &lep_1_iso_FixedCutHighPtCaloOnly, &b_lep_1_iso_FixedCutHighPtCaloOnly);
   fChain->SetBranchAddress("lep_1_iso_FixedCutHighPtTrackOnly", &lep_1_iso_FixedCutHighPtTrackOnly, &b_lep_1_iso_FixedCutHighPtTrackOnly);
   fChain->SetBranchAddress("lep_1_iso_FixedCutLoose", &lep_1_iso_FixedCutLoose, &b_lep_1_iso_FixedCutLoose);
   fChain->SetBranchAddress("lep_1_iso_FixedCutTight", &lep_1_iso_FixedCutTight, &b_lep_1_iso_FixedCutTight);
   fChain->SetBranchAddress("lep_1_iso_FixedCutTightTrackOnly", &lep_1_iso_FixedCutTightTrackOnly, &b_lep_1_iso_FixedCutTightTrackOnly);
   fChain->SetBranchAddress("lep_1_iso_FixedCutTrackCone40", &lep_1_iso_FixedCutTrackCone40, &b_lep_1_iso_FixedCutTrackCone40);
   fChain->SetBranchAddress("lep_1_iso_Gradient", &lep_1_iso_Gradient, &b_lep_1_iso_Gradient);
   fChain->SetBranchAddress("lep_1_iso_GradientLoose", &lep_1_iso_GradientLoose, &b_lep_1_iso_GradientLoose);
   fChain->SetBranchAddress("lep_1_iso_Loose", &lep_1_iso_Loose, &b_lep_1_iso_Loose);
   fChain->SetBranchAddress("lep_1_iso_LooseTrackOnly", &lep_1_iso_LooseTrackOnly, &b_lep_1_iso_LooseTrackOnly);
   fChain->SetBranchAddress("lep_1_m", &lep_1_m, &b_lep_1_m);
   fChain->SetBranchAddress("lep_1_muonAuthor", &lep_1_muonAuthor, &b_lep_1_muonAuthor);
   fChain->SetBranchAddress("lep_1_muonType", &lep_1_muonType, &b_lep_1_muonType);
   fChain->SetBranchAddress("lep_1_phi", &lep_1_phi, &b_lep_1_phi);
   fChain->SetBranchAddress("lep_1_pt", &lep_1_pt, &b_lep_1_pt);
   fChain->SetBranchAddress("lep_1_q", &lep_1_q, &b_lep_1_q);
   fChain->SetBranchAddress("lep_1_trk_d0", &lep_1_trk_d0, &b_lep_1_trk_d0);
   fChain->SetBranchAddress("lep_1_trk_d0_sig", &lep_1_trk_d0_sig, &b_lep_1_trk_d0_sig);
   fChain->SetBranchAddress("lep_1_trk_pt_error", &lep_1_trk_pt_error, &b_lep_1_trk_pt_error);
   fChain->SetBranchAddress("lep_1_trk_pvx_z0", &lep_1_trk_pvx_z0, &b_lep_1_trk_pvx_z0);
   fChain->SetBranchAddress("lep_1_trk_pvx_z0_sig", &lep_1_trk_pvx_z0_sig, &b_lep_1_trk_pvx_z0_sig);
   fChain->SetBranchAddress("lep_1_trk_pvx_z0_sintheta", &lep_1_trk_pvx_z0_sintheta, &b_lep_1_trk_pvx_z0_sintheta);
   fChain->SetBranchAddress("lep_1_trk_z0", &lep_1_trk_z0, &b_lep_1_trk_z0);
   fChain->SetBranchAddress("lep_1_trk_z0_sig", &lep_1_trk_z0_sig, &b_lep_1_trk_z0_sig);
   fChain->SetBranchAddress("lep_1_trk_z0_sintheta", &lep_1_trk_z0_sintheta, &b_lep_1_trk_z0_sintheta);
   fChain->SetBranchAddress("lephad", &lephad, &b_lephad);
   fChain->SetBranchAddress("lephad_coll_approx", &lephad_coll_approx, &b_lephad_coll_approx);
   fChain->SetBranchAddress("lephad_coll_approx_m", &lephad_coll_approx_m, &b_lephad_coll_approx_m);
   fChain->SetBranchAddress("lephad_coll_approx_x0", &lephad_coll_approx_x0, &b_lephad_coll_approx_x0);
   fChain->SetBranchAddress("lephad_coll_approx_x1", &lephad_coll_approx_x1, &b_lephad_coll_approx_x1);
   fChain->SetBranchAddress("lephad_cosalpha", &lephad_cosalpha, &b_lephad_cosalpha);
   fChain->SetBranchAddress("lephad_deta", &lephad_deta, &b_lephad_deta);
   fChain->SetBranchAddress("lephad_dphi", &lephad_dphi, &b_lephad_dphi);
   fChain->SetBranchAddress("lephad_dpt", &lephad_dpt, &b_lephad_dpt);
   fChain->SetBranchAddress("lephad_dr", &lephad_dr, &b_lephad_dr);
   fChain->SetBranchAddress("lephad_eta", &lephad_eta, &b_lephad_eta);
   fChain->SetBranchAddress("lephad_met_bisect", &lephad_met_bisect, &b_lephad_met_bisect);
   fChain->SetBranchAddress("lephad_met_centrality", &lephad_met_centrality, &b_lephad_met_centrality);
   fChain->SetBranchAddress("lephad_met_lep0_cos_dphi", &lephad_met_lep0_cos_dphi, &b_lephad_met_lep0_cos_dphi);
   fChain->SetBranchAddress("lephad_met_lep1_cos_dphi", &lephad_met_lep1_cos_dphi, &b_lephad_met_lep1_cos_dphi);
   fChain->SetBranchAddress("lephad_met_min_dphi", &lephad_met_min_dphi, &b_lephad_met_min_dphi);
   fChain->SetBranchAddress("lephad_met_sum_cos_dphi", &lephad_met_sum_cos_dphi, &b_lephad_met_sum_cos_dphi);
   fChain->SetBranchAddress("lephad_mt_lep0_met", &lephad_mt_lep0_met, &b_lephad_mt_lep0_met);
   fChain->SetBranchAddress("lephad_mt_lep1_met", &lephad_mt_lep1_met, &b_lephad_mt_lep1_met);
   fChain->SetBranchAddress("lephad_phi", &lephad_phi, &b_lephad_phi);
   fChain->SetBranchAddress("lephad_ptx", &lephad_ptx, &b_lephad_ptx);
   fChain->SetBranchAddress("lephad_pty", &lephad_pty, &b_lephad_pty);
   fChain->SetBranchAddress("lephad_qxq", &lephad_qxq, &b_lephad_qxq);
   fChain->SetBranchAddress("lephad_scal_sum_pt", &lephad_scal_sum_pt, &b_lephad_scal_sum_pt);
   fChain->SetBranchAddress("lephad_tauRec_coll_approx", &lephad_tauRec_coll_approx, &b_lephad_tauRec_coll_approx);
   fChain->SetBranchAddress("lephad_tauRec_coll_approx_m", &lephad_tauRec_coll_approx_m, &b_lephad_tauRec_coll_approx_m);
   fChain->SetBranchAddress("lephad_tauRec_coll_approx_x0", &lephad_tauRec_coll_approx_x0, &b_lephad_tauRec_coll_approx_x0);
   fChain->SetBranchAddress("lephad_tauRec_coll_approx_x1", &lephad_tauRec_coll_approx_x1, &b_lephad_tauRec_coll_approx_x1);
   fChain->SetBranchAddress("lephad_tauRec_cosalpha", &lephad_tauRec_cosalpha, &b_lephad_tauRec_cosalpha);
   fChain->SetBranchAddress("lephad_tauRec_deta", &lephad_tauRec_deta, &b_lephad_tauRec_deta);
   fChain->SetBranchAddress("lephad_tauRec_dphi", &lephad_tauRec_dphi, &b_lephad_tauRec_dphi);
   fChain->SetBranchAddress("lephad_tauRec_dpt", &lephad_tauRec_dpt, &b_lephad_tauRec_dpt);
   fChain->SetBranchAddress("lephad_tauRec_dr", &lephad_tauRec_dr, &b_lephad_tauRec_dr);
   fChain->SetBranchAddress("lephad_tauRec_met_bisect", &lephad_tauRec_met_bisect, &b_lephad_tauRec_met_bisect);
   fChain->SetBranchAddress("lephad_tauRec_met_centrality", &lephad_tauRec_met_centrality, &b_lephad_tauRec_met_centrality);
   fChain->SetBranchAddress("lephad_tauRec_met_lep0_cos_dphi", &lephad_tauRec_met_lep0_cos_dphi, &b_lephad_tauRec_met_lep0_cos_dphi);
   fChain->SetBranchAddress("lephad_tauRec_met_lep1_cos_dphi", &lephad_tauRec_met_lep1_cos_dphi, &b_lephad_tauRec_met_lep1_cos_dphi);
   fChain->SetBranchAddress("lephad_tauRec_met_min_dphi", &lephad_tauRec_met_min_dphi, &b_lephad_tauRec_met_min_dphi);
   fChain->SetBranchAddress("lephad_tauRec_met_sum_cos_phi", &lephad_tauRec_met_sum_cos_phi, &b_lephad_tauRec_met_sum_cos_phi);
   fChain->SetBranchAddress("lephad_tauRec_mt_lep0_met", &lephad_tauRec_mt_lep0_met, &b_lephad_tauRec_mt_lep0_met);
   fChain->SetBranchAddress("lephad_tauRec_mt_lep1_met", &lephad_tauRec_mt_lep1_met, &b_lephad_tauRec_mt_lep1_met);
   fChain->SetBranchAddress("lephad_tauRec_scal_sum_pt", &lephad_tauRec_scal_sum_pt, &b_lephad_tauRec_scal_sum_pt);
   fChain->SetBranchAddress("lephad_tauRec_vect_sum_pt", &lephad_tauRec_vect_sum_pt, &b_lephad_tauRec_vect_sum_pt);
   fChain->SetBranchAddress("lephad_tauRec_vis_mass", &lephad_tauRec_vis_mass, &b_lephad_tauRec_vis_mass);
   fChain->SetBranchAddress("lephad_vect_sum_pt", &lephad_vect_sum_pt, &b_lephad_vect_sum_pt);
   fChain->SetBranchAddress("lephad_vis_mass", &lephad_vis_mass, &b_lephad_vis_mass);
   fChain->SetBranchAddress("matched_jet_0_eta", &matched_jet_0_eta, &b_matched_jet_0_eta);
   fChain->SetBranchAddress("matched_jet_0_m", &matched_jet_0_m, &b_matched_jet_0_m);
   fChain->SetBranchAddress("matched_jet_0_pdgid", &matched_jet_0_pdgid, &b_matched_jet_0_pdgid);
   fChain->SetBranchAddress("matched_jet_0_phi", &matched_jet_0_phi, &b_matched_jet_0_phi);
   fChain->SetBranchAddress("matched_jet_0_pt", &matched_jet_0_pt, &b_matched_jet_0_pt);
   fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
   fChain->SetBranchAddress("met_hpto_et", &met_hpto_et, &b_met_hpto_et);
   fChain->SetBranchAddress("met_hpto_etx", &met_hpto_etx, &b_met_hpto_etx);
   fChain->SetBranchAddress("met_hpto_ety", &met_hpto_ety, &b_met_hpto_ety);
   fChain->SetBranchAddress("met_hpto_phi", &met_hpto_phi, &b_met_hpto_phi);
   fChain->SetBranchAddress("met_more_met_et_ele", &met_more_met_et_ele, &b_met_more_met_et_ele);
   fChain->SetBranchAddress("met_more_met_et_jet", &met_more_met_et_jet, &b_met_more_met_et_jet);
   fChain->SetBranchAddress("met_more_met_et_muon", &met_more_met_et_muon, &b_met_more_met_et_muon);
   fChain->SetBranchAddress("met_more_met_et_pho", &met_more_met_et_pho, &b_met_more_met_et_pho);
   fChain->SetBranchAddress("met_more_met_et_soft", &met_more_met_et_soft, &b_met_more_met_et_soft);
   fChain->SetBranchAddress("met_more_met_et_tau", &met_more_met_et_tau, &b_met_more_met_et_tau);
   fChain->SetBranchAddress("met_more_met_phi_ele", &met_more_met_phi_ele, &b_met_more_met_phi_ele);
   fChain->SetBranchAddress("met_more_met_phi_jet", &met_more_met_phi_jet, &b_met_more_met_phi_jet);
   fChain->SetBranchAddress("met_more_met_phi_muon", &met_more_met_phi_muon, &b_met_more_met_phi_muon);
   fChain->SetBranchAddress("met_more_met_phi_pho", &met_more_met_phi_pho, &b_met_more_met_phi_pho);
   fChain->SetBranchAddress("met_more_met_phi_soft", &met_more_met_phi_soft, &b_met_more_met_phi_soft);
   fChain->SetBranchAddress("met_more_met_phi_tau", &met_more_met_phi_tau, &b_met_more_met_phi_tau);
   fChain->SetBranchAddress("met_more_met_sumet_ele", &met_more_met_sumet_ele, &b_met_more_met_sumet_ele);
   fChain->SetBranchAddress("met_more_met_sumet_jet", &met_more_met_sumet_jet, &b_met_more_met_sumet_jet);
   fChain->SetBranchAddress("met_more_met_sumet_muon", &met_more_met_sumet_muon, &b_met_more_met_sumet_muon);
   fChain->SetBranchAddress("met_more_met_sumet_pho", &met_more_met_sumet_pho, &b_met_more_met_sumet_pho);
   fChain->SetBranchAddress("met_more_met_sumet_soft", &met_more_met_sumet_soft, &b_met_more_met_sumet_soft);
   fChain->SetBranchAddress("met_more_met_sumet_tau", &met_more_met_sumet_tau, &b_met_more_met_sumet_tau);
   fChain->SetBranchAddress("met_reco_et", &met_reco_et, &b_met_reco_et);
   fChain->SetBranchAddress("met_reco_etx", &met_reco_etx, &b_met_reco_etx);
   fChain->SetBranchAddress("met_reco_ety", &met_reco_ety, &b_met_reco_ety);
   fChain->SetBranchAddress("met_reco_phi", &met_reco_phi, &b_met_reco_phi);
   fChain->SetBranchAddress("met_reco_sig", &met_reco_sig, &b_met_reco_sig);
   fChain->SetBranchAddress("met_reco_sig_tracks", &met_reco_sig_tracks, &b_met_reco_sig_tracks);
   fChain->SetBranchAddress("met_reco_sumet", &met_reco_sumet, &b_met_reco_sumet);
   fChain->SetBranchAddress("met_truth_et", &met_truth_et, &b_met_truth_et);
   fChain->SetBranchAddress("met_truth_etx", &met_truth_etx, &b_met_truth_etx);
   fChain->SetBranchAddress("met_truth_ety", &met_truth_ety, &b_met_truth_ety);
   fChain->SetBranchAddress("met_truth_phi", &met_truth_phi, &b_met_truth_phi);
   fChain->SetBranchAddress("met_truth_sig", &met_truth_sig, &b_met_truth_sig);
   fChain->SetBranchAddress("met_truth_sig_tracks", &met_truth_sig_tracks, &b_met_truth_sig_tracks);
   fChain->SetBranchAddress("met_truth_sumet", &met_truth_sumet, &b_met_truth_sumet);
   fChain->SetBranchAddress("muTrigMatch_0_HLT_mu20_iloose_L1MU15", &muTrigMatch_0_HLT_mu20_iloose_L1MU15, &b_muTrigMatch_0_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("muTrigMatch_0_HLT_mu24_imedium", &muTrigMatch_0_HLT_mu24_imedium, &b_muTrigMatch_0_HLT_mu24_imedium);
   fChain->SetBranchAddress("muTrigMatch_0_HLT_mu24_ivarmedium", &muTrigMatch_0_HLT_mu24_ivarmedium, &b_muTrigMatch_0_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("muTrigMatch_0_HLT_mu26_imedium", &muTrigMatch_0_HLT_mu26_imedium, &b_muTrigMatch_0_HLT_mu26_imedium);
   fChain->SetBranchAddress("muTrigMatch_0_HLT_mu26_ivarmedium", &muTrigMatch_0_HLT_mu26_ivarmedium, &b_muTrigMatch_0_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("muTrigMatch_0_HLT_mu40", &muTrigMatch_0_HLT_mu40, &b_muTrigMatch_0_HLT_mu40);
   fChain->SetBranchAddress("muTrigMatch_0_HLT_mu50", &muTrigMatch_0_HLT_mu50, &b_muTrigMatch_0_HLT_mu50);
   fChain->SetBranchAddress("muTrigMatch_0_trigger_matched", &muTrigMatch_0_trigger_matched, &b_muTrigMatch_0_trigger_matched);
   fChain->SetBranchAddress("n_actual_int", &n_actual_int, &b_n_actual_int);
   fChain->SetBranchAddress("n_avg_int", &n_avg_int, &b_n_avg_int);
   fChain->SetBranchAddress("n_avg_int_cor", &n_avg_int_cor, &b_n_avg_int_cor);
   fChain->SetBranchAddress("n_bjets", &n_bjets, &b_n_bjets);
   fChain->SetBranchAddress("n_electrons", &n_electrons, &b_n_electrons);
   fChain->SetBranchAddress("n_electrons_met", &n_electrons_met, &b_n_electrons_met);
   fChain->SetBranchAddress("n_electrons_olr", &n_electrons_olr, &b_n_electrons_olr);
   fChain->SetBranchAddress("n_jets", &n_jets, &b_n_jets);
   fChain->SetBranchAddress("n_jets_30", &n_jets_30, &b_n_jets_30);
   fChain->SetBranchAddress("n_jets_40", &n_jets_40, &b_n_jets_40);
   fChain->SetBranchAddress("n_jets_bad", &n_jets_bad, &b_n_jets_bad);
   fChain->SetBranchAddress("n_jets_mc_hs", &n_jets_mc_hs, &b_n_jets_mc_hs);
   fChain->SetBranchAddress("n_jets_met", &n_jets_met, &b_n_jets_met);
   fChain->SetBranchAddress("n_jets_olr", &n_jets_olr, &b_n_jets_olr);
   fChain->SetBranchAddress("n_muons", &n_muons, &b_n_muons);
   fChain->SetBranchAddress("n_muons_met", &n_muons_met, &b_n_muons_met);
   fChain->SetBranchAddress("n_muons_olr", &n_muons_olr, &b_n_muons_olr);
   fChain->SetBranchAddress("n_photons", &n_photons, &b_n_photons);
   fChain->SetBranchAddress("n_photons_met", &n_photons_met, &b_n_photons_met);
   fChain->SetBranchAddress("n_photons_olr", &n_photons_olr, &b_n_photons_olr);
   fChain->SetBranchAddress("n_pvx", &n_pvx, &b_n_pvx);
   fChain->SetBranchAddress("n_taus", &n_taus, &b_n_taus);
   fChain->SetBranchAddress("n_taus_loose", &n_taus_loose, &b_n_taus_loose);
   fChain->SetBranchAddress("n_taus_medium", &n_taus_medium, &b_n_taus_medium);
   fChain->SetBranchAddress("n_taus_met", &n_taus_met, &b_n_taus_met);
   fChain->SetBranchAddress("n_taus_olr", &n_taus_olr, &b_n_taus_olr);
   fChain->SetBranchAddress("n_taus_tight", &n_taus_tight, &b_n_taus_tight);
   fChain->SetBranchAddress("n_truth_gluon_jets", &n_truth_gluon_jets, &b_n_truth_gluon_jets);
   fChain->SetBranchAddress("n_truth_jets", &n_truth_jets, &b_n_truth_jets);
   fChain->SetBranchAddress("n_truth_jets_pt20_eta45", &n_truth_jets_pt20_eta45, &b_n_truth_jets_pt20_eta45);
   fChain->SetBranchAddress("n_truth_quark_jets", &n_truth_quark_jets, &b_n_truth_quark_jets);
   fChain->SetBranchAddress("n_vx", &n_vx, &b_n_vx);
   fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
   fChain->SetBranchAddress("tau_0", &tau_0, &b_tau_0);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad", &tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad, &b_tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetBDTloose", &tau_0_NOMINAL_TauEffSF_JetBDTloose, &b_tau_0_NOMINAL_TauEffSF_JetBDTloose);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetBDTmedium", &tau_0_NOMINAL_TauEffSF_JetBDTmedium, &b_tau_0_NOMINAL_TauEffSF_JetBDTmedium);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetBDTtight", &tau_0_NOMINAL_TauEffSF_JetBDTtight, &b_tau_0_NOMINAL_TauEffSF_JetBDTtight);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron", &tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron, &b_tau_0_NOMINAL_TauEffSF_LooseEleBDTPlusVeto_electron);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron", &tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron, &b_tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron", &tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron, &b_tau_0_NOMINAL_TauEffSF_MediumEleBDTPlusVeto_electron);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron", &tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron, &b_tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron", &tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron, &b_tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_reco", &tau_0_NOMINAL_TauEffSF_reco, &b_tau_0_NOMINAL_TauEffSF_reco);
   fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_selection", &tau_0_NOMINAL_TauEffSF_selection, &b_tau_0_NOMINAL_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDTPlusVeto_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDTPlusVeto_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDTPlusVeto_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDT_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDT_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_LooseEleBDT_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDTPlusVeto_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDTPlusVeto_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDTPlusVeto_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDT_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDT_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_MediumEleBDT_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_VeryLooseLlhEleOLR_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_VeryLooseLlhEleOLR_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_VeryLooseLlhEleOLR_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDTPlusVeto_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDTPlusVeto_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDTPlusVeto_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDT_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDT_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_LooseEleBDT_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDTPlusVeto_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDTPlusVeto_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDTPlusVeto_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDT_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDT_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_MediumEleBDT_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_VeryLooseLlhEleOLR_electron", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_VeryLooseLlhEleOLR_electron, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_VeryLooseLlhEleOLR_electron);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection", &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection, &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad", &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad, &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection", &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection, &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad", &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad, &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection", &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection, &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTloose", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTloose, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTloose);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTmedium", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTmedium, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTmedium);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTtight", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTtight, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1down_TauEffSF_JetBDTtight);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTloose", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTloose, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTloose);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTmedium", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTmedium, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTmedium);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTtight", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTtight, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT_1up_TauEffSF_JetBDTtight);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTloose", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTloose, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTloose);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTmedium", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTmedium, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTmedium);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTtight", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTtight, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1down_TauEffSF_JetBDTtight);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTloose", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTloose, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTloose);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTmedium", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTmedium, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTmedium);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTtight", &tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTtight, &b_tau_0_TAUS_TRUEHADTAU_EFF_JETID_TOTAL_1up_TauEffSF_JetBDTtight);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco, &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_reco);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection, &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1down_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco, &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_reco);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection, &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT_1up_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco, &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection, &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco, &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco);
   fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection, &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection);
   fChain->SetBranchAddress("tau_0_allTrk_eta", &tau_0_allTrk_eta, &b_tau_0_allTrk_eta);
   fChain->SetBranchAddress("tau_0_allTrk_n", &tau_0_allTrk_n, &b_tau_0_allTrk_n);
   fChain->SetBranchAddress("tau_0_allTrk_phi", &tau_0_allTrk_phi, &b_tau_0_allTrk_phi);
   fChain->SetBranchAddress("tau_0_allTrk_pt", &tau_0_allTrk_pt, &b_tau_0_allTrk_pt);
   fChain->SetBranchAddress("tau_0_decay_mode", &tau_0_decay_mode, &b_tau_0_decay_mode);
   fChain->SetBranchAddress("tau_0_ele_BDTEleScoreTrans_run2", &tau_0_ele_BDTEleScoreTrans_run2, &b_tau_0_ele_BDTEleScoreTrans_run2);
   fChain->SetBranchAddress("tau_0_ele_bdt_loose", &tau_0_ele_bdt_loose, &b_tau_0_ele_bdt_loose);
   fChain->SetBranchAddress("tau_0_ele_bdt_medium", &tau_0_ele_bdt_medium, &b_tau_0_ele_bdt_medium);
   fChain->SetBranchAddress("tau_0_ele_bdt_score", &tau_0_ele_bdt_score, &b_tau_0_ele_bdt_score);
   fChain->SetBranchAddress("tau_0_ele_bdt_tight", &tau_0_ele_bdt_tight, &b_tau_0_ele_bdt_tight);
   fChain->SetBranchAddress("tau_0_ele_match_lhscore", &tau_0_ele_match_lhscore, &b_tau_0_ele_match_lhscore);
   fChain->SetBranchAddress("tau_0_ele_olr_pass", &tau_0_ele_olr_pass, &b_tau_0_ele_olr_pass);
   fChain->SetBranchAddress("tau_0_eleid_AbsDEtaLeadTrk", &tau_0_eleid_AbsDEtaLeadTrk, &b_tau_0_eleid_AbsDEtaLeadTrk);
   fChain->SetBranchAddress("tau_0_eleid_AbsDPhiLeadTrk", &tau_0_eleid_AbsDPhiLeadTrk, &b_tau_0_eleid_AbsDPhiLeadTrk);
   fChain->SetBranchAddress("tau_0_eleid_AbsEtaLeadTrk", &tau_0_eleid_AbsEtaLeadTrk, &b_tau_0_eleid_AbsEtaLeadTrk);
   fChain->SetBranchAddress("tau_0_eleid_EMFracAtEMScale", &tau_0_eleid_EMFracAtEMScale, &b_tau_0_eleid_EMFracAtEMScale);
   fChain->SetBranchAddress("tau_0_eleid_EMFracFixed", &tau_0_eleid_EMFracFixed, &b_tau_0_eleid_EMFracFixed);
   fChain->SetBranchAddress("tau_0_eleid_PSSFraction", &tau_0_eleid_PSSFraction, &b_tau_0_eleid_PSSFraction);
   fChain->SetBranchAddress("tau_0_eleid_centFrac", &tau_0_eleid_centFrac, &b_tau_0_eleid_centFrac);
   fChain->SetBranchAddress("tau_0_eleid_etHotShotWinOverPtLeadTrk", &tau_0_eleid_etHotShotWinOverPtLeadTrk, &b_tau_0_eleid_etHotShotWinOverPtLeadTrk);
   fChain->SetBranchAddress("tau_0_eleid_etOverPtLeadTrk", &tau_0_eleid_etOverPtLeadTrk, &b_tau_0_eleid_etOverPtLeadTrk);
   fChain->SetBranchAddress("tau_0_eleid_hadLeakEt", &tau_0_eleid_hadLeakEt, &b_tau_0_eleid_hadLeakEt);
   fChain->SetBranchAddress("tau_0_eleid_hadLeakFracFixed", &tau_0_eleid_hadLeakFracFixed, &b_tau_0_eleid_hadLeakFracFixed);
   fChain->SetBranchAddress("tau_0_eleid_isolFrac", &tau_0_eleid_isolFrac, &b_tau_0_eleid_isolFrac);
   fChain->SetBranchAddress("tau_0_eleid_leadTrackProbHT", &tau_0_eleid_leadTrackProbHT, &b_tau_0_eleid_leadTrackProbHT);
   fChain->SetBranchAddress("tau_0_eleid_secMaxStripEt", &tau_0_eleid_secMaxStripEt, &b_tau_0_eleid_secMaxStripEt);
   fChain->SetBranchAddress("tau_0_eleid_secMaxStripEtOverPtLeadTrk", &tau_0_eleid_secMaxStripEtOverPtLeadTrk, &b_tau_0_eleid_secMaxStripEtOverPtLeadTrk);
   fChain->SetBranchAddress("tau_0_et", &tau_0_et, &b_tau_0_et);
   fChain->SetBranchAddress("tau_0_eta", &tau_0_eta, &b_tau_0_eta);
   fChain->SetBranchAddress("tau_0_id_ChPiEMEOverCaloEME", &tau_0_id_ChPiEMEOverCaloEME, &b_tau_0_id_ChPiEMEOverCaloEME);
   fChain->SetBranchAddress("tau_0_id_EMPOverTrkSysP", &tau_0_id_EMPOverTrkSysP, &b_tau_0_id_EMPOverTrkSysP);
   fChain->SetBranchAddress("tau_0_id_SumPtTrkFrac", &tau_0_id_SumPtTrkFrac, &b_tau_0_id_SumPtTrkFrac);
   fChain->SetBranchAddress("tau_0_id_centFrac", &tau_0_id_centFrac, &b_tau_0_id_centFrac);
   fChain->SetBranchAddress("tau_0_id_dRmax", &tau_0_id_dRmax, &b_tau_0_id_dRmax);
   fChain->SetBranchAddress("tau_0_id_etOverPtLeadTrk", &tau_0_id_etOverPtLeadTrk, &b_tau_0_id_etOverPtLeadTrk);
   fChain->SetBranchAddress("tau_0_id_innerTrkAvgDist", &tau_0_id_innerTrkAvgDist, &b_tau_0_id_innerTrkAvgDist);
   fChain->SetBranchAddress("tau_0_id_ipSigLeadTrk", &tau_0_id_ipSigLeadTrk, &b_tau_0_id_ipSigLeadTrk);
   fChain->SetBranchAddress("tau_0_id_mEflowApprox", &tau_0_id_mEflowApprox, &b_tau_0_id_mEflowApprox);
   fChain->SetBranchAddress("tau_0_id_massTrkSys", &tau_0_id_massTrkSys, &b_tau_0_id_massTrkSys);
   fChain->SetBranchAddress("tau_0_id_ptIntermediateAxis", &tau_0_id_ptIntermediateAxis, &b_tau_0_id_ptIntermediateAxis);
   fChain->SetBranchAddress("tau_0_id_ptRatioEflowApprox", &tau_0_id_ptRatioEflowApprox, &b_tau_0_id_ptRatioEflowApprox);
   fChain->SetBranchAddress("tau_0_id_trFlightPathSig", &tau_0_id_trFlightPathSig, &b_tau_0_id_trFlightPathSig);
   fChain->SetBranchAddress("tau_0_jet_bdt_loose", &tau_0_jet_bdt_loose, &b_tau_0_jet_bdt_loose);
   fChain->SetBranchAddress("tau_0_jet_bdt_medium", &tau_0_jet_bdt_medium, &b_tau_0_jet_bdt_medium);
   fChain->SetBranchAddress("tau_0_jet_bdt_score", &tau_0_jet_bdt_score, &b_tau_0_jet_bdt_score);
   fChain->SetBranchAddress("tau_0_jet_bdt_score_trans", &tau_0_jet_bdt_score_trans, &b_tau_0_jet_bdt_score_trans);
   fChain->SetBranchAddress("tau_0_jet_bdt_tight", &tau_0_jet_bdt_tight, &b_tau_0_jet_bdt_tight);
   fChain->SetBranchAddress("tau_0_jet_width", &tau_0_jet_width, &b_tau_0_jet_width);
   fChain->SetBranchAddress("tau_0_leadTrk_d0", &tau_0_leadTrk_d0, &b_tau_0_leadTrk_d0);
   fChain->SetBranchAddress("tau_0_leadTrk_d0_sig", &tau_0_leadTrk_d0_sig, &b_tau_0_leadTrk_d0_sig);
   fChain->SetBranchAddress("tau_0_leadTrk_eta", &tau_0_leadTrk_eta, &b_tau_0_leadTrk_eta);
   fChain->SetBranchAddress("tau_0_leadTrk_phi", &tau_0_leadTrk_phi, &b_tau_0_leadTrk_phi);
   fChain->SetBranchAddress("tau_0_leadTrk_pt", &tau_0_leadTrk_pt, &b_tau_0_leadTrk_pt);
   fChain->SetBranchAddress("tau_0_leadTrk_pvx_z0", &tau_0_leadTrk_pvx_z0, &b_tau_0_leadTrk_pvx_z0);
   fChain->SetBranchAddress("tau_0_leadTrk_pvx_z0_sig", &tau_0_leadTrk_pvx_z0_sig, &b_tau_0_leadTrk_pvx_z0_sig);
   fChain->SetBranchAddress("tau_0_leadTrk_pvx_z0_sintheta", &tau_0_leadTrk_pvx_z0_sintheta, &b_tau_0_leadTrk_pvx_z0_sintheta);
   fChain->SetBranchAddress("tau_0_leadTrk_z0", &tau_0_leadTrk_z0, &b_tau_0_leadTrk_z0);
   fChain->SetBranchAddress("tau_0_leadTrk_z0_sig", &tau_0_leadTrk_z0_sig, &b_tau_0_leadTrk_z0_sig);
   fChain->SetBranchAddress("tau_0_leadTrk_z0_sintheta", &tau_0_leadTrk_z0_sintheta, &b_tau_0_leadTrk_z0_sintheta);
   fChain->SetBranchAddress("tau_0_m", &tau_0_m, &b_tau_0_m);
   fChain->SetBranchAddress("tau_0_mvx", &tau_0_mvx, &b_tau_0_mvx);
   fChain->SetBranchAddress("tau_0_mvx_tagged", &tau_0_mvx_tagged, &b_tau_0_mvx_tagged);
   fChain->SetBranchAddress("tau_0_n_all_tracks", &tau_0_n_all_tracks, &b_tau_0_n_all_tracks);
   fChain->SetBranchAddress("tau_0_n_charged_tracks", &tau_0_n_charged_tracks, &b_tau_0_n_charged_tracks);
   fChain->SetBranchAddress("tau_0_n_conversion_tracks", &tau_0_n_conversion_tracks, &b_tau_0_n_conversion_tracks);
   fChain->SetBranchAddress("tau_0_n_core_tracks", &tau_0_n_core_tracks, &b_tau_0_n_core_tracks);
   fChain->SetBranchAddress("tau_0_n_failTrackFilter_tracks", &tau_0_n_failTrackFilter_tracks, &b_tau_0_n_failTrackFilter_tracks);
   fChain->SetBranchAddress("tau_0_n_fake_tracks", &tau_0_n_fake_tracks, &b_tau_0_n_fake_tracks);
   fChain->SetBranchAddress("tau_0_n_isolation_tracks", &tau_0_n_isolation_tracks, &b_tau_0_n_isolation_tracks);
   fChain->SetBranchAddress("tau_0_n_modified_isolation_tracks", &tau_0_n_modified_isolation_tracks, &b_tau_0_n_modified_isolation_tracks);
   fChain->SetBranchAddress("tau_0_n_old_tracks", &tau_0_n_old_tracks, &b_tau_0_n_old_tracks);
   fChain->SetBranchAddress("tau_0_n_passTrkSelectionTight_tracks", &tau_0_n_passTrkSelectionTight_tracks, &b_tau_0_n_passTrkSelectionTight_tracks);
   fChain->SetBranchAddress("tau_0_n_passTrkSelector_tracks", &tau_0_n_passTrkSelector_tracks, &b_tau_0_n_passTrkSelector_tracks);
   fChain->SetBranchAddress("tau_0_n_unclassified_tracks", &tau_0_n_unclassified_tracks, &b_tau_0_n_unclassified_tracks);
   fChain->SetBranchAddress("tau_0_n_wide_tracks", &tau_0_n_wide_tracks, &b_tau_0_n_wide_tracks);
   fChain->SetBranchAddress("tau_0_phi", &tau_0_phi, &b_tau_0_phi);
   fChain->SetBranchAddress("tau_0_pt", &tau_0_pt, &b_tau_0_pt);
   fChain->SetBranchAddress("tau_0_q", &tau_0_q, &b_tau_0_q);
   fChain->SetBranchAddress("tau_0_sub_ConstituentBased_eta", &tau_0_sub_ConstituentBased_eta, &b_tau_0_sub_ConstituentBased_eta);
   fChain->SetBranchAddress("tau_0_sub_ConstituentBased_m", &tau_0_sub_ConstituentBased_m, &b_tau_0_sub_ConstituentBased_m);
   fChain->SetBranchAddress("tau_0_sub_ConstituentBased_phi", &tau_0_sub_ConstituentBased_phi, &b_tau_0_sub_ConstituentBased_phi);
   fChain->SetBranchAddress("tau_0_sub_ConstituentBased_pt", &tau_0_sub_ConstituentBased_pt, &b_tau_0_sub_ConstituentBased_pt);
   fChain->SetBranchAddress("tau_0_sub_PanTauCellBased_eta", &tau_0_sub_PanTauCellBased_eta, &b_tau_0_sub_PanTauCellBased_eta);
   fChain->SetBranchAddress("tau_0_sub_PanTauCellBased_m", &tau_0_sub_PanTauCellBased_m, &b_tau_0_sub_PanTauCellBased_m);
   fChain->SetBranchAddress("tau_0_sub_PanTauCellBased_phi", &tau_0_sub_PanTauCellBased_phi, &b_tau_0_sub_PanTauCellBased_phi);
   fChain->SetBranchAddress("tau_0_sub_PanTauCellBased_pt", &tau_0_sub_PanTauCellBased_pt, &b_tau_0_sub_PanTauCellBased_pt);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTValue_1p0n_vs_1p1n", &tau_0_sub_PanTau_BDTValue_1p0n_vs_1p1n, &b_tau_0_sub_PanTau_BDTValue_1p0n_vs_1p1n);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTValue_1p1n_vs_1pXn", &tau_0_sub_PanTau_BDTValue_1p1n_vs_1pXn, &b_tau_0_sub_PanTau_BDTValue_1p1n_vs_1pXn);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTValue_3p0n_vs_3pXn", &tau_0_sub_PanTau_BDTValue_3p0n_vs_3pXn, &b_tau_0_sub_PanTau_BDTValue_3p0n_vs_3pXn);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Basic_NNeutralConsts", &tau_0_sub_PanTau_BDTVar_Basic_NNeutralConsts, &b_tau_0_sub_PanTau_BDTVar_Basic_NNeutralConsts);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Charged_HLV_SumM", &tau_0_sub_PanTau_BDTVar_Charged_HLV_SumM, &b_tau_0_sub_PanTau_BDTVar_Charged_HLV_SumM);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt", &tau_0_sub_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt, &b_tau_0_sub_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts", &tau_0_sub_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts, &b_tau_0_sub_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged", &tau_0_sub_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged, &b_tau_0_sub_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Neutral_HLV_SumM", &tau_0_sub_PanTau_BDTVar_Neutral_HLV_SumM, &b_tau_0_sub_PanTau_BDTVar_Neutral_HLV_SumM);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1", &tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1, &b_tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2", &tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2, &b_tau_0_sub_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts", &tau_0_sub_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts, &b_tau_0_sub_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts", &tau_0_sub_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts, &b_tau_0_sub_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts);
   fChain->SetBranchAddress("tau_0_sub_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed", &tau_0_sub_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed, &b_tau_0_sub_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed);
   fChain->SetBranchAddress("tau_0_sub_PanTau_DecayMode", &tau_0_sub_PanTau_DecayMode, &b_tau_0_sub_PanTau_DecayMode);
   fChain->SetBranchAddress("tau_0_sub_PanTau_DecayModeProto", &tau_0_sub_PanTau_DecayModeProto, &b_tau_0_sub_PanTau_DecayModeProto);
   fChain->SetBranchAddress("tau_0_sub_PanTau_isPanTauCandidate", &tau_0_sub_PanTau_isPanTauCandidate, &b_tau_0_sub_PanTau_isPanTauCandidate);
   fChain->SetBranchAddress("tau_0_sub_ParticleFlowCombined_eta", &tau_0_sub_ParticleFlowCombined_eta, &b_tau_0_sub_ParticleFlowCombined_eta);
   fChain->SetBranchAddress("tau_0_sub_ParticleFlowCombined_m", &tau_0_sub_ParticleFlowCombined_m, &b_tau_0_sub_ParticleFlowCombined_m);
   fChain->SetBranchAddress("tau_0_sub_ParticleFlowCombined_phi", &tau_0_sub_ParticleFlowCombined_phi, &b_tau_0_sub_ParticleFlowCombined_phi);
   fChain->SetBranchAddress("tau_0_sub_ParticleFlowCombined_pt", &tau_0_sub_ParticleFlowCombined_pt, &b_tau_0_sub_ParticleFlowCombined_pt);
   fChain->SetBranchAddress("tau_0_sub_ctrk0_cellBased_eta", &tau_0_sub_ctrk0_cellBased_eta, &b_tau_0_sub_ctrk0_cellBased_eta);
   fChain->SetBranchAddress("tau_0_sub_ctrk0_cellBased_phi", &tau_0_sub_ctrk0_cellBased_phi, &b_tau_0_sub_ctrk0_cellBased_phi);
   fChain->SetBranchAddress("tau_0_sub_ctrk0_cellBased_pt", &tau_0_sub_ctrk0_cellBased_pt, &b_tau_0_sub_ctrk0_cellBased_pt);
   fChain->SetBranchAddress("tau_0_sub_ctrk1_cellBased_eta", &tau_0_sub_ctrk1_cellBased_eta, &b_tau_0_sub_ctrk1_cellBased_eta);
   fChain->SetBranchAddress("tau_0_sub_ctrk1_cellBased_phi", &tau_0_sub_ctrk1_cellBased_phi, &b_tau_0_sub_ctrk1_cellBased_phi);
   fChain->SetBranchAddress("tau_0_sub_ctrk1_cellBased_pt", &tau_0_sub_ctrk1_cellBased_pt, &b_tau_0_sub_ctrk1_cellBased_pt);
   fChain->SetBranchAddress("tau_0_sub_ctrk2_cellBased_eta", &tau_0_sub_ctrk2_cellBased_eta, &b_tau_0_sub_ctrk2_cellBased_eta);
   fChain->SetBranchAddress("tau_0_sub_ctrk2_cellBased_phi", &tau_0_sub_ctrk2_cellBased_phi, &b_tau_0_sub_ctrk2_cellBased_phi);
   fChain->SetBranchAddress("tau_0_sub_ctrk2_cellBased_pt", &tau_0_sub_ctrk2_cellBased_pt, &b_tau_0_sub_ctrk2_cellBased_pt);
   fChain->SetBranchAddress("tau_0_sub_n_charged", &tau_0_sub_n_charged, &b_tau_0_sub_n_charged);
   fChain->SetBranchAddress("tau_0_sub_n_neutral", &tau_0_sub_n_neutral, &b_tau_0_sub_n_neutral);
   fChain->SetBranchAddress("tau_0_sub_neu0_bdtPi0Score", &tau_0_sub_neu0_bdtPi0Score, &b_tau_0_sub_neu0_bdtPi0Score);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_CENTER_LAMBDA", &tau_0_sub_neu0_cellBased_CENTER_LAMBDA, &b_tau_0_sub_neu0_cellBased_CENTER_LAMBDA);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_DELTA_PHI", &tau_0_sub_neu0_cellBased_DELTA_PHI, &b_tau_0_sub_neu0_cellBased_DELTA_PHI);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_DELTA_THETA", &tau_0_sub_neu0_cellBased_DELTA_THETA, &b_tau_0_sub_neu0_cellBased_DELTA_THETA);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_EM1CoreFrac", &tau_0_sub_neu0_cellBased_EM1CoreFrac, &b_tau_0_sub_neu0_cellBased_EM1CoreFrac);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_ENG_FRAC_CORE", &tau_0_sub_neu0_cellBased_ENG_FRAC_CORE, &b_tau_0_sub_neu0_cellBased_ENG_FRAC_CORE);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_ENG_FRAC_EM", &tau_0_sub_neu0_cellBased_ENG_FRAC_EM, &b_tau_0_sub_neu0_cellBased_ENG_FRAC_EM);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_ENG_FRAC_MAX", &tau_0_sub_neu0_cellBased_ENG_FRAC_MAX, &b_tau_0_sub_neu0_cellBased_ENG_FRAC_MAX);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_FIRST_ETA", &tau_0_sub_neu0_cellBased_FIRST_ETA, &b_tau_0_sub_neu0_cellBased_FIRST_ETA);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_LATERAL", &tau_0_sub_neu0_cellBased_LATERAL, &b_tau_0_sub_neu0_cellBased_LATERAL);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_LONGITUDINAL", &tau_0_sub_neu0_cellBased_LONGITUDINAL, &b_tau_0_sub_neu0_cellBased_LONGITUDINAL);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_NHitsInEM1", &tau_0_sub_neu0_cellBased_NHitsInEM1, &b_tau_0_sub_neu0_cellBased_NHitsInEM1);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_NPosECells_EM1", &tau_0_sub_neu0_cellBased_NPosECells_EM1, &b_tau_0_sub_neu0_cellBased_NPosECells_EM1);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_NPosECells_EM2", &tau_0_sub_neu0_cellBased_NPosECells_EM2, &b_tau_0_sub_neu0_cellBased_NPosECells_EM2);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_NPosECells_PS", &tau_0_sub_neu0_cellBased_NPosECells_PS, &b_tau_0_sub_neu0_cellBased_NPosECells_PS);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_SECOND_ENG_DENS", &tau_0_sub_neu0_cellBased_SECOND_ENG_DENS, &b_tau_0_sub_neu0_cellBased_SECOND_ENG_DENS);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_SECOND_LAMBDA", &tau_0_sub_neu0_cellBased_SECOND_LAMBDA, &b_tau_0_sub_neu0_cellBased_SECOND_LAMBDA);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_SECOND_R", &tau_0_sub_neu0_cellBased_SECOND_R, &b_tau_0_sub_neu0_cellBased_SECOND_R);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_asymmetryInEM1WRTTrk", &tau_0_sub_neu0_cellBased_asymmetryInEM1WRTTrk, &b_tau_0_sub_neu0_cellBased_asymmetryInEM1WRTTrk);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_energy_EM1", &tau_0_sub_neu0_cellBased_energy_EM1, &b_tau_0_sub_neu0_cellBased_energy_EM1);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_energy_EM2", &tau_0_sub_neu0_cellBased_energy_EM2, &b_tau_0_sub_neu0_cellBased_energy_EM2);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_eta", &tau_0_sub_neu0_cellBased_eta, &b_tau_0_sub_neu0_cellBased_eta);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM1", &tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM1, &b_tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM1);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM2", &tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM2, &b_tau_0_sub_neu0_cellBased_firstEtaWRTClusterPosition_EM2);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_phi", &tau_0_sub_neu0_cellBased_phi, &b_tau_0_sub_neu0_cellBased_phi);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_pt", &tau_0_sub_neu0_cellBased_pt, &b_tau_0_sub_neu0_cellBased_pt);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM1", &tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM1, &b_tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM1);
   fChain->SetBranchAddress("tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM2", &tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM2, &b_tau_0_sub_neu0_cellBased_secondEtaWRTClusterPosition_EM2);
   fChain->SetBranchAddress("tau_0_sub_neu1_bdtPi0Score", &tau_0_sub_neu1_bdtPi0Score, &b_tau_0_sub_neu1_bdtPi0Score);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_CENTER_LAMBDA", &tau_0_sub_neu1_cellBased_CENTER_LAMBDA, &b_tau_0_sub_neu1_cellBased_CENTER_LAMBDA);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_DELTA_PHI", &tau_0_sub_neu1_cellBased_DELTA_PHI, &b_tau_0_sub_neu1_cellBased_DELTA_PHI);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_DELTA_THETA", &tau_0_sub_neu1_cellBased_DELTA_THETA, &b_tau_0_sub_neu1_cellBased_DELTA_THETA);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_EM1CoreFrac", &tau_0_sub_neu1_cellBased_EM1CoreFrac, &b_tau_0_sub_neu1_cellBased_EM1CoreFrac);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_ENG_FRAC_CORE", &tau_0_sub_neu1_cellBased_ENG_FRAC_CORE, &b_tau_0_sub_neu1_cellBased_ENG_FRAC_CORE);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_ENG_FRAC_EM", &tau_0_sub_neu1_cellBased_ENG_FRAC_EM, &b_tau_0_sub_neu1_cellBased_ENG_FRAC_EM);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_ENG_FRAC_MAX", &tau_0_sub_neu1_cellBased_ENG_FRAC_MAX, &b_tau_0_sub_neu1_cellBased_ENG_FRAC_MAX);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_FIRST_ETA", &tau_0_sub_neu1_cellBased_FIRST_ETA, &b_tau_0_sub_neu1_cellBased_FIRST_ETA);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_LATERAL", &tau_0_sub_neu1_cellBased_LATERAL, &b_tau_0_sub_neu1_cellBased_LATERAL);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_LONGITUDINAL", &tau_0_sub_neu1_cellBased_LONGITUDINAL, &b_tau_0_sub_neu1_cellBased_LONGITUDINAL);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_NHitsInEM1", &tau_0_sub_neu1_cellBased_NHitsInEM1, &b_tau_0_sub_neu1_cellBased_NHitsInEM1);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_NPosECells_EM1", &tau_0_sub_neu1_cellBased_NPosECells_EM1, &b_tau_0_sub_neu1_cellBased_NPosECells_EM1);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_NPosECells_EM2", &tau_0_sub_neu1_cellBased_NPosECells_EM2, &b_tau_0_sub_neu1_cellBased_NPosECells_EM2);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_NPosECells_PS", &tau_0_sub_neu1_cellBased_NPosECells_PS, &b_tau_0_sub_neu1_cellBased_NPosECells_PS);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_SECOND_ENG_DENS", &tau_0_sub_neu1_cellBased_SECOND_ENG_DENS, &b_tau_0_sub_neu1_cellBased_SECOND_ENG_DENS);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_SECOND_LAMBDA", &tau_0_sub_neu1_cellBased_SECOND_LAMBDA, &b_tau_0_sub_neu1_cellBased_SECOND_LAMBDA);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_SECOND_R", &tau_0_sub_neu1_cellBased_SECOND_R, &b_tau_0_sub_neu1_cellBased_SECOND_R);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_asymmetryInEM1WRTTrk", &tau_0_sub_neu1_cellBased_asymmetryInEM1WRTTrk, &b_tau_0_sub_neu1_cellBased_asymmetryInEM1WRTTrk);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_energy_EM1", &tau_0_sub_neu1_cellBased_energy_EM1, &b_tau_0_sub_neu1_cellBased_energy_EM1);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_energy_EM2", &tau_0_sub_neu1_cellBased_energy_EM2, &b_tau_0_sub_neu1_cellBased_energy_EM2);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_eta", &tau_0_sub_neu1_cellBased_eta, &b_tau_0_sub_neu1_cellBased_eta);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM1", &tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM1, &b_tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM1);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM2", &tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM2, &b_tau_0_sub_neu1_cellBased_firstEtaWRTClusterPosition_EM2);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_phi", &tau_0_sub_neu1_cellBased_phi, &b_tau_0_sub_neu1_cellBased_phi);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_pt", &tau_0_sub_neu1_cellBased_pt, &b_tau_0_sub_neu1_cellBased_pt);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM1", &tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM1, &b_tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM1);
   fChain->SetBranchAddress("tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM2", &tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM2, &b_tau_0_sub_neu1_cellBased_secondEtaWRTClusterPosition_EM2);
   fChain->SetBranchAddress("tau_0_tauRec_eta", &tau_0_tauRec_eta, &b_tau_0_tauRec_eta);
   fChain->SetBranchAddress("tau_0_tauRec_m", &tau_0_tauRec_m, &b_tau_0_tauRec_m);
   fChain->SetBranchAddress("tau_0_tauRec_phi", &tau_0_tauRec_phi, &b_tau_0_tauRec_phi);
   fChain->SetBranchAddress("tau_0_tauRec_pt", &tau_0_tauRec_pt, &b_tau_0_tauRec_pt);
   fChain->SetBranchAddress("tau_0_trig1_HLT_ChPiEMEOverCaloEME", &tau_0_trig1_HLT_ChPiEMEOverCaloEME, &b_tau_0_trig1_HLT_ChPiEMEOverCaloEME);
   fChain->SetBranchAddress("tau_0_trig1_HLT_ChPiEMEOverCaloEMECorrected", &tau_0_trig1_HLT_ChPiEMEOverCaloEMECorrected, &b_tau_0_trig1_HLT_ChPiEMEOverCaloEMECorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_EMPOverTrkSysP", &tau_0_trig1_HLT_EMPOverTrkSysP, &b_tau_0_trig1_HLT_EMPOverTrkSysP);
   fChain->SetBranchAddress("tau_0_trig1_HLT_EMPOverTrkSysPCorrected", &tau_0_trig1_HLT_EMPOverTrkSysPCorrected, &b_tau_0_trig1_HLT_EMPOverTrkSysPCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_SumPtTrkFrac", &tau_0_trig1_HLT_SumPtTrkFrac, &b_tau_0_trig1_HLT_SumPtTrkFrac);
   fChain->SetBranchAddress("tau_0_trig1_HLT_SumPtTrkFracCorrected", &tau_0_trig1_HLT_SumPtTrkFracCorrected, &b_tau_0_trig1_HLT_SumPtTrkFracCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_centFrac", &tau_0_trig1_HLT_centFrac, &b_tau_0_trig1_HLT_centFrac);
   fChain->SetBranchAddress("tau_0_trig1_HLT_centFracCorrected", &tau_0_trig1_HLT_centFracCorrected, &b_tau_0_trig1_HLT_centFracCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_dRmax", &tau_0_trig1_HLT_dRmax, &b_tau_0_trig1_HLT_dRmax);
   fChain->SetBranchAddress("tau_0_trig1_HLT_dRmaxCorrected", &tau_0_trig1_HLT_dRmaxCorrected, &b_tau_0_trig1_HLT_dRmaxCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_eraw", &tau_0_trig1_HLT_eraw, &b_tau_0_trig1_HLT_eraw);
   fChain->SetBranchAddress("tau_0_trig1_HLT_etOverPtLeadTrk", &tau_0_trig1_HLT_etOverPtLeadTrk, &b_tau_0_trig1_HLT_etOverPtLeadTrk);
   fChain->SetBranchAddress("tau_0_trig1_HLT_etOverPtLeadTrkCorrected", &tau_0_trig1_HLT_etOverPtLeadTrkCorrected, &b_tau_0_trig1_HLT_etOverPtLeadTrkCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_eta", &tau_0_trig1_HLT_eta, &b_tau_0_trig1_HLT_eta);
   fChain->SetBranchAddress("tau_0_trig1_HLT_etraw", &tau_0_trig1_HLT_etraw, &b_tau_0_trig1_HLT_etraw);
   fChain->SetBranchAddress("tau_0_trig1_HLT_innerTrkAvgDist", &tau_0_trig1_HLT_innerTrkAvgDist, &b_tau_0_trig1_HLT_innerTrkAvgDist);
   fChain->SetBranchAddress("tau_0_trig1_HLT_innerTrkAvgDistCorrected", &tau_0_trig1_HLT_innerTrkAvgDistCorrected, &b_tau_0_trig1_HLT_innerTrkAvgDistCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_ipSigLeadTrk", &tau_0_trig1_HLT_ipSigLeadTrk, &b_tau_0_trig1_HLT_ipSigLeadTrk);
   fChain->SetBranchAddress("tau_0_trig1_HLT_ipSigLeadTrkCorrected", &tau_0_trig1_HLT_ipSigLeadTrkCorrected, &b_tau_0_trig1_HLT_ipSigLeadTrkCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_jet_bdt_loose", &tau_0_trig1_HLT_jet_bdt_loose, &b_tau_0_trig1_HLT_jet_bdt_loose);
   fChain->SetBranchAddress("tau_0_trig1_HLT_jet_bdt_medium", &tau_0_trig1_HLT_jet_bdt_medium, &b_tau_0_trig1_HLT_jet_bdt_medium);
   fChain->SetBranchAddress("tau_0_trig1_HLT_jet_bdt_score", &tau_0_trig1_HLT_jet_bdt_score, &b_tau_0_trig1_HLT_jet_bdt_score);
   fChain->SetBranchAddress("tau_0_trig1_HLT_jet_bdt_tight", &tau_0_trig1_HLT_jet_bdt_tight, &b_tau_0_trig1_HLT_jet_bdt_tight);
   fChain->SetBranchAddress("tau_0_trig1_HLT_m", &tau_0_trig1_HLT_m, &b_tau_0_trig1_HLT_m);
   fChain->SetBranchAddress("tau_0_trig1_HLT_mEflowApprox", &tau_0_trig1_HLT_mEflowApprox, &b_tau_0_trig1_HLT_mEflowApprox);
   fChain->SetBranchAddress("tau_0_trig1_HLT_mEflowApproxCorrected", &tau_0_trig1_HLT_mEflowApproxCorrected, &b_tau_0_trig1_HLT_mEflowApproxCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_massTrkSys", &tau_0_trig1_HLT_massTrkSys, &b_tau_0_trig1_HLT_massTrkSys);
   fChain->SetBranchAddress("tau_0_trig1_HLT_massTrkSysCorrected", &tau_0_trig1_HLT_massTrkSysCorrected, &b_tau_0_trig1_HLT_massTrkSysCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_matched", &tau_0_trig1_HLT_matched, &b_tau_0_trig1_HLT_matched);
   fChain->SetBranchAddress("tau_0_trig1_HLT_n_charged_tracks", &tau_0_trig1_HLT_n_charged_tracks, &b_tau_0_trig1_HLT_n_charged_tracks);
   fChain->SetBranchAddress("tau_0_trig1_HLT_n_isolation_tracks", &tau_0_trig1_HLT_n_isolation_tracks, &b_tau_0_trig1_HLT_n_isolation_tracks);
   fChain->SetBranchAddress("tau_0_trig1_HLT_phi", &tau_0_trig1_HLT_phi, &b_tau_0_trig1_HLT_phi);
   fChain->SetBranchAddress("tau_0_trig1_HLT_pt", &tau_0_trig1_HLT_pt, &b_tau_0_trig1_HLT_pt);
   fChain->SetBranchAddress("tau_0_trig1_HLT_ptRatioEflowApprox", &tau_0_trig1_HLT_ptRatioEflowApprox, &b_tau_0_trig1_HLT_ptRatioEflowApprox);
   fChain->SetBranchAddress("tau_0_trig1_HLT_ptRatioEflowApproxCorrected", &tau_0_trig1_HLT_ptRatioEflowApproxCorrected, &b_tau_0_trig1_HLT_ptRatioEflowApproxCorrected);
   fChain->SetBranchAddress("tau_0_trig1_HLT_trFlightPathSig", &tau_0_trig1_HLT_trFlightPathSig, &b_tau_0_trig1_HLT_trFlightPathSig);
   fChain->SetBranchAddress("tau_0_trig1_HLT_trFlightPathSigCorrected", &tau_0_trig1_HLT_trFlightPathSigCorrected, &b_tau_0_trig1_HLT_trFlightPathSigCorrected);
   fChain->SetBranchAddress("tau_0_trig2_PreselTrig_eta", &tau_0_trig2_PreselTrig_eta, &b_tau_0_trig2_PreselTrig_eta);
   fChain->SetBranchAddress("tau_0_trig2_PreselTrig_m", &tau_0_trig2_PreselTrig_m, &b_tau_0_trig2_PreselTrig_m);
   fChain->SetBranchAddress("tau_0_trig2_PreselTrig_matched", &tau_0_trig2_PreselTrig_matched, &b_tau_0_trig2_PreselTrig_matched);
   fChain->SetBranchAddress("tau_0_trig2_PreselTrig_n_charged_tracks", &tau_0_trig2_PreselTrig_n_charged_tracks, &b_tau_0_trig2_PreselTrig_n_charged_tracks);
   fChain->SetBranchAddress("tau_0_trig2_PreselTrig_n_isolation_tracks", &tau_0_trig2_PreselTrig_n_isolation_tracks, &b_tau_0_trig2_PreselTrig_n_isolation_tracks);
   fChain->SetBranchAddress("tau_0_trig2_PreselTrig_phi", &tau_0_trig2_PreselTrig_phi, &b_tau_0_trig2_PreselTrig_phi);
   fChain->SetBranchAddress("tau_0_trig2_PreselTrig_pt", &tau_0_trig2_PreselTrig_pt, &b_tau_0_trig2_PreselTrig_pt);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo", &tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo, &b_tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I", &tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I, &b_tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25", &tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25, &b_tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM", &tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM, &b_tau_0_trig_HLT_mu14_iloose_tau25_medium1_tracktwo_L1MU10_TAU12IM);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_iloose_tau35_medium1_tracktwo", &tau_0_trig_HLT_mu14_iloose_tau35_medium1_tracktwo, &b_tau_0_trig_HLT_mu14_iloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo", &tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I", &tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I, &b_tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25", &tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25, &b_tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM", &tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM, &b_tau_0_trig_HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM);
   fChain->SetBranchAddress("tau_0_trig_HLT_mu14_ivarloose_tau35_medium1_tracktwo", &tau_0_trig_HLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_tau_0_trig_HLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau125_medium1_tracktwo", &tau_0_trig_HLT_tau125_medium1_tracktwo, &b_tau_0_trig_HLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau160_medium1_tracktwo", &tau_0_trig_HLT_tau160_medium1_tracktwo, &b_tau_0_trig_HLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_idperf_track", &tau_0_trig_HLT_tau25_idperf_track, &b_tau_0_trig_HLT_tau25_idperf_track);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_idperf_tracktwo", &tau_0_trig_HLT_tau25_idperf_tracktwo, &b_tau_0_trig_HLT_tau25_idperf_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_loose1_ptonly", &tau_0_trig_HLT_tau25_loose1_ptonly, &b_tau_0_trig_HLT_tau25_loose1_ptonly);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_loose1_tracktwo", &tau_0_trig_HLT_tau25_loose1_tracktwo, &b_tau_0_trig_HLT_tau25_loose1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_medium1_mvonly", &tau_0_trig_HLT_tau25_medium1_mvonly, &b_tau_0_trig_HLT_tau25_medium1_mvonly);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_medium1_ptonly", &tau_0_trig_HLT_tau25_medium1_ptonly, &b_tau_0_trig_HLT_tau25_medium1_ptonly);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_medium1_track", &tau_0_trig_HLT_tau25_medium1_track, &b_tau_0_trig_HLT_tau25_medium1_track);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_medium1_tracktwo", &tau_0_trig_HLT_tau25_medium1_tracktwo, &b_tau_0_trig_HLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12", &tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12, &b_tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IL", &tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IL, &b_tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IL);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IT", &tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IT, &b_tau_0_trig_HLT_tau25_medium1_tracktwo_L1TAU12IT);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_perf_ptonly", &tau_0_trig_HLT_tau25_perf_ptonly, &b_tau_0_trig_HLT_tau25_perf_ptonly);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_perf_track", &tau_0_trig_HLT_tau25_perf_track, &b_tau_0_trig_HLT_tau25_perf_track);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_perf_tracktwo", &tau_0_trig_HLT_tau25_perf_tracktwo, &b_tau_0_trig_HLT_tau25_perf_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_tight1_ptonly", &tau_0_trig_HLT_tau25_tight1_ptonly, &b_tau_0_trig_HLT_tau25_tight1_ptonly);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau25_tight1_tracktwo", &tau_0_trig_HLT_tau25_tight1_tracktwo, &b_tau_0_trig_HLT_tau25_tight1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau35_medium1_tracktwo", &tau_0_trig_HLT_tau35_medium1_tracktwo, &b_tau_0_trig_HLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau50_medium1_tracktwo_L1TAU12", &tau_0_trig_HLT_tau50_medium1_tracktwo_L1TAU12, &b_tau_0_trig_HLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau80_medium1_tracktwo", &tau_0_trig_HLT_tau80_medium1_tracktwo, &b_tau_0_trig_HLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("tau_0_trig_HLT_tau80_medium1_tracktwo_L1TAU60", &tau_0_trig_HLT_tau80_medium1_tracktwo_L1TAU60, &b_tau_0_trig_HLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("tau_0_trig_L1_J25", &tau_0_trig_L1_J25, &b_tau_0_trig_L1_J25);
   fChain->SetBranchAddress("tau_0_trig_L1_TAU12", &tau_0_trig_L1_TAU12, &b_tau_0_trig_L1_TAU12);
   fChain->SetBranchAddress("tau_0_trig_L1_TAU12IM", &tau_0_trig_L1_TAU12IM, &b_tau_0_trig_L1_TAU12IM);
   fChain->SetBranchAddress("tau_0_trig_L1_TAU20", &tau_0_trig_L1_TAU20, &b_tau_0_trig_L1_TAU20);
   fChain->SetBranchAddress("tau_0_trig_L1_TAU20IM", &tau_0_trig_L1_TAU20IM, &b_tau_0_trig_L1_TAU20IM);
   fChain->SetBranchAddress("tau_0_trig_trigger_matched", &tau_0_trig_trigger_matched, &b_tau_0_trig_trigger_matched);
   fChain->SetBranchAddress("tau_0_trk_multi_ckt4iso_n", &tau_0_trk_multi_ckt4iso_n, &b_tau_0_trk_multi_ckt4iso_n);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr60_d04_n_l5", &tau_0_trk_multi_cw_dr60_d04_n_l5, &b_tau_0_trk_multi_cw_dr60_d04_n_l5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr60_d04_n_lp5", &tau_0_trk_multi_cw_dr60_d04_n_lp5, &b_tau_0_trk_multi_cw_dr60_d04_n_lp5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr60_d04_n_t5", &tau_0_trk_multi_cw_dr60_d04_n_t5, &b_tau_0_trk_multi_cw_dr60_d04_n_t5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr60_n_l5", &tau_0_trk_multi_cw_dr60_n_l5, &b_tau_0_trk_multi_cw_dr60_n_l5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr60_n_lp5", &tau_0_trk_multi_cw_dr60_n_lp5, &b_tau_0_trk_multi_cw_dr60_n_lp5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr60_n_t5", &tau_0_trk_multi_cw_dr60_n_t5, &b_tau_0_trk_multi_cw_dr60_n_t5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr80_d04_n_l5", &tau_0_trk_multi_cw_dr80_d04_n_l5, &b_tau_0_trk_multi_cw_dr80_d04_n_l5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr80_d04_n_lp5", &tau_0_trk_multi_cw_dr80_d04_n_lp5, &b_tau_0_trk_multi_cw_dr80_d04_n_lp5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr80_d04_n_t5", &tau_0_trk_multi_cw_dr80_d04_n_t5, &b_tau_0_trk_multi_cw_dr80_d04_n_t5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr80_n_l5", &tau_0_trk_multi_cw_dr80_n_l5, &b_tau_0_trk_multi_cw_dr80_n_l5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr80_n_lp5", &tau_0_trk_multi_cw_dr80_n_lp5, &b_tau_0_trk_multi_cw_dr80_n_lp5);
   fChain->SetBranchAddress("tau_0_trk_multi_cw_dr80_n_t5", &tau_0_trk_multi_cw_dr80_n_t5, &b_tau_0_trk_multi_cw_dr80_n_t5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr60_d04_n_l5", &tau_0_trk_multi_cws_dr60_d04_n_l5, &b_tau_0_trk_multi_cws_dr60_d04_n_l5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr60_d04_n_lp5", &tau_0_trk_multi_cws_dr60_d04_n_lp5, &b_tau_0_trk_multi_cws_dr60_d04_n_lp5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr60_d04_n_t5", &tau_0_trk_multi_cws_dr60_d04_n_t5, &b_tau_0_trk_multi_cws_dr60_d04_n_t5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr60_n_l5", &tau_0_trk_multi_cws_dr60_n_l5, &b_tau_0_trk_multi_cws_dr60_n_l5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr60_n_lp5", &tau_0_trk_multi_cws_dr60_n_lp5, &b_tau_0_trk_multi_cws_dr60_n_lp5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr60_n_t5", &tau_0_trk_multi_cws_dr60_n_t5, &b_tau_0_trk_multi_cws_dr60_n_t5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr80_d04_n_l5", &tau_0_trk_multi_cws_dr80_d04_n_l5, &b_tau_0_trk_multi_cws_dr80_d04_n_l5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr80_d04_n_lp5", &tau_0_trk_multi_cws_dr80_d04_n_lp5, &b_tau_0_trk_multi_cws_dr80_d04_n_lp5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr80_d04_n_t5", &tau_0_trk_multi_cws_dr80_d04_n_t5, &b_tau_0_trk_multi_cws_dr80_d04_n_t5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr80_n_l5", &tau_0_trk_multi_cws_dr80_n_l5, &b_tau_0_trk_multi_cws_dr80_n_l5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr80_n_lp5", &tau_0_trk_multi_cws_dr80_n_lp5, &b_tau_0_trk_multi_cws_dr80_n_lp5);
   fChain->SetBranchAddress("tau_0_trk_multi_cws_dr80_n_t5", &tau_0_trk_multi_cws_dr80_n_t5, &b_tau_0_trk_multi_cws_dr80_n_t5);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_n_had_tracks", &tau_0_trk_multi_diag_n_had_tracks, &b_tau_0_trk_multi_diag_n_had_tracks);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_cv_tracks", &tau_0_trk_multi_diag_n_spu_cv_tracks, &b_tau_0_trk_multi_diag_n_spu_cv_tracks);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_fk_tracks", &tau_0_trk_multi_diag_n_spu_fk_tracks, &b_tau_0_trk_multi_diag_n_spu_fk_tracks);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_pu_tracks", &tau_0_trk_multi_diag_n_spu_pu_tracks, &b_tau_0_trk_multi_diag_n_spu_pu_tracks);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_tracks", &tau_0_trk_multi_diag_n_spu_tracks, &b_tau_0_trk_multi_diag_n_spu_tracks);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_uc_tracks", &tau_0_trk_multi_diag_n_spu_uc_tracks, &b_tau_0_trk_multi_diag_n_spu_uc_tracks);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_ue_tracks", &tau_0_trk_multi_diag_n_spu_ue_tracks, &b_tau_0_trk_multi_diag_n_spu_ue_tracks);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_n_tau_tracks", &tau_0_trk_multi_diag_n_tau_tracks, &b_tau_0_trk_multi_diag_n_tau_tracks);
   fChain->SetBranchAddress("tau_0_trk_multi_diag_purity", &tau_0_trk_multi_diag_purity, &b_tau_0_trk_multi_diag_purity);
   fChain->SetBranchAddress("tau_0_truth", &tau_0_truth, &b_tau_0_truth);
   fChain->SetBranchAddress("tau_0_truth_classifierParticleOrigin", &tau_0_truth_classifierParticleOrigin, &b_tau_0_truth_classifierParticleOrigin);
   fChain->SetBranchAddress("tau_0_truth_classifierParticleType", &tau_0_truth_classifierParticleType, &b_tau_0_truth_classifierParticleType);
   fChain->SetBranchAddress("tau_0_truth_decay_mode", &tau_0_truth_decay_mode, &b_tau_0_truth_decay_mode);
   fChain->SetBranchAddress("tau_0_truth_et", &tau_0_truth_et, &b_tau_0_truth_et);
   fChain->SetBranchAddress("tau_0_truth_eta", &tau_0_truth_eta, &b_tau_0_truth_eta);
   fChain->SetBranchAddress("tau_0_truth_eta_vis", &tau_0_truth_eta_vis, &b_tau_0_truth_eta_vis);
   fChain->SetBranchAddress("tau_0_truth_eta_vis_charged", &tau_0_truth_eta_vis_charged, &b_tau_0_truth_eta_vis_charged);
   fChain->SetBranchAddress("tau_0_truth_eta_vis_neutral", &tau_0_truth_eta_vis_neutral, &b_tau_0_truth_eta_vis_neutral);
   fChain->SetBranchAddress("tau_0_truth_isEle", &tau_0_truth_isEle, &b_tau_0_truth_isEle);
   fChain->SetBranchAddress("tau_0_truth_isHadTau", &tau_0_truth_isHadTau, &b_tau_0_truth_isHadTau);
   fChain->SetBranchAddress("tau_0_truth_isJet", &tau_0_truth_isJet, &b_tau_0_truth_isJet);
   fChain->SetBranchAddress("tau_0_truth_isMuon", &tau_0_truth_isMuon, &b_tau_0_truth_isMuon);
   fChain->SetBranchAddress("tau_0_truth_isTau", &tau_0_truth_isTau, &b_tau_0_truth_isTau);
   fChain->SetBranchAddress("tau_0_truth_m", &tau_0_truth_m, &b_tau_0_truth_m);
   fChain->SetBranchAddress("tau_0_truth_m_vis", &tau_0_truth_m_vis, &b_tau_0_truth_m_vis);
   fChain->SetBranchAddress("tau_0_truth_m_vis_charged", &tau_0_truth_m_vis_charged, &b_tau_0_truth_m_vis_charged);
   fChain->SetBranchAddress("tau_0_truth_m_vis_neutral", &tau_0_truth_m_vis_neutral, &b_tau_0_truth_m_vis_neutral);
   fChain->SetBranchAddress("tau_0_truth_mother_pdgId", &tau_0_truth_mother_pdgId, &b_tau_0_truth_mother_pdgId);
   fChain->SetBranchAddress("tau_0_truth_mother_status", &tau_0_truth_mother_status, &b_tau_0_truth_mother_status);
   fChain->SetBranchAddress("tau_0_truth_n_charged", &tau_0_truth_n_charged, &b_tau_0_truth_n_charged);
   fChain->SetBranchAddress("tau_0_truth_n_charged_pion", &tau_0_truth_n_charged_pion, &b_tau_0_truth_n_charged_pion);
   fChain->SetBranchAddress("tau_0_truth_n_neutral", &tau_0_truth_n_neutral, &b_tau_0_truth_n_neutral);
   fChain->SetBranchAddress("tau_0_truth_n_neutral_pion", &tau_0_truth_n_neutral_pion, &b_tau_0_truth_n_neutral_pion);
   fChain->SetBranchAddress("tau_0_truth_origin", &tau_0_truth_origin, &b_tau_0_truth_origin);
   fChain->SetBranchAddress("tau_0_truth_pdgId", &tau_0_truth_pdgId, &b_tau_0_truth_pdgId);
   fChain->SetBranchAddress("tau_0_truth_phi", &tau_0_truth_phi, &b_tau_0_truth_phi);
   fChain->SetBranchAddress("tau_0_truth_phi_vis", &tau_0_truth_phi_vis, &b_tau_0_truth_phi_vis);
   fChain->SetBranchAddress("tau_0_truth_phi_vis_charged", &tau_0_truth_phi_vis_charged, &b_tau_0_truth_phi_vis_charged);
   fChain->SetBranchAddress("tau_0_truth_phi_vis_neutral", &tau_0_truth_phi_vis_neutral, &b_tau_0_truth_phi_vis_neutral);
   fChain->SetBranchAddress("tau_0_truth_pt", &tau_0_truth_pt, &b_tau_0_truth_pt);
   fChain->SetBranchAddress("tau_0_truth_pt_vis", &tau_0_truth_pt_vis, &b_tau_0_truth_pt_vis);
   fChain->SetBranchAddress("tau_0_truth_pt_vis_charged", &tau_0_truth_pt_vis_charged, &b_tau_0_truth_pt_vis_charged);
   fChain->SetBranchAddress("tau_0_truth_pt_vis_neutral", &tau_0_truth_pt_vis_neutral, &b_tau_0_truth_pt_vis_neutral);
   fChain->SetBranchAddress("tau_0_truth_pz", &tau_0_truth_pz, &b_tau_0_truth_pz);
   fChain->SetBranchAddress("tau_0_truth_q", &tau_0_truth_q, &b_tau_0_truth_q);
   fChain->SetBranchAddress("tau_0_truth_status", &tau_0_truth_status, &b_tau_0_truth_status);
   fChain->SetBranchAddress("tau_0_truth_type", &tau_0_truth_type, &b_tau_0_truth_type);
   fChain->SetBranchAddress("tau_1", &tau_1, &b_tau_1);
   fChain->SetBranchAddress("tau_1_allTrk_eta", &tau_1_allTrk_eta, &b_tau_1_allTrk_eta);
   fChain->SetBranchAddress("tau_1_allTrk_n", &tau_1_allTrk_n, &b_tau_1_allTrk_n);
   fChain->SetBranchAddress("tau_1_allTrk_phi", &tau_1_allTrk_phi, &b_tau_1_allTrk_phi);
   fChain->SetBranchAddress("tau_1_allTrk_pt", &tau_1_allTrk_pt, &b_tau_1_allTrk_pt);
   fChain->SetBranchAddress("tau_1_decay_mode", &tau_1_decay_mode, &b_tau_1_decay_mode);
   fChain->SetBranchAddress("tau_1_ele_BDTEleScoreTrans_run2", &tau_1_ele_BDTEleScoreTrans_run2, &b_tau_1_ele_BDTEleScoreTrans_run2);
   fChain->SetBranchAddress("tau_1_ele_bdt_loose", &tau_1_ele_bdt_loose, &b_tau_1_ele_bdt_loose);
   fChain->SetBranchAddress("tau_1_ele_bdt_medium", &tau_1_ele_bdt_medium, &b_tau_1_ele_bdt_medium);
   fChain->SetBranchAddress("tau_1_ele_bdt_score", &tau_1_ele_bdt_score, &b_tau_1_ele_bdt_score);
   fChain->SetBranchAddress("tau_1_ele_bdt_tight", &tau_1_ele_bdt_tight, &b_tau_1_ele_bdt_tight);
   fChain->SetBranchAddress("tau_1_ele_match_lhscore", &tau_1_ele_match_lhscore, &b_tau_1_ele_match_lhscore);
   fChain->SetBranchAddress("tau_1_ele_olr_pass", &tau_1_ele_olr_pass, &b_tau_1_ele_olr_pass);
   fChain->SetBranchAddress("tau_1_et", &tau_1_et, &b_tau_1_et);
   fChain->SetBranchAddress("tau_1_eta", &tau_1_eta, &b_tau_1_eta);
   fChain->SetBranchAddress("tau_1_jet_bdt_loose", &tau_1_jet_bdt_loose, &b_tau_1_jet_bdt_loose);
   fChain->SetBranchAddress("tau_1_jet_bdt_medium", &tau_1_jet_bdt_medium, &b_tau_1_jet_bdt_medium);
   fChain->SetBranchAddress("tau_1_jet_bdt_score", &tau_1_jet_bdt_score, &b_tau_1_jet_bdt_score);
   fChain->SetBranchAddress("tau_1_jet_bdt_score_trans", &tau_1_jet_bdt_score_trans, &b_tau_1_jet_bdt_score_trans);
   fChain->SetBranchAddress("tau_1_jet_bdt_tight", &tau_1_jet_bdt_tight, &b_tau_1_jet_bdt_tight);
   fChain->SetBranchAddress("tau_1_jet_width", &tau_1_jet_width, &b_tau_1_jet_width);
   fChain->SetBranchAddress("tau_1_leadTrk_eta", &tau_1_leadTrk_eta, &b_tau_1_leadTrk_eta);
   fChain->SetBranchAddress("tau_1_leadTrk_phi", &tau_1_leadTrk_phi, &b_tau_1_leadTrk_phi);
   fChain->SetBranchAddress("tau_1_leadTrk_pt", &tau_1_leadTrk_pt, &b_tau_1_leadTrk_pt);
   fChain->SetBranchAddress("tau_1_m", &tau_1_m, &b_tau_1_m);
   fChain->SetBranchAddress("tau_1_mvx", &tau_1_mvx, &b_tau_1_mvx);
   fChain->SetBranchAddress("tau_1_mvx_tagged", &tau_1_mvx_tagged, &b_tau_1_mvx_tagged);
   fChain->SetBranchAddress("tau_1_n_all_tracks", &tau_1_n_all_tracks, &b_tau_1_n_all_tracks);
   fChain->SetBranchAddress("tau_1_n_charged_tracks", &tau_1_n_charged_tracks, &b_tau_1_n_charged_tracks);
   fChain->SetBranchAddress("tau_1_n_conversion_tracks", &tau_1_n_conversion_tracks, &b_tau_1_n_conversion_tracks);
   fChain->SetBranchAddress("tau_1_n_core_tracks", &tau_1_n_core_tracks, &b_tau_1_n_core_tracks);
   fChain->SetBranchAddress("tau_1_n_failTrackFilter_tracks", &tau_1_n_failTrackFilter_tracks, &b_tau_1_n_failTrackFilter_tracks);
   fChain->SetBranchAddress("tau_1_n_fake_tracks", &tau_1_n_fake_tracks, &b_tau_1_n_fake_tracks);
   fChain->SetBranchAddress("tau_1_n_isolation_tracks", &tau_1_n_isolation_tracks, &b_tau_1_n_isolation_tracks);
   fChain->SetBranchAddress("tau_1_n_modified_isolation_tracks", &tau_1_n_modified_isolation_tracks, &b_tau_1_n_modified_isolation_tracks);
   fChain->SetBranchAddress("tau_1_n_old_tracks", &tau_1_n_old_tracks, &b_tau_1_n_old_tracks);
   fChain->SetBranchAddress("tau_1_n_passTrkSelectionTight_tracks", &tau_1_n_passTrkSelectionTight_tracks, &b_tau_1_n_passTrkSelectionTight_tracks);
   fChain->SetBranchAddress("tau_1_n_passTrkSelector_tracks", &tau_1_n_passTrkSelector_tracks, &b_tau_1_n_passTrkSelector_tracks);
   fChain->SetBranchAddress("tau_1_n_unclassified_tracks", &tau_1_n_unclassified_tracks, &b_tau_1_n_unclassified_tracks);
   fChain->SetBranchAddress("tau_1_n_wide_tracks", &tau_1_n_wide_tracks, &b_tau_1_n_wide_tracks);
   fChain->SetBranchAddress("tau_1_phi", &tau_1_phi, &b_tau_1_phi);
   fChain->SetBranchAddress("tau_1_pt", &tau_1_pt, &b_tau_1_pt);
   fChain->SetBranchAddress("tau_1_q", &tau_1_q, &b_tau_1_q);
   fChain->SetBranchAddress("tau_1_tauRec_eta", &tau_1_tauRec_eta, &b_tau_1_tauRec_eta);
   fChain->SetBranchAddress("tau_1_tauRec_m", &tau_1_tauRec_m, &b_tau_1_tauRec_m);
   fChain->SetBranchAddress("tau_1_tauRec_phi", &tau_1_tauRec_phi, &b_tau_1_tauRec_phi);
   fChain->SetBranchAddress("tau_1_tauRec_pt", &tau_1_tauRec_pt, &b_tau_1_tauRec_pt);
   fChain->SetBranchAddress("tau_tes_alpha_pt_shift", &tau_tes_alpha_pt_shift, &b_tau_tes_alpha_pt_shift);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_total", &weight_total, &b_weight_total);
   Notify();
}

Bool_t AF2Tree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void AF2Tree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t AF2Tree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
void AF2Tree::updateCuts() {
    //link the cut here
		// ------------------------------2015 Trigger----------------------------------------
	bool MuonTrigger15A = (HLT_mu20_iloose_L1MU15 || HLT_mu50);
	bool MuonTrigger15B = (HLT_mu20_iloose_L1MU15 || HLT_mu40);
	//bool EleTrigger15 = (HLT_e24_lhmedium_L1EM20VH || HLT_e60_lhmedium || HLT_e120_lhloose);


	//-------------------------------2016 Trigger-----------------------------------------
	bool MuonTrigger16A = (HLT_mu24_imedium || HLT_mu40);
	bool MuonTrigger16B = (HLT_mu24_imedium || HLT_mu50);
	bool MuonTrigger16C = (HLT_mu26_ivarmedium || HLT_mu50);
	//bool EleTrigger16A = (HLT_e24_lhtight_nod0_ivarloose || HLT_e24_lhmedium_nod0_L1EM20VH || HLT_e60_lhmedium_nod0 || HLT_e140_lhloose_nod0);
	//bool EleTrigger16B = (HLT_e26_lhtight_nod0_ivarloose || HLT_e60_lhmedium_nod0 || HLT_e140_lhloose_nod0);

	bool MuonSelection = false;
	float TriggerSF = 0;
	double weight = 1;

	//We set mc_channel_number of data to 0 prevoisly
	if (mc_channel_number != 0) {
		MuonSelection = (lep_0_id_medium == 1 && ((HLT_mu26_ivarmedium == 1 && lep_0_pt > 27.3 && muTrigMatch_0_HLT_mu26_ivarmedium == 1) || (HLT_mu50 == 1 && lep_0_pt > 52.5 && muTrigMatch_0_HLT_mu50 == 1)));
		TriggerSF = lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
		//calculate weight for MC samples
		double intial = GetRunNgenAF2(mc_channel_number);
		//double  lumi = 43013;//for v05
		double  lumi = 32806;//for v03
		double Xsec = GetXsecR21(mc_channel_number);
		double k = GetKFactorR21(mc_channel_number);
		double filter = GetFilterEffR21(mc_channel_number);
		double factor = lep_0_NOMINAL_MuEffSF_Reco_QualMedium * TriggerSF * lep_0_NOMINAL_MuEffSF_TTVA * lep_0_NOMINAL_MuEffSF_IsoGradient;
		//double factor = lep_0_NOMINAL_MuEffSF_Reco_QualMedium * TriggerSF * lep_0_NOMINAL_MuEffSF_TTVA * lep_0_NOMINAL_MuEffSF_IsoGradient * jet_NOMINAL_forward_jets_global_effSF_JVT * jet_NOMINAL_forward_jets_global_ineffSF_JVT * jet_NOMINAL_central_jets_global_effSF_JVT * jet_NOMINAL_central_jets_global_ineffSF_JVT * tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad * tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron * jet_NOMINAL_global_ineffSF_MVX * jet_NOMINAL_global_effSF_MVX * tau_0_NOMINAL_TauEffSF_reco;
		//  double factor = weight_total*jet_NOMINAL_forward_jets_global_effSF_JVT*jet_NOMINAL_forward_jets_global_ineffSF_JVT * jet_NOMINAL_central_jets_global_effSF_JVT * jet_NOMINAL_central_jets_global_ineffSF_JVT * tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad * tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron * jet_NOMINAL_global_ineffSF_MVX * jet_NOMINAL_global_effSF_MVX * lep_0_NOMINAL_MuEffSF_Reco_QualMedium * lep_0_NOMINAL_MuEffSF_TTVA * tau_0_NOMINAL_TauEffSF_reco * lep_0_NOMINAL_MuEffSF_IsoLoose * lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
		weight = weight_total * Xsec*lumi*k*factor*filter / intial;
	}
	else {
		MuonSelection = (lep_0_id_medium == 1 && ((HLT_mu26_ivarmedium == 1 && lep_0_pt > 27.3 && muTrigMatch_0_HLT_mu26_ivarmedium == 1)));
	}

	/******************************************2016 Trigger seeting****************************
	//We set mc_channel_number of data to 0 prevoisly
	if (mc_channel_number != 0) {
		if (296939 <= NOMINAL_pileup_random_run_number && NOMINAL_pileup_random_run_number <= 300345) { //2016 periodA
			MuonSelection = (lep_0_id_medium == 1 && ((HLT_mu24_imedium == 1 && lep_0_pt > 25.2 && muTrigMatch_0_HLT_mu24_imedium == 1)));
			TriggerSF = lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu40_QualMedium_IsoNone;
		}
		else if (300345 <= NOMINAL_pileup_random_run_number && NOMINAL_pileup_random_run_number <= 302919) { //2016 periodB-D3
			MuonSelection = (lep_0_id_medium == 1 && ((HLT_mu24_imedium == 1 && lep_0_pt > 25.2 && muTrigMatch_0_HLT_mu24_imedium == 1)));
			TriggerSF = lep_0_NOMINAL_MuEffSF_HLT_mu24_imedium_OR_HLT_mu50_QualMedium_IsoNone;
		}
		else if (302919 <= NOMINAL_pileup_random_run_number) { //2016 periodD4-
			MuonSelection = (lep_0_id_medium == 1 && ((HLT_mu26_ivarmedium == 1 && lep_0_pt > 27.3 && muTrigMatch_0_HLT_mu26_ivarmedium == 1)));
			TriggerSF = lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
		}
		//calculate weight for MC samples
		double intial = GetRunNgenR21(mc_channel_number);
		//double  lumi = 32861;//for V02
		//double  lumi = 43013;//for v05
		double  lumi = 32806;//for v03
		double Xsec = GetXsecR21(mc_channel_number);
		double k = GetKFactorR21(mc_channel_number);
		double filter = GetFilterEffR21(mc_channel_number);
		//    double factor = lep_0_NOMINAL_HLT_mu20_iloose_L1MU15_MU_TRIG_QUAL_MEDIUM_MU_TRIG_ISO_LOOSE*lep_0_NOMINAL_effSF_IsoLoose;
		double factor = lep_0_NOMINAL_MuEffSF_Reco_QualMedium * TriggerSF * lep_0_NOMINAL_MuEffSF_TTVA * lep_0_NOMINAL_MuEffSF_IsoGradient;
		weight = weight_total*Xsec*lumi*k*factor*filter / intial;
	}
	else {		//it's data
		if (296939 <= run_number && run_number <= 300345) { //2016 periodA
			MuonSelection = (lep_0_id_medium == 1 && ((HLT_mu24_imedium == 1 && lep_0_pt > 25.2 && muTrigMatch_0_HLT_mu24_imedium == 1)));
		}
		else if (300345 <= run_number && run_number <= 302919) { //2016 periodB-D3
			MuonSelection = (lep_0_id_medium == 1 && ((HLT_mu24_imedium == 1 && lep_0_pt > 25.2 && muTrigMatch_0_HLT_mu24_imedium == 1)));
		}
		else if (302919 <= run_number) { //2016 periodD4-
			MuonSelection = (lep_0_id_medium == 1 && ((HLT_mu26_ivarmedium == 1 && lep_0_pt > 27.3 && muTrigMatch_0_HLT_mu26_ivarmedium == 1)));
		}
	}
	******************************************2016 Trigger seeting****************************/

	anaClass->weight = weight;

	double DPhi = fabs(tau_0_phi - lep_0_phi);
	if (DPhi > M_PI) DPhi = 2 * M_PI - DPhi;

	anaClass->dEta = fabs(tau_0_eta - lep_0_eta); 
	anaClass->TauIDL = (tau_0_jet_bdt_loose == 1);
	anaClass->TauIDM = (tau_0_jet_bdt_medium == 1);
	anaClass->TauIDT = (tau_0_jet_bdt_tight == 1);
	bool TauSelection = (tau_0_jet_bdt_score_trans > 0.005 && tau_0_pt > 20 && abs(tau_0_q) == 1 && (tau_0_n_charged_tracks == 1 || tau_0_n_charged_tracks == 3) && (fabs(tau_0_eta)<0.8 || (0.8<fabs(tau_0_eta) && fabs(tau_0_eta)<1.37) || (1.52<fabs(tau_0_eta) && fabs(tau_0_eta)<2.47)));
	bool BaseSelection = (n_pvx >= 1 && n_muons == 1 && n_electrons == 0 && n_taus>0 && MuonSelection && TauSelection && n_bjets == 0);

	anaClass->TauSelection = TauSelection;
	anaClass->BaseSelection = BaseSelection;
	anaClass->MuonSelection = MuonSelection;
	anaClass->TopCR = (n_muons == 1 && n_electrons == 0 && n_taus>0 && n_bjets>0 && MuonSelection);
	anaClass->SR = (BaseSelection 
					&& 45<lephad_vis_mass && lephad_vis_mass<85 
					&& lep_0_iso_ptvarcone30 / lep_0_pt < 0.01 * 1000 && lep_0_iso_topoetcone20 / lep_0_pt < 0.05 * 1000 
					&& lephad_mt_lep0_met < 50 
					&& lephad_met_sum_cos_dphi > -0.15 
					&& lep_0_pt<40 
					&& DPhi>2.4);
	anaClass->MCR = (BaseSelection 
					&& (lep_0_iso_ptvarcone30 / lep_0_pt > 0.01 * 1000 && lep_0_iso_topoetcone20 / lep_0_pt > 0.05 * 1000) 
					&& lephad_mt_lep0_met < 50 
					&& lephad_met_sum_cos_dphi > -0.15 
					&& 45<lephad_vis_mass && lephad_vis_mass<85 
					&& lep_0_pt<40 
					&& DPhi>2.4);
	anaClass->WCR = (BaseSelection 
					&& lep_0_iso_ptvarcone30 / lep_0_pt < 0.01 * 1000 && lep_0_iso_topoetcone20 / lep_0_pt < 0.05 * 1000 
					&& lephad_mt_lep0_met > 60 
					&& met_reco_et > 30 
					&& lephad_met_sum_cos_dphi < 0.0);
	anaClass->RecoDecayMode = tau_0_sub_PanTau_DecayMode;
	anaClass->TruDecayMode = tau_0_truth_decay_mode;

	anaClass->tauAlltracked_pt = tau_0_allTrk_pt;
	anaClass->tauTruth_pt = tau_0_truth_pt;
	anaClass->tauReco_pt = tau_0_pt;
	anaClass->nTrack = tau_0_n_charged_tracks + tau_0_n_isolation_tracks + tau_0_n_conversion_tracks;
	anaClass->isOS = (tau_0_q * lep_0_q <0);
	anaClass->one_pron = (tau_0_n_charged_tracks == 1);
	anaClass->three_pron = (tau_0_n_charged_tracks == 3);
	anaClass->isTrueTau = fabs(tau_0_truth_isHadTau) == 1;
	anaClass->mu = n_avg_int_cor;
	anaClass->VisMass = lephad_vis_mass;
	anaClass->MuReco_pt = lep_0_pt;
	anaClass->TauReco_eta = tau_0_eta;
	anaClass->MuReco_eta = lep_0_eta;
	anaClass->MET = met_reco_et;
	anaClass->isLepFake = (tau_0_truth_isEle || tau_0_truth_isMuon);

}

