#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>
#include <TStyle.h>
#include <TCanvas.h>
#include "tau.h"

#ifndef tauTree_h
#define tauTree_h

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.

class tauTree {
public :
	tau           *anaClass;
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   tauTree(TTree *tree=0);
   virtual ~tauTree();
   void setAnaClass(tau* anaClass) {
	   this->anaClass = anaClass;
   }
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual void     updateCuts() = 0;
   virtual double   GetRunNgen(UInt_t run) = 0;
   virtual double   GetLumi() = 0;
   virtual double   GetKFactor(UInt_t run);
   virtual double   GetXsec(UInt_t run);
   virtual double   GetFilterEff(UInt_t run);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

tauTree::tauTree(TTree *tree) : fChain(0) 
{
   Init(tree);
}

tauTree::~tauTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t tauTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t tauTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void tauTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   Notify();
}

Bool_t tauTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void tauTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t tauTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

void tauTree::Loop()
{
	if (fChain == 0) return;

	Long64_t nentries = fChain->GetEntries();

	Long64_t nbytes = 0, nb = 0;

	gROOT->SetStyle("Plain");
	gStyle->SetOptFit(1111);
	gStyle->SetOptStat(1111);

	anaClass->registerRegions();
	anaClass->registerExtraRegions();
	for (Long64_t jentry = 0; jentry<nentries;jentry++) {
		Long64_t ientry = LoadTree(jentry);
		if (ientry < 0) break;
		nb = fChain->GetEntry(jentry);   nbytes += nb;
		if (jentry % 10000 == 0) std::cout << "processing... event " << jentry << " of "<< nentries << " events" <<std::endl;

		updateCuts();
		anaClass->FillRegions();
	}

	anaClass->WriteHists();
}

double tauTree::GetXsec(UInt_t run){
	if (run == 361100 ) 	 return  11306. ;     
	else if (run == 361101 ) return  11306. ;
	else if (run == 361102 ) return  11306. ;
	else if (run == 361103 ) return  8282.7;
	else if (run == 361104 ) return  8282.8;
	else if (run == 361105 ) return  8282.6;
	else if (run == 361106 ) return  1901.2;
	else if (run == 361107 ) return  1901.1;
	else if (run == 361108 ) return  1901.2;
	else if (run == 410000 ) return   696.11;
	else if (run == 410501 ) return   730.170;
	else return 0;
}

double tauTree::GetKFactor(UInt_t run){
	if (run == 361100 ) 	 return  1.01723999646 ;     
	else if (run == 361101 ) return  1.01723999646 ;
	else if (run == 361102 ) return  1.01723999646 ;
	else if (run == 361103 ) return  1.03577349174;
	else if (run == 361104 ) return  1.03576098662;
	else if (run == 361105 ) return  1.03578599715;
	else if (run == 361106 ) return  1.02600047339;
	else if (run == 361107 ) return  1.02605444217;
	else if (run == 361108 ) return  1.02605444217;
	else if (run == 410000 ) return   1.1949;
	else if (run == 410501 ) return   1.13913198296;
	else return 0;
}

double tauTree::GetFilterEff(UInt_t run){
	if (run == 361100 ) 	 return  1. ;     
	else if (run == 361101 ) return  1. ;
	else if (run == 361102 ) return  1. ;
	else if (run == 361103 ) return   1.;
	else if (run == 361104 ) return   1.;
	else if (run == 361105 ) return   1.;
	else if (run == 361106 ) return   1.;
	else if (run == 361107 ) return   1.;
	else if (run == 361108 ) return   1.;
	else if (run == 410000 ) return   0.543;
	else if (run == 410501 ) return   0.54383;
	else return 0;
}
#endif
