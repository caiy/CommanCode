#include <string>
#include <vector>
#include <TFile.h>

#include "tau.h"

void tau::registerRegions(){

	//loop for different Reco decaymode
	for(int i = 0; i < 5; ++i){ 
		std::string RecoDecayTag;
		int Nbins(0), NbinsNT(12), NbinsDE(10);
		double LowValue(0),UpValue(0);
		
		//bins for NTrack
		double LowValueNT(0),UpValueNT(12);
		double LowValueDE(0), UpValueDE(2.5);
		//bins for upsilon
		if (0 == i) {
			RecoDecayTag = "_Reco1";
			Nbins = 15;
			LowValue = 0;
			UpValue = 1.5;
			UpValueNT = 5; 
			NbinsNT = 5; 
		} else if (1 == i){
			RecoDecayTag = "_Reco2";
			Nbins = 15;
			LowValue = -1;
			UpValue = 1.5;
		} else if (2 == i){
			RecoDecayTag = "_Reco3";
			Nbins = 15;
			LowValue = -1;
			UpValue = 0.5;
		}else if (3 == i){
			RecoDecayTag = "_Reco4";
			Nbins = 10;
			LowValue = 0.5;
			UpValue = 1.5;
		}else if (4 == i){
			RecoDecayTag = "_Reco5";
			Nbins = 15;
			LowValue = 0;
			UpValue = 1.5;
		}

		HistTemplate upsilonHistTem(upsilon, Nbins, LowValue, UpValue);
		HistTemplate NTrackHistTem(NTrack, NbinsNT, LowValueNT, UpValueNT);
		HistTemplate deltaEtaHistTem(deltaEta, NbinsDE, LowValueDE, UpValueDE);

		/**********************************************************************************************************************************
		*****Note:1. The Region is defined by a lambda function so if the cut condition is changed, the cut status of each region will
		*****        change immediately! By applying this trick we can seperate the cut defination with a certain tree. Isn't it good?
		*****     2. If you want to modify regions defined here, feel free to do it. For a specific sample, if you want to define a 
		*****        specific region for it, *DON'T* *DEFINE* *REGIONS* *HERE*. You can write a class extending this class and override 
		*****        the 'registerExtraRegions()' method.(It is writen just for this porpose! There are some expamles in tauSubClass.h).  
		***** 	  3. For each region, you can add multiple histgrams. Not just one hist if you need!
		***********************************************************************************************************************************/

		Region* regionSR = addRegion([&,i] {return (SR && tauIDcut() && RecoDecayMode == i);});
		regionSR->AddHist(upsilonHistTem,"Upsilon_SR" + RecoDecayTag);
		regionSR->AddHist(NTrackHistTem,"NTrack_SR" + RecoDecayTag);
		regionSR->AddHist(deltaEtaHistTem, "DeltaEta_SR" + RecoDecayTag);
		
		Region* regionSR_1 = addRegion([&,i] {return (SR && tauIDcut() && one_pron && RecoDecayMode == i);});
		regionSR_1->AddHist(upsilonHistTem,"Upsilon_SR_1" + RecoDecayTag);
		regionSR_1->AddHist(NTrackHistTem,"NTrack_SR_1" + RecoDecayTag);
		regionSR_1->AddHist(deltaEtaHistTem, "DeltaEta_SR_1" + RecoDecayTag);

		Region* regionSR_3 = addRegion([&,i] {return (SR && tauIDcut() && three_pron && RecoDecayMode == i);});
		regionSR_3->AddHist(upsilonHistTem,"Upsilon_SR_3" + RecoDecayTag);
		regionSR_3->AddHist(NTrackHistTem,"NTrack_SR_3" + RecoDecayTag);
		regionSR_3->AddHist(deltaEtaHistTem, "DeltaEta_SR_3" + RecoDecayTag);

		Region* regionSROS = addRegion([&,i] {return SR && tauIDcut() && isOS && RecoDecayMode == i;});
		regionSROS->AddHist(upsilonHistTem,"Upsilon_SR_OS" + RecoDecayTag);
		regionSROS->AddHist(NTrackHistTem,"NTrack_SR_OS" + RecoDecayTag);
		regionSROS->AddHist(deltaEtaHistTem, "DeltaEta_SR_OS" + RecoDecayTag);

		Region* regionSROS_1 = addRegion([&,i] {return (SR && tauIDcut() && isOS && one_pron && RecoDecayMode == i);});
		regionSROS_1->AddHist(upsilonHistTem, "Upsilon_SR_OS_1" + RecoDecayTag);
		regionSROS_1->AddHist(NTrackHistTem, "NTrack_SR_OS_1" + RecoDecayTag);
		regionSROS_1->AddHist(deltaEtaHistTem, "DeltaEta_SR_OS_1" + RecoDecayTag);

		Region* regionSROS_3 = addRegion([&,i] {return (SR && tauIDcut() && isOS && three_pron && RecoDecayMode == i);});
		regionSROS_3->AddHist(upsilonHistTem, "Upsilon_SR_OS_3" + RecoDecayTag);
		regionSROS_3->AddHist(NTrackHistTem, "NTrack_SR_OS_3" + RecoDecayTag);
		regionSROS_3->AddHist(deltaEtaHistTem, "DeltaEta_SR_OS_3" + RecoDecayTag);

		Region* regionSRSS = addRegion([&,i] {return (SR && tauIDcut() && !isOS && RecoDecayMode == i);});
		regionSRSS->AddHist(upsilonHistTem, "Upsilon_SR_SS" + RecoDecayTag);
		regionSRSS->AddHist(NTrackHistTem, "NTrack_SR_SS" + RecoDecayTag);
		regionSRSS->AddHist(deltaEtaHistTem, "DeltaEta_SR_SS" + RecoDecayTag);

		Region* regionSRSS_1 = addRegion([&,i] {return (SR && tauIDcut() && !isOS && one_pron && RecoDecayMode == i);});
		regionSRSS_1->AddHist(upsilonHistTem, "Upsilon_SR_SS_1" + RecoDecayTag);
		regionSRSS_1->AddHist(NTrackHistTem, "NTrack_SR_SS_1" + RecoDecayTag);
		regionSRSS_1->AddHist(deltaEtaHistTem, "DeltaEta_SR_SS_1" + RecoDecayTag);

		Region* regionSRSS_3 = addRegion([&,i] {return (SR && tauIDcut() && !isOS && three_pron && RecoDecayMode == i);});
		regionSRSS_3->AddHist(upsilonHistTem, "Upsilon_SR_SS_3" + RecoDecayTag);
		regionSRSS_3->AddHist(NTrackHistTem, "NTrack_SR_SS_3" + RecoDecayTag);
		regionSRSS_3->AddHist(deltaEtaHistTem, "DeltaEta_SR_SS_3" + RecoDecayTag);

		Region* region_lep_true = addRegion([&,i] {return (SR && tauIDcut() && isOS && isTrueTau && RecoDecayMode == i);});
		region_lep_true->AddHist(upsilonHistTem, "Upsilon_lep_true" + RecoDecayTag);
		region_lep_true->AddHist(NTrackHistTem, "NTrack_lep_true" + RecoDecayTag);
		region_lep_true->AddHist(deltaEtaHistTem, "DeltaEta_lep_true" + RecoDecayTag);

		Region* region_lep_true_1 = addRegion([&,i] {return (SR && tauIDcut() && isOS && isTrueTau && one_pron && RecoDecayMode == i);});
		region_lep_true_1->AddHist(upsilonHistTem, "Upsilon_lep_true_1" + RecoDecayTag);
		region_lep_true_1->AddHist(NTrackHistTem, "NTrack_lep_true_1" + RecoDecayTag);
		region_lep_true_1->AddHist(deltaEtaHistTem, "DeltaEta_lep_true_1" + RecoDecayTag);

		Region* region_lep_true_3 = addRegion([&,i] {return (SR && tauIDcut() && isOS && isTrueTau && three_pron && RecoDecayMode == i);});
		region_lep_true_3->AddHist(upsilonHistTem, "Upsilon_lep_true_3" + RecoDecayTag);
		region_lep_true_3->AddHist(NTrackHistTem, "NTrack_lep_true_3" + RecoDecayTag);
		region_lep_true_3->AddHist(deltaEtaHistTem, "DeltaEta_lep_true_3" + RecoDecayTag);

		Region* region_lep_fake = addRegion([&,i] {return (SR && tauIDcut() && isOS && isLepFake && RecoDecayMode == i);});
		region_lep_fake->AddHist(upsilonHistTem, "Upsilon_lep_fake" + RecoDecayTag);
		region_lep_fake->AddHist(NTrackHistTem, "NTrack_lep_fake" + RecoDecayTag);
		region_lep_fake->AddHist(deltaEtaHistTem, "DeltaEta_lep_fake" + RecoDecayTag);

		Region* region_lep_fake_1 = addRegion([&,i] {return (SR && tauIDcut() && isOS && isLepFake && one_pron && RecoDecayMode == i);});
		region_lep_fake_1->AddHist(upsilonHistTem, "Upsilon_lep_fake_1" + RecoDecayTag);
		region_lep_fake_1->AddHist(NTrackHistTem, "NTrack_lep_fake_1" + RecoDecayTag);
		region_lep_fake_1->AddHist(deltaEtaHistTem, "DeltaEta_lep_fake_1" + RecoDecayTag);

		Region* region_lep_fake_3 = addRegion([&,i] {return (SR && tauIDcut() && isOS && isLepFake && three_pron && RecoDecayMode == i);});
		region_lep_fake_3->AddHist(upsilonHistTem, "Upsilon_lep_fake_3" + RecoDecayTag);
		region_lep_fake_3->AddHist(NTrackHistTem, "NTrack_lep_fake_3" + RecoDecayTag);
		region_lep_fake_3->AddHist(deltaEtaHistTem, "DeltaEta_lep_fake_3" + RecoDecayTag);

		Region* regionWCROS = addRegion([&,i] {return (WCR && tauIDcut() && isOS && RecoDecayMode == i);});
		regionWCROS->AddHist(upsilonHistTem, "Upsilon_WCR_OS" + RecoDecayTag);
		regionWCROS->AddHist(NTrackHistTem, "NTrack_WCR_OS" + RecoDecayTag);
		regionWCROS->AddHist(deltaEtaHistTem, "DeltaEta_WCR_OS" + RecoDecayTag);

		Region* regionWCRSS = addRegion([&,i] {return (WCR && tauIDcut() && !isOS && RecoDecayMode == i);});
		regionWCRSS->AddHist(upsilonHistTem, "Upsilon_WCR_SS" + RecoDecayTag);
		regionWCRSS->AddHist(NTrackHistTem, "NTrack_WCR_SS" + RecoDecayTag);
		regionWCRSS->AddHist(deltaEtaHistTem, "DeltaEta_WCR_SS" + RecoDecayTag);

		Region* regionMCROS = addRegion([&,i] {return (MCR && tauIDcut() && isOS && RecoDecayMode == i);});
		regionMCROS->AddHist(upsilonHistTem, "Upsilon_MCR_OS" + RecoDecayTag);
		regionMCROS->AddHist(NTrackHistTem, "NTrack_MCR_OS" + RecoDecayTag);
		regionMCROS->AddHist(deltaEtaHistTem, "DeltaEta_MCR_OS" + RecoDecayTag);

		Region* regionMCRSS = addRegion([&,i] {return (MCR && tauIDcut() && !isOS && RecoDecayMode == i);});
		regionMCRSS->AddHist(upsilonHistTem, "Upsilon_MCR_SS" + RecoDecayTag);
		regionMCRSS->AddHist(NTrackHistTem, "NTrack_MCR_SS" + RecoDecayTag);
		regionMCRSS->AddHist(deltaEtaHistTem, "DeltaEta_MCR_SS" + RecoDecayTag);
	}
}
