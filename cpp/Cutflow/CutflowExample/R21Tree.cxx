#define R21Tree_cxx
#include "R21Tree.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <sstream>
#include <math.h>
#include <regex>
#include <vector>
#include <fstream>
#include <TSystem.h>
#include <TMath.h>
#include "Cutflow.h"
#include "metadata.h"
#include <iomanip>

using namespace std;

std::string file;
std::string datamode;
int decaymode;
double sigEvents = 0;
double Gmode = 0;

void R21Tree::Loop()
{
//   In a ROOT session, you can do:
//      root> .L selector.C
//      root> selector t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;
   
   Long64_t nentries = fChain->GetEntries();

   Long64_t nbytes = 0, nb = 0;
   gROOT->SetStyle("Plain");

   double DPhi(0),upsilon(0);
   mc_channel_number = 0;
   bool muSelection(true);

   Cutflow* mCutflow = new Cutflow(file);

   mCutflow->registerCut("baseline",[&] {return true;}, "tauID_baseline",4,0,4,[&] {return (tau_0_jet_bdt_medium);});
   mCutflow->registerCut("TauCut_ID",[&] {return (tau_0_jet_bdt_medium ==1);}, "tauID",4,0,4,[&] {return (tau_0_jet_bdt_medium);});
   mCutflow->registerCut("decaymode",[&] {return (tau_0_sub_PanTau_DecayMode >=0 && tau_0_sub_PanTau_DecayMode < 5);},"decaymode",10,0,10,[&] {return (tau_0_sub_PanTau_DecayMode);});
   mCutflow->registerCut("upsilon",[&] {return (upsilon < 2 && upsilon > -1);},"upsilon",10,-2,8,[&] {return (upsilon);});
   mCutflow->registerCut("decaymode = "+to_string(decaymode),[&] {return (tau_0_sub_PanTau_DecayMode ==(decaymode - 1));},"upsilon_Reco"+to_string(decaymode),20,-1,2,[&] {return (upsilon);});
   mCutflow->registerCut("tau_0_pt > 20",[&] {return tau_0_pt > 20;},"tau_pt",100,0,1000,[&] {return (tau_0_pt);});
   mCutflow->registerCut("abs(tau_0_q) == 1",[&] {return fabs(tau_0_q) == 1;},"tau_0_q",100,-4,4,[&] {return (tau_0_q);});
   mCutflow->registerCut("charged_tracks",[&] {return (tau_0_n_charged_tracks == 1 || tau_0_n_charged_tracks == 3);},"charged_tracks",10,0,10,[&] {return (tau_0_n_charged_tracks);});
   mCutflow->registerCut("tau_0_eta",[&] {return (fabs(tau_0_eta)<2.47);});
   mCutflow->registerCut("n_pvx >= 1",[&] {return (n_pvx >= 1);},"n_pvx",4,0,4,[&] {return (n_pvx);});
   mCutflow->registerCut("lep&tau cut",[&] {return (n_electrons == 0 && n_muons == 1 && n_taus>0);},"n_electrons",4,0,4,[&] {return n_electrons;});
   mCutflow->registerCut("b-veto",[&] {return (n_bjets == 0);},"n_bjets",4,0,4,[&] {return n_bjets;});
   mCutflow->registerCut("lephad_vis_mass",[&] {return (45<lephad_vis_mass && lephad_vis_mass<85);},"lephad_vis_mass",20,0,200,[&] {return lephad_vis_mass;});
   mCutflow->registerCut("lephad_mt_lep0_met < 50",[&] {return (lephad_mt_lep0_met < 50);},"lephad_mt_lep0_met",20,0,200,[&] {return lephad_mt_lep0_met;});
   mCutflow->registerCut("lephad_met_sum_cos_dphi > -0.15",[&] {return (lephad_met_sum_cos_dphi > -0.15);},"lephad_met_sum_cos_dphi",10,-4,4,[&] {return lephad_met_sum_cos_dphi;});
   mCutflow->registerCut("lep_0_pt < 40",[&] {return (lep_0_pt < 40);},"lep_0_pt",20,0,200,[&] {return lep_0_pt;});
   auto targetCut = mCutflow->registerCut("lep_0_iso_ptvarcone30 / lep_0_pt < 10",[&] {return (lep_0_iso_ptvarcone30 / lep_0_pt <10);},"lep_0_iso_ptvarcone30_divide_lep_0_pt",10,0,20,[&] {return lep_0_iso_ptvarcone30 / lep_0_pt;});
   targetCut->addHist("lep_0_iso_ptvarcone30",10,0,200,[&] {return lep_0_iso_ptvarcone30;});
   mCutflow->registerCut("DPhi > 2.4",[&] {return (DPhi > 2.4);},"DPhi",10,0,10,[&] {return DPhi;});
   mCutflow->registerCut("lep_0_iso_topoetcone20 / lep_0_pt < 50",[&] {return (lep_0_iso_topoetcone20 / lep_0_pt < 50);},"lep_0_iso_topoetcone20_divide_lep_0_pt",20,0,200,[&] {return lep_0_iso_topoetcone20 / lep_0_pt;});
   mCutflow->registerCut("lep_0_id_medium",[&] {return (lep_0_id_medium == 1);},"lep_0_id_medium",5,0,5,[&] {return lep_0_id_medium;});
   mCutflow->registerCut("mu selection",[&] {return (muSelection);});

   double weight = 1;
   mCutflow->setWeight([&] {return (weight);});
	//main loop
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (0 == jentry%100000)
            cout << jentry << " entry of " << nentries << "entries" << endl;

	  if (mc_channel_number != 0) {
		muSelection = (HLT_mu26_ivarmedium == 1 && lep_0_pt > 27.3 && muTrigMatch_0_HLT_mu26_ivarmedium == 1);
		double TriggerSF = lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
		//calculate weight for MC samples
		double intial = GetRunNgenR21(mc_channel_number);
		double  lumi = 43013;
		double Xsec = GetXsecR21(mc_channel_number);
		double k = GetKFactorR21(mc_channel_number);
		double filter = GetFilterEffR21(mc_channel_number);
		//    double factor = lep_0_NOMINAL_HLT_mu20_iloose_L1MU15_MU_TRIG_QUAL_MEDIUM_MU_TRIG_ISO_LOOSE*lep_0_NOMINAL_effSF_IsoLoose;
		double factor = lep_0_NOMINAL_MuEffSF_Reco_QualMedium * TriggerSF * lep_0_NOMINAL_MuEffSF_TTVA * lep_0_NOMINAL_MuEffSF_IsoGradient;
		weight = weight_total * Xsec*lumi*k*factor*filter / intial;
	}	else {
		muSelection = (HLT_mu26_ivarmedium == 1 && lep_0_pt > 27.3 && muTrigMatch_0_HLT_mu26_ivarmedium == 1);
	}

	  DPhi = fabs(tau_0_phi - lep_0_phi);
	  if (DPhi > M_PI) DPhi = 2 * M_PI - DPhi;
	  upsilon = (2 * tau_0_allTrk_pt - tau_0_pt) / tau_0_pt; 

	  //cut flow and N-1 distribution fill
	  mCutflow->startCut();

   }//end of loop

    mCutflow->PrintOut();
	delete mCutflow;
}


int main(int argc, char* argv[])
{
    cout<<"*******************************Start**********************************"<<endl;

    //argv[1]表示datamode，datamode=background表示不需要分析文件名信息
    //datamode=signal表示需分析文件名信息
    //datamode=data表示使用针对data的cut
    //argv[2]表示cutmode，当其为OS时，为OScut，SS时，为SScut
    //argv[3]表示filepath
    datamode = argv[1];
    decaymode = atoi(argv[2]);
    std::string pathDATA = argv[3];

    if("background" == datamode || "signal" == datamode)
    {
        //因为argv[0]是程序本身,argv[1],[2]是条件判断,argv[3]是path,所以这里要减4,之后要加4
        for (int i =0; i<argc-4; i++)
        {
            std::string filename = argv[i+4];
            getfileinfo(i, pathDATA, filename);
        }
    }
    else
    {
        std::cout << "Wrong data mode! You should choose input mode among " << 
                "'data', 'background' or 'signal'"<< endl;
        return -1;
    }
    std::cout << "******************E N D****************************************" << std::endl;

    return 0;
}

//正则匹配，过滤出root文件
void getfileinfo(int i, string pathDATA, const string& ip)
{
    regex pattern;
    if("background" == datamode)
        pattern = "(.*\\.root)";
    else if("signal" == datamode)
        pattern = "(.*_MSL_(\\d*)_N1_(\\d*).*\\.root)";       //C1N2_Stau_462p5_162p5.root ==> Gmode=462.5 , lspmode=162.5
	std::vector<std::string> result = getinfo(pattern,ip);
    if(result.size() != 0)
    {       
        //datamode==signal时采用额外的匹配方法以记下其Gmode和lspmode
        if("signal" ==datamode)
        {   //result[1]:filename, result[2]:Gmode, result[3]:lspmode
            Gmode = to_numeral(result[2]);
            double lspmode = to_numeral(result[3]);

			std::string readfile = "signalinfo.txt";
			ifstream infile(readfile);
			if (!infile)
			{    
				cout << "No such file!" << endl;
			}
			string name;
			double nEvents;
			while (infile >> name >> nEvents){
					if(name == result[1]){
						sigEvents = nEvents;
						break;
					}
			}

            cout<<"Start cutting of "<<result[1]<<" which Gmode is "<<Gmode<<" , "
                    <<"lspmode is "<<lspmode <<", Event number is "<< sigEvents <<endl;
            ofstream datacout("rootdata1.txt",ios::app);
            datacout<<"Gm:"<<Gmode<<",    "<<"lsp:"<<lspmode<<","<<endl;
            datacout.close();
        }
        maincut(i, pathDATA, result[1]);
    }
}

std::vector<std::string> getinfo(regex pattern, const string& ip)
{
	std::smatch result;
	std::vector<std::string> ret;
    bool valid = regex_match(ip,result,pattern); 
    cout<<ip<<"  "<<(valid?"valid":"invalid")<<endl;
    if(valid)
    {       
			for(unsigned int i = 0; i < result.size(); i++){
				ret.push_back(result[i]);
			}
    }
	return ret;
}

void maincut(int i, string path, string filename)
{
    std::string chainname = "NOMINAL";
	file = filename;
    TChain *chain = new TChain(chainname.c_str());
    std::string rootfile = path + filename;
    chain->Add(rootfile.c_str());

    const char* newfile = filename.c_str();
    TFile* f = new TFile(newfile, "recreate");
    R21Tree* analysis = new R21Tree(chain);

    std::cout << "**********Start cutting " << filename << "*********" << std::endl;
    analysis->Loop();
    std::cout << "**********End of cutting " << filename << "********" <<std::endl;

	f->Write();
    f->Close();

    delete analysis;
    delete chain;
    delete f;
}

//Binary Search
double BinarySearch(vector<pair<double,double>> weight,double key){
	int right=weight.size()-1;
	int left=0;
	while(right - left != 1){
		int mid=(left+right)/2;
		if(key<=weight.at(mid).first){
			right=mid;
		}else{
			left=mid;
		}
	}
	return weight.at(right).second;
}

//string to double
double to_numeral(const string& src)
{
    double dest;
    stringstream ss(src);
    ss >> dest;
    return dest;
}
