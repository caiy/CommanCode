#pragma once
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri May 24 22:22:27 2019 by ROOT version 6.12/06
// from TTree syst/syst
// found on file: ../mergesyst/run/syst.root
//////////////////////////////////////////////////////////

#ifndef selector_h
#define selector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

using namespace std;

// Header file for the classes stored in the TTree if any.

class selector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           mcChannelNumber;
   Double_t        SR_h_Low_qcdw;
   Double_t        SR_h_Low_qcup;
   Double_t        SR_h_Low_scdw;
   Double_t        SR_h_Low_scup;
   Double_t        SR_h_Med_qcdw;
   Double_t        SR_h_Med_qcup;
   Double_t        SR_h_Med_scdw;
   Double_t        SR_h_Med_scup;
   Double_t        SR_h_High_qcdw;
   Double_t        SR_h_High_qcup;
   Double_t        SR_h_High_scdw;
   Double_t        SR_h_High_scup;
   Double_t        VR_off_Low_qcdw;
   Double_t        VR_off_Low_qcup;
   Double_t        VR_off_Low_scdw;
   Double_t        VR_off_Low_scup;
   Double_t        VR_off_Med_qcdw;
   Double_t        VR_off_Med_qcup;
   Double_t        VR_off_Med_scdw;
   Double_t        VR_off_Med_scup;
   Double_t        VR_off_High_qcdw;
   Double_t        VR_off_High_qcup;
   Double_t        VR_off_High_scdw;
   Double_t        VR_off_High_scup;
   Double_t        VR_on_Low_qcdw;
   Double_t        VR_on_Low_qcup;
   Double_t        VR_on_Low_scdw;
   Double_t        VR_on_Low_scup;
   Double_t        VR_on_Med_qcdw;
   Double_t        VR_on_Med_qcup;
   Double_t        VR_on_Med_scdw;
   Double_t        VR_on_Med_scup;
   Double_t        VR_on_High_qcdw;
   Double_t        VR_on_High_qcup;
   Double_t        VR_on_High_scdw;
   Double_t        VR_on_High_scup;
   Double_t        VR_off_Low_radw;
   Double_t        VR_off_Med_radw;
   Double_t        VR_off_High_radw;
   Double_t        SR_h_Low_radw;
   Double_t        SR_h_Med_radw;
   Double_t        SR_h_High_radw;
   Double_t        VR_on_Low_radw;
   Double_t        VR_on_Med_radw;
   Double_t        VR_on_High_radw;
   Double_t        VR_off_Low_raup;
   Double_t        VR_off_Med_raup;
   Double_t        VR_off_High_raup;
   Double_t        SR_h_Low_raup;
   Double_t        SR_h_Med_raup;
   Double_t        SR_h_High_raup;
   Double_t        VR_on_Low_raup;
   Double_t        VR_on_Med_raup;
   Double_t        VR_on_High_raup;

   // List of branches
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_SR_h_Low_qcdw;   //!
   TBranch        *b_SR_h_Low_qcup;   //!
   TBranch        *b_SR_h_Low_scdw;   //!
   TBranch        *b_SR_h_Low_scup;   //!
   TBranch        *b_SR_h_Med_qcdw;   //!
   TBranch        *b_SR_h_Med_qcup;   //!
   TBranch        *b_SR_h_Med_scdw;   //!
   TBranch        *b_SR_h_Med_scup;   //!
   TBranch        *b_SR_h_High_qcdw;   //!
   TBranch        *b_SR_h_High_qcup;   //!
   TBranch        *b_SR_h_High_scdw;   //!
   TBranch        *b_SR_h_High_scup;   //!
   TBranch        *b_VR_off_Low_qcdw;   //!
   TBranch        *b_VR_off_Low_qcup;   //!
   TBranch        *b_VR_off_Low_scdw;   //!
   TBranch        *b_VR_off_Low_scup;   //!
   TBranch        *b_VR_off_Med_qcdw;   //!
   TBranch        *b_VR_off_Med_qcup;   //!
   TBranch        *b_VR_off_Med_scdw;   //!
   TBranch        *b_VR_off_Med_scup;   //!
   TBranch        *b_VR_off_High_qcdw;   //!
   TBranch        *b_VR_off_High_qcup;   //!
   TBranch        *b_VR_off_High_scdw;   //!
   TBranch        *b_VR_off_High_scup;   //!
   TBranch        *b_VR_on_Low_qcdw;   //!
   TBranch        *b_VR_on_Low_qcup;   //!
   TBranch        *b_VR_on_Low_scdw;   //!
   TBranch        *b_VR_on_Low_scup;   //!
   TBranch        *b_VR_on_Med_qcdw;   //!
   TBranch        *b_VR_on_Med_qcup;   //!
   TBranch        *b_VR_on_Med_scdw;   //!
   TBranch        *b_VR_on_Med_scup;   //!
   TBranch        *b_VR_on_High_qcdw;   //!
   TBranch        *b_VR_on_High_qcup;   //!
   TBranch        *b_VR_on_High_scdw;   //!
   TBranch        *b_VR_on_High_scup;   //!
   TBranch        *b_VR_off_Low_radw;   //!
   TBranch        *b_VR_off_Med_radw;   //!
   TBranch        *b_VR_off_High_radw;   //!
   TBranch        *b_SR_h_Low_radw;   //!
   TBranch        *b_SR_h_Med_radw;   //!
   TBranch        *b_SR_h_High_radw;   //!
   TBranch        *b_VR_on_Low_radw;   //!
   TBranch        *b_VR_on_Med_radw;   //!
   TBranch        *b_VR_on_High_radw;   //!
   TBranch        *b_VR_off_Low_raup;   //!
   TBranch        *b_VR_off_Med_raup;   //!
   TBranch        *b_VR_off_High_raup;   //!
   TBranch        *b_SR_h_Low_raup;   //!
   TBranch        *b_SR_h_Med_raup;   //!
   TBranch        *b_SR_h_High_raup;   //!
   TBranch        *b_VR_on_Low_raup;   //!
   TBranch        *b_VR_on_Med_raup;   //!
   TBranch        *b_VR_on_High_raup;   //!

   selector(TTree *tree=0);
   virtual ~selector();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef selector_cxx
selector::selector(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../mergesyst/run/syst.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../mergesyst/run/syst.root");
      }
      f->GetObject("syst",tree);

   }
   Init(tree);
}

selector::~selector()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t selector::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t selector::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void selector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("SR_h_Low_qcdw", &SR_h_Low_qcdw, &b_SR_h_Low_qcdw);
   fChain->SetBranchAddress("SR_h_Low_qcup", &SR_h_Low_qcup, &b_SR_h_Low_qcup);
   fChain->SetBranchAddress("SR_h_Low_scdw", &SR_h_Low_scdw, &b_SR_h_Low_scdw);
   fChain->SetBranchAddress("SR_h_Low_scup", &SR_h_Low_scup, &b_SR_h_Low_scup);
   fChain->SetBranchAddress("SR_h_Med_qcdw", &SR_h_Med_qcdw, &b_SR_h_Med_qcdw);
   fChain->SetBranchAddress("SR_h_Med_qcup", &SR_h_Med_qcup, &b_SR_h_Med_qcup);
   fChain->SetBranchAddress("SR_h_Med_scdw", &SR_h_Med_scdw, &b_SR_h_Med_scdw);
   fChain->SetBranchAddress("SR_h_Med_scup", &SR_h_Med_scup, &b_SR_h_Med_scup);
   fChain->SetBranchAddress("SR_h_High_qcdw", &SR_h_High_qcdw, &b_SR_h_High_qcdw);
   fChain->SetBranchAddress("SR_h_High_qcup", &SR_h_High_qcup, &b_SR_h_High_qcup);
   fChain->SetBranchAddress("SR_h_High_scdw", &SR_h_High_scdw, &b_SR_h_High_scdw);
   fChain->SetBranchAddress("SR_h_High_scup", &SR_h_High_scup, &b_SR_h_High_scup);
   fChain->SetBranchAddress("VR_off_Low_qcdw", &VR_off_Low_qcdw, &b_VR_off_Low_qcdw);
   fChain->SetBranchAddress("VR_off_Low_qcup", &VR_off_Low_qcup, &b_VR_off_Low_qcup);
   fChain->SetBranchAddress("VR_off_Low_scdw", &VR_off_Low_scdw, &b_VR_off_Low_scdw);
   fChain->SetBranchAddress("VR_off_Low_scup", &VR_off_Low_scup, &b_VR_off_Low_scup);
   fChain->SetBranchAddress("VR_off_Med_qcdw", &VR_off_Med_qcdw, &b_VR_off_Med_qcdw);
   fChain->SetBranchAddress("VR_off_Med_qcup", &VR_off_Med_qcup, &b_VR_off_Med_qcup);
   fChain->SetBranchAddress("VR_off_Med_scdw", &VR_off_Med_scdw, &b_VR_off_Med_scdw);
   fChain->SetBranchAddress("VR_off_Med_scup", &VR_off_Med_scup, &b_VR_off_Med_scup);
   fChain->SetBranchAddress("VR_off_High_qcdw", &VR_off_High_qcdw, &b_VR_off_High_qcdw);
   fChain->SetBranchAddress("VR_off_High_qcup", &VR_off_High_qcup, &b_VR_off_High_qcup);
   fChain->SetBranchAddress("VR_off_High_scdw", &VR_off_High_scdw, &b_VR_off_High_scdw);
   fChain->SetBranchAddress("VR_off_High_scup", &VR_off_High_scup, &b_VR_off_High_scup);
   fChain->SetBranchAddress("VR_on_Low_qcdw", &VR_on_Low_qcdw, &b_VR_on_Low_qcdw);
   fChain->SetBranchAddress("VR_on_Low_qcup", &VR_on_Low_qcup, &b_VR_on_Low_qcup);
   fChain->SetBranchAddress("VR_on_Low_scdw", &VR_on_Low_scdw, &b_VR_on_Low_scdw);
   fChain->SetBranchAddress("VR_on_Low_scup", &VR_on_Low_scup, &b_VR_on_Low_scup);
   fChain->SetBranchAddress("VR_on_Med_qcdw", &VR_on_Med_qcdw, &b_VR_on_Med_qcdw);
   fChain->SetBranchAddress("VR_on_Med_qcup", &VR_on_Med_qcup, &b_VR_on_Med_qcup);
   fChain->SetBranchAddress("VR_on_Med_scdw", &VR_on_Med_scdw, &b_VR_on_Med_scdw);
   fChain->SetBranchAddress("VR_on_Med_scup", &VR_on_Med_scup, &b_VR_on_Med_scup);
   fChain->SetBranchAddress("VR_on_High_qcdw", &VR_on_High_qcdw, &b_VR_on_High_qcdw);
   fChain->SetBranchAddress("VR_on_High_qcup", &VR_on_High_qcup, &b_VR_on_High_qcup);
   fChain->SetBranchAddress("VR_on_High_scdw", &VR_on_High_scdw, &b_VR_on_High_scdw);
   fChain->SetBranchAddress("VR_on_High_scup", &VR_on_High_scup, &b_VR_on_High_scup);
   fChain->SetBranchAddress("VR_off_Low_radw", &VR_off_Low_radw, &b_VR_off_Low_radw);
   fChain->SetBranchAddress("VR_off_Med_radw", &VR_off_Med_radw, &b_VR_off_Med_radw);
   fChain->SetBranchAddress("VR_off_High_radw", &VR_off_High_radw, &b_VR_off_High_radw);
   fChain->SetBranchAddress("SR_h_Low_radw", &SR_h_Low_radw, &b_SR_h_Low_radw);
   fChain->SetBranchAddress("SR_h_Med_radw", &SR_h_Med_radw, &b_SR_h_Med_radw);
   fChain->SetBranchAddress("SR_h_High_radw", &SR_h_High_radw, &b_SR_h_High_radw);
   fChain->SetBranchAddress("VR_on_Low_radw", &VR_on_Low_radw, &b_VR_on_Low_radw);
   fChain->SetBranchAddress("VR_on_Med_radw", &VR_on_Med_radw, &b_VR_on_Med_radw);
   fChain->SetBranchAddress("VR_on_High_radw", &VR_on_High_radw, &b_VR_on_High_radw);
   fChain->SetBranchAddress("VR_off_Low_raup", &VR_off_Low_raup, &b_VR_off_Low_raup);
   fChain->SetBranchAddress("VR_off_Med_raup", &VR_off_Med_raup, &b_VR_off_Med_raup);
   fChain->SetBranchAddress("VR_off_High_raup", &VR_off_High_raup, &b_VR_off_High_raup);
   fChain->SetBranchAddress("SR_h_Low_raup", &SR_h_Low_raup, &b_SR_h_Low_raup);
   fChain->SetBranchAddress("SR_h_Med_raup", &SR_h_Med_raup, &b_SR_h_Med_raup);
   fChain->SetBranchAddress("SR_h_High_raup", &SR_h_High_raup, &b_SR_h_High_raup);
   fChain->SetBranchAddress("VR_on_Low_raup", &VR_on_Low_raup, &b_VR_on_Low_raup);
   fChain->SetBranchAddress("VR_on_Med_raup", &VR_on_Med_raup, &b_VR_on_Med_raup);
   fChain->SetBranchAddress("VR_on_High_raup", &VR_on_High_raup, &b_VR_on_High_raup);
   Notify();
}

Bool_t selector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void selector::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t selector::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef selector_cxx
