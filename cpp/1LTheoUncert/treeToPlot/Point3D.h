#pragma once
#include <vector>

class Point3D{
	public:
		Point3D(double X, double Y, double Z){
			this->_X = X;
			this->_Y = Y;
			this->_Z = Z;
		}

		Point3D(){};

		void setPoint(double X, double Y, double Z){
			this->_X = X;
			this->_Y = Y;
			this->_Z = Z;
		}

		double X(){
			return _X;
		}

		double Y(){
			return _Y;
		}

		double Z(){
			return _Z;
		}

		double getDistance(Point3D P){
			double Xdis = this->_X - P.X();
			double Ydis = this->_Y - P.Y();
			double Zdis = this->_Z - P.Z();
			return (Xdis * Xdis + Ydis * Ydis + Zdis *Zdis);
		}

		double getXYDistance(Point3D P){
			double Xdis = this->_X - P.X();
			double Ydis = this->_Y - P.Y();
			return (Xdis * Xdis + Ydis * Ydis);
		}

		// Get the point with the non-0 minimal distance and not the same Z
		Point3D getMinXYDistancePointNo0(std::vector<Point3D> Ps){
			double minDis = 1e99;
			Point3D minP;
			for(auto P : Ps){
				double dis = getXYDistance(P);
				if(dis < minDis && dis != 0 && this->_Z != P.Z()){
					minDis = dis;
					minP = P;
				}
			}
			return minP;
		}

		
	private:
		double _X, _Y, _Z;
	
};

