#pragma once
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed May  1 00:14:48 2019 by ROOT version 6.10/06
// from TTree syst/syst
// found on file: ../summary/syst.root
//////////////////////////////////////////////////////////

#ifndef selector_h
#define selector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

using namespace std;

// Header file for the classes stored in the TTree if any.

class selector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           mcChannelNumber;
   Double_t        SR_h_Low_py1dw;
   Double_t        SR_h_Low_py1up;
   Double_t        SR_h_Low_py2dw;
   Double_t        SR_h_Low_py2up;
   Double_t        SR_h_Low_py3adw;
   Double_t        SR_h_Low_py3aup;
   Double_t        SR_h_Low_py3bdw;
   Double_t        SR_h_Low_py3bup;
   Double_t        SR_h_Low_py3cdw;
   Double_t        SR_h_Low_py3cup;
   Double_t        SR_h_Low_qcdw;
   Double_t        SR_h_Low_qcup;
   Double_t        SR_h_Low_scdw;
   Double_t        SR_h_Low_scup;
   Double_t        SR_h_Med_py1dw;
   Double_t        SR_h_Med_py1up;
   Double_t        SR_h_Med_py2dw;
   Double_t        SR_h_Med_py2up;
   Double_t        SR_h_Med_py3adw;
   Double_t        SR_h_Med_py3aup;
   Double_t        SR_h_Med_py3bdw;
   Double_t        SR_h_Med_py3bup;
   Double_t        SR_h_Med_py3cdw;
   Double_t        SR_h_Med_py3cup;
   Double_t        SR_h_Med_qcdw;
   Double_t        SR_h_Med_qcup;
   Double_t        SR_h_Med_scdw;
   Double_t        SR_h_Med_scup;
   Double_t        SR_h_High_py1dw;
   Double_t        SR_h_High_py1up;
   Double_t        SR_h_High_py2dw;
   Double_t        SR_h_High_py2up;
   Double_t        SR_h_High_py3adw;
   Double_t        SR_h_High_py3aup;
   Double_t        SR_h_High_py3bdw;
   Double_t        SR_h_High_py3bup;
   Double_t        SR_h_High_py3cdw;
   Double_t        SR_h_High_py3cup;
   Double_t        SR_h_High_qcdw;
   Double_t        SR_h_High_qcup;
   Double_t        SR_h_High_scdw;
   Double_t        SR_h_High_scup;
   Double_t        VR_off_Low_py1dw;
   Double_t        VR_off_Low_py1up;
   Double_t        VR_off_Low_py2dw;
   Double_t        VR_off_Low_py2up;
   Double_t        VR_off_Low_py3adw;
   Double_t        VR_off_Low_py3aup;
   Double_t        VR_off_Low_py3bdw;
   Double_t        VR_off_Low_py3bup;
   Double_t        VR_off_Low_py3cdw;
   Double_t        VR_off_Low_py3cup;
   Double_t        VR_off_Low_qcdw;
   Double_t        VR_off_Low_qcup;
   Double_t        VR_off_Low_scdw;
   Double_t        VR_off_Low_scup;
   Double_t        VR_off_Med_py1dw;
   Double_t        VR_off_Med_py1up;
   Double_t        VR_off_Med_py2dw;
   Double_t        VR_off_Med_py2up;
   Double_t        VR_off_Med_py3adw;
   Double_t        VR_off_Med_py3aup;
   Double_t        VR_off_Med_py3bdw;
   Double_t        VR_off_Med_py3bup;
   Double_t        VR_off_Med_py3cdw;
   Double_t        VR_off_Med_py3cup;
   Double_t        VR_off_Med_qcdw;
   Double_t        VR_off_Med_qcup;
   Double_t        VR_off_Med_scdw;
   Double_t        VR_off_Med_scup;
   Double_t        VR_off_High_py1dw;
   Double_t        VR_off_High_py1up;
   Double_t        VR_off_High_py2dw;
   Double_t        VR_off_High_py2up;
   Double_t        VR_off_High_py3adw;
   Double_t        VR_off_High_py3aup;
   Double_t        VR_off_High_py3bdw;
   Double_t        VR_off_High_py3bup;
   Double_t        VR_off_High_py3cdw;
   Double_t        VR_off_High_py3cup;
   Double_t        VR_off_High_qcdw;
   Double_t        VR_off_High_qcup;
   Double_t        VR_off_High_scdw;
   Double_t        VR_off_High_scup;
   Double_t        VR_on_Low_py1dw;
   Double_t        VR_on_Low_py1up;
   Double_t        VR_on_Low_py2dw;
   Double_t        VR_on_Low_py2up;
   Double_t        VR_on_Low_py3adw;
   Double_t        VR_on_Low_py3aup;
   Double_t        VR_on_Low_py3bdw;
   Double_t        VR_on_Low_py3bup;
   Double_t        VR_on_Low_py3cdw;
   Double_t        VR_on_Low_py3cup;
   Double_t        VR_on_Low_qcdw;
   Double_t        VR_on_Low_qcup;
   Double_t        VR_on_Low_scdw;
   Double_t        VR_on_Low_scup;
   Double_t        VR_on_Med_py1dw;
   Double_t        VR_on_Med_py1up;
   Double_t        VR_on_Med_py2dw;
   Double_t        VR_on_Med_py2up;
   Double_t        VR_on_Med_py3adw;
   Double_t        VR_on_Med_py3aup;
   Double_t        VR_on_Med_py3bdw;
   Double_t        VR_on_Med_py3bup;
   Double_t        VR_on_Med_py3cdw;
   Double_t        VR_on_Med_py3cup;
   Double_t        VR_on_Med_qcdw;
   Double_t        VR_on_Med_qcup;
   Double_t        VR_on_Med_scdw;
   Double_t        VR_on_Med_scup;
   Double_t        VR_on_High_py1dw;
   Double_t        VR_on_High_py1up;
   Double_t        VR_on_High_py2dw;
   Double_t        VR_on_High_py2up;
   Double_t        VR_on_High_py3adw;
   Double_t        VR_on_High_py3aup;
   Double_t        VR_on_High_py3bdw;
   Double_t        VR_on_High_py3bup;
   Double_t        VR_on_High_py3cdw;
   Double_t        VR_on_High_py3cup;
   Double_t        VR_on_High_qcdw;
   Double_t        VR_on_High_qcup;
   Double_t        VR_on_High_scdw;
   Double_t        VR_on_High_scup;

   // List of branches
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_SR_h_Low_py1dw;   //!
   TBranch        *b_SR_h_Low_py1up;   //!
   TBranch        *b_SR_h_Low_py2dw;   //!
   TBranch        *b_SR_h_Low_py2up;   //!
   TBranch        *b_SR_h_Low_py3adw;   //!
   TBranch        *b_SR_h_Low_py3aup;   //!
   TBranch        *b_SR_h_Low_py3bdw;   //!
   TBranch        *b_SR_h_Low_py3bup;   //!
   TBranch        *b_SR_h_Low_py3cdw;   //!
   TBranch        *b_SR_h_Low_py3cup;   //!
   TBranch        *b_SR_h_Low_qcdw;   //!
   TBranch        *b_SR_h_Low_qcup;   //!
   TBranch        *b_SR_h_Low_scdw;   //!
   TBranch        *b_SR_h_Low_scup;   //!
   TBranch        *b_SR_h_Med_py1dw;   //!
   TBranch        *b_SR_h_Med_py1up;   //!
   TBranch        *b_SR_h_Med_py2dw;   //!
   TBranch        *b_SR_h_Med_py2up;   //!
   TBranch        *b_SR_h_Med_py3adw;   //!
   TBranch        *b_SR_h_Med_py3aup;   //!
   TBranch        *b_SR_h_Med_py3bdw;   //!
   TBranch        *b_SR_h_Med_py3bup;   //!
   TBranch        *b_SR_h_Med_py3cdw;   //!
   TBranch        *b_SR_h_Med_py3cup;   //!
   TBranch        *b_SR_h_Med_qcdw;   //!
   TBranch        *b_SR_h_Med_qcup;   //!
   TBranch        *b_SR_h_Med_scdw;   //!
   TBranch        *b_SR_h_Med_scup;   //!
   TBranch        *b_SR_h_High_py1dw;   //!
   TBranch        *b_SR_h_High_py1up;   //!
   TBranch        *b_SR_h_High_py2dw;   //!
   TBranch        *b_SR_h_High_py2up;   //!
   TBranch        *b_SR_h_High_py3adw;   //!
   TBranch        *b_SR_h_High_py3aup;   //!
   TBranch        *b_SR_h_High_py3bdw;   //!
   TBranch        *b_SR_h_High_py3bup;   //!
   TBranch        *b_SR_h_High_py3cdw;   //!
   TBranch        *b_SR_h_High_py3cup;   //!
   TBranch        *b_SR_h_High_qcdw;   //!
   TBranch        *b_SR_h_High_qcup;   //!
   TBranch        *b_SR_h_High_scdw;   //!
   TBranch        *b_SR_h_High_scup;   //!
   TBranch        *b_VR_off_Low_py1dw;   //!
   TBranch        *b_VR_off_Low_py1up;   //!
   TBranch        *b_VR_off_Low_py2dw;   //!
   TBranch        *b_VR_off_Low_py2up;   //!
   TBranch        *b_VR_off_Low_py3adw;   //!
   TBranch        *b_VR_off_Low_py3aup;   //!
   TBranch        *b_VR_off_Low_py3bdw;   //!
   TBranch        *b_VR_off_Low_py3bup;   //!
   TBranch        *b_VR_off_Low_py3cdw;   //!
   TBranch        *b_VR_off_Low_py3cup;   //!
   TBranch        *b_VR_off_Low_qcdw;   //!
   TBranch        *b_VR_off_Low_qcup;   //!
   TBranch        *b_VR_off_Low_scdw;   //!
   TBranch        *b_VR_off_Low_scup;   //!
   TBranch        *b_VR_off_Med_py1dw;   //!
   TBranch        *b_VR_off_Med_py1up;   //!
   TBranch        *b_VR_off_Med_py2dw;   //!
   TBranch        *b_VR_off_Med_py2up;   //!
   TBranch        *b_VR_off_Med_py3adw;   //!
   TBranch        *b_VR_off_Med_py3aup;   //!
   TBranch        *b_VR_off_Med_py3bdw;   //!
   TBranch        *b_VR_off_Med_py3bup;   //!
   TBranch        *b_VR_off_Med_py3cdw;   //!
   TBranch        *b_VR_off_Med_py3cup;   //!
   TBranch        *b_VR_off_Med_qcdw;   //!
   TBranch        *b_VR_off_Med_qcup;   //!
   TBranch        *b_VR_off_Med_scdw;   //!
   TBranch        *b_VR_off_Med_scup;   //!
   TBranch        *b_VR_off_High_py1dw;   //!
   TBranch        *b_VR_off_High_py1up;   //!
   TBranch        *b_VR_off_High_py2dw;   //!
   TBranch        *b_VR_off_High_py2up;   //!
   TBranch        *b_VR_off_High_py3adw;   //!
   TBranch        *b_VR_off_High_py3aup;   //!
   TBranch        *b_VR_off_High_py3bdw;   //!
   TBranch        *b_VR_off_High_py3bup;   //!
   TBranch        *b_VR_off_High_py3cdw;   //!
   TBranch        *b_VR_off_High_py3cup;   //!
   TBranch        *b_VR_off_High_qcdw;   //!
   TBranch        *b_VR_off_High_qcup;   //!
   TBranch        *b_VR_off_High_scdw;   //!
   TBranch        *b_VR_off_High_scup;   //!
   TBranch        *b_VR_on_Low_py1dw;   //!
   TBranch        *b_VR_on_Low_py1up;   //!
   TBranch        *b_VR_on_Low_py2dw;   //!
   TBranch        *b_VR_on_Low_py2up;   //!
   TBranch        *b_VR_on_Low_py3adw;   //!
   TBranch        *b_VR_on_Low_py3aup;   //!
   TBranch        *b_VR_on_Low_py3bdw;   //!
   TBranch        *b_VR_on_Low_py3bup;   //!
   TBranch        *b_VR_on_Low_py3cdw;   //!
   TBranch        *b_VR_on_Low_py3cup;   //!
   TBranch        *b_VR_on_Low_qcdw;   //!
   TBranch        *b_VR_on_Low_qcup;   //!
   TBranch        *b_VR_on_Low_scdw;   //!
   TBranch        *b_VR_on_Low_scup;   //!
   TBranch        *b_VR_on_Med_py1dw;   //!
   TBranch        *b_VR_on_Med_py1up;   //!
   TBranch        *b_VR_on_Med_py2dw;   //!
   TBranch        *b_VR_on_Med_py2up;   //!
   TBranch        *b_VR_on_Med_py3adw;   //!
   TBranch        *b_VR_on_Med_py3aup;   //!
   TBranch        *b_VR_on_Med_py3bdw;   //!
   TBranch        *b_VR_on_Med_py3bup;   //!
   TBranch        *b_VR_on_Med_py3cdw;   //!
   TBranch        *b_VR_on_Med_py3cup;   //!
   TBranch        *b_VR_on_Med_qcdw;   //!
   TBranch        *b_VR_on_Med_qcup;   //!
   TBranch        *b_VR_on_Med_scdw;   //!
   TBranch        *b_VR_on_Med_scup;   //!
   TBranch        *b_VR_on_High_py1dw;   //!
   TBranch        *b_VR_on_High_py1up;   //!
   TBranch        *b_VR_on_High_py2dw;   //!
   TBranch        *b_VR_on_High_py2up;   //!
   TBranch        *b_VR_on_High_py3adw;   //!
   TBranch        *b_VR_on_High_py3aup;   //!
   TBranch        *b_VR_on_High_py3bdw;   //!
   TBranch        *b_VR_on_High_py3bup;   //!
   TBranch        *b_VR_on_High_py3cdw;   //!
   TBranch        *b_VR_on_High_py3cup;   //!
   TBranch        *b_VR_on_High_qcdw;   //!
   TBranch        *b_VR_on_High_qcup;   //!
   TBranch        *b_VR_on_High_scdw;   //!
   TBranch        *b_VR_on_High_scup;   //!

   selector(TTree *tree=0);
   virtual ~selector();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef selector_cxx
selector::selector(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("../summary/syst.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("../summary/syst.root");
      }
      f->GetObject("syst",tree);

   }
   Init(tree);
}

selector::~selector()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t selector::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t selector::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void selector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("SR_h_Low_py1dw", &SR_h_Low_py1dw, &b_SR_h_Low_py1dw);
   fChain->SetBranchAddress("SR_h_Low_py1up", &SR_h_Low_py1up, &b_SR_h_Low_py1up);
   fChain->SetBranchAddress("SR_h_Low_py2dw", &SR_h_Low_py2dw, &b_SR_h_Low_py2dw);
   fChain->SetBranchAddress("SR_h_Low_py2up", &SR_h_Low_py2up, &b_SR_h_Low_py2up);
   fChain->SetBranchAddress("SR_h_Low_py3adw", &SR_h_Low_py3adw, &b_SR_h_Low_py3adw);
   fChain->SetBranchAddress("SR_h_Low_py3aup", &SR_h_Low_py3aup, &b_SR_h_Low_py3aup);
   fChain->SetBranchAddress("SR_h_Low_py3bdw", &SR_h_Low_py3bdw, &b_SR_h_Low_py3bdw);
   fChain->SetBranchAddress("SR_h_Low_py3bup", &SR_h_Low_py3bup, &b_SR_h_Low_py3bup);
   fChain->SetBranchAddress("SR_h_Low_py3cdw", &SR_h_Low_py3cdw, &b_SR_h_Low_py3cdw);
   fChain->SetBranchAddress("SR_h_Low_py3cup", &SR_h_Low_py3cup, &b_SR_h_Low_py3cup);
   fChain->SetBranchAddress("SR_h_Low_qcdw", &SR_h_Low_qcdw, &b_SR_h_Low_qcdw);
   fChain->SetBranchAddress("SR_h_Low_qcup", &SR_h_Low_qcup, &b_SR_h_Low_qcup);
   fChain->SetBranchAddress("SR_h_Low_scdw", &SR_h_Low_scdw, &b_SR_h_Low_scdw);
   fChain->SetBranchAddress("SR_h_Low_scup", &SR_h_Low_scup, &b_SR_h_Low_scup);
   fChain->SetBranchAddress("SR_h_Med_py1dw", &SR_h_Med_py1dw, &b_SR_h_Med_py1dw);
   fChain->SetBranchAddress("SR_h_Med_py1up", &SR_h_Med_py1up, &b_SR_h_Med_py1up);
   fChain->SetBranchAddress("SR_h_Med_py2dw", &SR_h_Med_py2dw, &b_SR_h_Med_py2dw);
   fChain->SetBranchAddress("SR_h_Med_py2up", &SR_h_Med_py2up, &b_SR_h_Med_py2up);
   fChain->SetBranchAddress("SR_h_Med_py3adw", &SR_h_Med_py3adw, &b_SR_h_Med_py3adw);
   fChain->SetBranchAddress("SR_h_Med_py3aup", &SR_h_Med_py3aup, &b_SR_h_Med_py3aup);
   fChain->SetBranchAddress("SR_h_Med_py3bdw", &SR_h_Med_py3bdw, &b_SR_h_Med_py3bdw);
   fChain->SetBranchAddress("SR_h_Med_py3bup", &SR_h_Med_py3bup, &b_SR_h_Med_py3bup);
   fChain->SetBranchAddress("SR_h_Med_py3cdw", &SR_h_Med_py3cdw, &b_SR_h_Med_py3cdw);
   fChain->SetBranchAddress("SR_h_Med_py3cup", &SR_h_Med_py3cup, &b_SR_h_Med_py3cup);
   fChain->SetBranchAddress("SR_h_Med_qcdw", &SR_h_Med_qcdw, &b_SR_h_Med_qcdw);
   fChain->SetBranchAddress("SR_h_Med_qcup", &SR_h_Med_qcup, &b_SR_h_Med_qcup);
   fChain->SetBranchAddress("SR_h_Med_scdw", &SR_h_Med_scdw, &b_SR_h_Med_scdw);
   fChain->SetBranchAddress("SR_h_Med_scup", &SR_h_Med_scup, &b_SR_h_Med_scup);
   fChain->SetBranchAddress("SR_h_High_py1dw", &SR_h_High_py1dw, &b_SR_h_High_py1dw);
   fChain->SetBranchAddress("SR_h_High_py1up", &SR_h_High_py1up, &b_SR_h_High_py1up);
   fChain->SetBranchAddress("SR_h_High_py2dw", &SR_h_High_py2dw, &b_SR_h_High_py2dw);
   fChain->SetBranchAddress("SR_h_High_py2up", &SR_h_High_py2up, &b_SR_h_High_py2up);
   fChain->SetBranchAddress("SR_h_High_py3adw", &SR_h_High_py3adw, &b_SR_h_High_py3adw);
   fChain->SetBranchAddress("SR_h_High_py3aup", &SR_h_High_py3aup, &b_SR_h_High_py3aup);
   fChain->SetBranchAddress("SR_h_High_py3bdw", &SR_h_High_py3bdw, &b_SR_h_High_py3bdw);
   fChain->SetBranchAddress("SR_h_High_py3bup", &SR_h_High_py3bup, &b_SR_h_High_py3bup);
   fChain->SetBranchAddress("SR_h_High_py3cdw", &SR_h_High_py3cdw, &b_SR_h_High_py3cdw);
   fChain->SetBranchAddress("SR_h_High_py3cup", &SR_h_High_py3cup, &b_SR_h_High_py3cup);
   fChain->SetBranchAddress("SR_h_High_qcdw", &SR_h_High_qcdw, &b_SR_h_High_qcdw);
   fChain->SetBranchAddress("SR_h_High_qcup", &SR_h_High_qcup, &b_SR_h_High_qcup);
   fChain->SetBranchAddress("SR_h_High_scdw", &SR_h_High_scdw, &b_SR_h_High_scdw);
   fChain->SetBranchAddress("SR_h_High_scup", &SR_h_High_scup, &b_SR_h_High_scup);
   fChain->SetBranchAddress("VR_off_Low_py1dw", &VR_off_Low_py1dw, &b_VR_off_Low_py1dw);
   fChain->SetBranchAddress("VR_off_Low_py1up", &VR_off_Low_py1up, &b_VR_off_Low_py1up);
   fChain->SetBranchAddress("VR_off_Low_py2dw", &VR_off_Low_py2dw, &b_VR_off_Low_py2dw);
   fChain->SetBranchAddress("VR_off_Low_py2up", &VR_off_Low_py2up, &b_VR_off_Low_py2up);
   fChain->SetBranchAddress("VR_off_Low_py3adw", &VR_off_Low_py3adw, &b_VR_off_Low_py3adw);
   fChain->SetBranchAddress("VR_off_Low_py3aup", &VR_off_Low_py3aup, &b_VR_off_Low_py3aup);
   fChain->SetBranchAddress("VR_off_Low_py3bdw", &VR_off_Low_py3bdw, &b_VR_off_Low_py3bdw);
   fChain->SetBranchAddress("VR_off_Low_py3bup", &VR_off_Low_py3bup, &b_VR_off_Low_py3bup);
   fChain->SetBranchAddress("VR_off_Low_py3cdw", &VR_off_Low_py3cdw, &b_VR_off_Low_py3cdw);
   fChain->SetBranchAddress("VR_off_Low_py3cup", &VR_off_Low_py3cup, &b_VR_off_Low_py3cup);
   fChain->SetBranchAddress("VR_off_Low_qcdw", &VR_off_Low_qcdw, &b_VR_off_Low_qcdw);
   fChain->SetBranchAddress("VR_off_Low_qcup", &VR_off_Low_qcup, &b_VR_off_Low_qcup);
   fChain->SetBranchAddress("VR_off_Low_scdw", &VR_off_Low_scdw, &b_VR_off_Low_scdw);
   fChain->SetBranchAddress("VR_off_Low_scup", &VR_off_Low_scup, &b_VR_off_Low_scup);
   fChain->SetBranchAddress("VR_off_Med_py1dw", &VR_off_Med_py1dw, &b_VR_off_Med_py1dw);
   fChain->SetBranchAddress("VR_off_Med_py1up", &VR_off_Med_py1up, &b_VR_off_Med_py1up);
   fChain->SetBranchAddress("VR_off_Med_py2dw", &VR_off_Med_py2dw, &b_VR_off_Med_py2dw);
   fChain->SetBranchAddress("VR_off_Med_py2up", &VR_off_Med_py2up, &b_VR_off_Med_py2up);
   fChain->SetBranchAddress("VR_off_Med_py3adw", &VR_off_Med_py3adw, &b_VR_off_Med_py3adw);
   fChain->SetBranchAddress("VR_off_Med_py3aup", &VR_off_Med_py3aup, &b_VR_off_Med_py3aup);
   fChain->SetBranchAddress("VR_off_Med_py3bdw", &VR_off_Med_py3bdw, &b_VR_off_Med_py3bdw);
   fChain->SetBranchAddress("VR_off_Med_py3bup", &VR_off_Med_py3bup, &b_VR_off_Med_py3bup);
   fChain->SetBranchAddress("VR_off_Med_py3cdw", &VR_off_Med_py3cdw, &b_VR_off_Med_py3cdw);
   fChain->SetBranchAddress("VR_off_Med_py3cup", &VR_off_Med_py3cup, &b_VR_off_Med_py3cup);
   fChain->SetBranchAddress("VR_off_Med_qcdw", &VR_off_Med_qcdw, &b_VR_off_Med_qcdw);
   fChain->SetBranchAddress("VR_off_Med_qcup", &VR_off_Med_qcup, &b_VR_off_Med_qcup);
   fChain->SetBranchAddress("VR_off_Med_scdw", &VR_off_Med_scdw, &b_VR_off_Med_scdw);
   fChain->SetBranchAddress("VR_off_Med_scup", &VR_off_Med_scup, &b_VR_off_Med_scup);
   fChain->SetBranchAddress("VR_off_High_py1dw", &VR_off_High_py1dw, &b_VR_off_High_py1dw);
   fChain->SetBranchAddress("VR_off_High_py1up", &VR_off_High_py1up, &b_VR_off_High_py1up);
   fChain->SetBranchAddress("VR_off_High_py2dw", &VR_off_High_py2dw, &b_VR_off_High_py2dw);
   fChain->SetBranchAddress("VR_off_High_py2up", &VR_off_High_py2up, &b_VR_off_High_py2up);
   fChain->SetBranchAddress("VR_off_High_py3adw", &VR_off_High_py3adw, &b_VR_off_High_py3adw);
   fChain->SetBranchAddress("VR_off_High_py3aup", &VR_off_High_py3aup, &b_VR_off_High_py3aup);
   fChain->SetBranchAddress("VR_off_High_py3bdw", &VR_off_High_py3bdw, &b_VR_off_High_py3bdw);
   fChain->SetBranchAddress("VR_off_High_py3bup", &VR_off_High_py3bup, &b_VR_off_High_py3bup);
   fChain->SetBranchAddress("VR_off_High_py3cdw", &VR_off_High_py3cdw, &b_VR_off_High_py3cdw);
   fChain->SetBranchAddress("VR_off_High_py3cup", &VR_off_High_py3cup, &b_VR_off_High_py3cup);
   fChain->SetBranchAddress("VR_off_High_qcdw", &VR_off_High_qcdw, &b_VR_off_High_qcdw);
   fChain->SetBranchAddress("VR_off_High_qcup", &VR_off_High_qcup, &b_VR_off_High_qcup);
   fChain->SetBranchAddress("VR_off_High_scdw", &VR_off_High_scdw, &b_VR_off_High_scdw);
   fChain->SetBranchAddress("VR_off_High_scup", &VR_off_High_scup, &b_VR_off_High_scup);
   fChain->SetBranchAddress("VR_on_Low_py1dw", &VR_on_Low_py1dw, &b_VR_on_Low_py1dw);
   fChain->SetBranchAddress("VR_on_Low_py1up", &VR_on_Low_py1up, &b_VR_on_Low_py1up);
   fChain->SetBranchAddress("VR_on_Low_py2dw", &VR_on_Low_py2dw, &b_VR_on_Low_py2dw);
   fChain->SetBranchAddress("VR_on_Low_py2up", &VR_on_Low_py2up, &b_VR_on_Low_py2up);
   fChain->SetBranchAddress("VR_on_Low_py3adw", &VR_on_Low_py3adw, &b_VR_on_Low_py3adw);
   fChain->SetBranchAddress("VR_on_Low_py3aup", &VR_on_Low_py3aup, &b_VR_on_Low_py3aup);
   fChain->SetBranchAddress("VR_on_Low_py3bdw", &VR_on_Low_py3bdw, &b_VR_on_Low_py3bdw);
   fChain->SetBranchAddress("VR_on_Low_py3bup", &VR_on_Low_py3bup, &b_VR_on_Low_py3bup);
   fChain->SetBranchAddress("VR_on_Low_py3cdw", &VR_on_Low_py3cdw, &b_VR_on_Low_py3cdw);
   fChain->SetBranchAddress("VR_on_Low_py3cup", &VR_on_Low_py3cup, &b_VR_on_Low_py3cup);
   fChain->SetBranchAddress("VR_on_Low_qcdw", &VR_on_Low_qcdw, &b_VR_on_Low_qcdw);
   fChain->SetBranchAddress("VR_on_Low_qcup", &VR_on_Low_qcup, &b_VR_on_Low_qcup);
   fChain->SetBranchAddress("VR_on_Low_scdw", &VR_on_Low_scdw, &b_VR_on_Low_scdw);
   fChain->SetBranchAddress("VR_on_Low_scup", &VR_on_Low_scup, &b_VR_on_Low_scup);
   fChain->SetBranchAddress("VR_on_Med_py1dw", &VR_on_Med_py1dw, &b_VR_on_Med_py1dw);
   fChain->SetBranchAddress("VR_on_Med_py1up", &VR_on_Med_py1up, &b_VR_on_Med_py1up);
   fChain->SetBranchAddress("VR_on_Med_py2dw", &VR_on_Med_py2dw, &b_VR_on_Med_py2dw);
   fChain->SetBranchAddress("VR_on_Med_py2up", &VR_on_Med_py2up, &b_VR_on_Med_py2up);
   fChain->SetBranchAddress("VR_on_Med_py3adw", &VR_on_Med_py3adw, &b_VR_on_Med_py3adw);
   fChain->SetBranchAddress("VR_on_Med_py3aup", &VR_on_Med_py3aup, &b_VR_on_Med_py3aup);
   fChain->SetBranchAddress("VR_on_Med_py3bdw", &VR_on_Med_py3bdw, &b_VR_on_Med_py3bdw);
   fChain->SetBranchAddress("VR_on_Med_py3bup", &VR_on_Med_py3bup, &b_VR_on_Med_py3bup);
   fChain->SetBranchAddress("VR_on_Med_py3cdw", &VR_on_Med_py3cdw, &b_VR_on_Med_py3cdw);
   fChain->SetBranchAddress("VR_on_Med_py3cup", &VR_on_Med_py3cup, &b_VR_on_Med_py3cup);
   fChain->SetBranchAddress("VR_on_Med_qcdw", &VR_on_Med_qcdw, &b_VR_on_Med_qcdw);
   fChain->SetBranchAddress("VR_on_Med_qcup", &VR_on_Med_qcup, &b_VR_on_Med_qcup);
   fChain->SetBranchAddress("VR_on_Med_scdw", &VR_on_Med_scdw, &b_VR_on_Med_scdw);
   fChain->SetBranchAddress("VR_on_Med_scup", &VR_on_Med_scup, &b_VR_on_Med_scup);
   fChain->SetBranchAddress("VR_on_High_py1dw", &VR_on_High_py1dw, &b_VR_on_High_py1dw);
   fChain->SetBranchAddress("VR_on_High_py1up", &VR_on_High_py1up, &b_VR_on_High_py1up);
   fChain->SetBranchAddress("VR_on_High_py2dw", &VR_on_High_py2dw, &b_VR_on_High_py2dw);
   fChain->SetBranchAddress("VR_on_High_py2up", &VR_on_High_py2up, &b_VR_on_High_py2up);
   fChain->SetBranchAddress("VR_on_High_py3adw", &VR_on_High_py3adw, &b_VR_on_High_py3adw);
   fChain->SetBranchAddress("VR_on_High_py3aup", &VR_on_High_py3aup, &b_VR_on_High_py3aup);
   fChain->SetBranchAddress("VR_on_High_py3bdw", &VR_on_High_py3bdw, &b_VR_on_High_py3bdw);
   fChain->SetBranchAddress("VR_on_High_py3bup", &VR_on_High_py3bup, &b_VR_on_High_py3bup);
   fChain->SetBranchAddress("VR_on_High_py3cdw", &VR_on_High_py3cdw, &b_VR_on_High_py3cdw);
   fChain->SetBranchAddress("VR_on_High_py3cup", &VR_on_High_py3cup, &b_VR_on_High_py3cup);
   fChain->SetBranchAddress("VR_on_High_qcdw", &VR_on_High_qcdw, &b_VR_on_High_qcdw);
   fChain->SetBranchAddress("VR_on_High_qcup", &VR_on_High_qcup, &b_VR_on_High_qcup);
   fChain->SetBranchAddress("VR_on_High_scdw", &VR_on_High_scdw, &b_VR_on_High_scdw);
   fChain->SetBranchAddress("VR_on_High_scup", &VR_on_High_scup, &b_VR_on_High_scup);
   Notify();
}

Bool_t selector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void selector::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t selector::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef selector_cxx
