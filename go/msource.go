package main

import(
	"flag"
	"fmt"
	"sync"
	"strconv"
	"os/exec"
)

func shellWorker(wg *sync.WaitGroup, script string, allArgs chan string){
	defer wg.Done()
	for arg := range allArgs{
		s := "source " + script + " " + arg
		cmd := exec.Command("bash", "-c", s)
		output, err := cmd.CombinedOutput()
		fmt.Println(string(output))
		if err != nil{
			fmt.Println(err)
		}
	}
}

func main(){
	var help bool

	flag.BoolVar(&help, "h", false, "print this message and exit.\nUsage: msource $script $threadNum $prog1arg [$prog2arg $prog3arg ...]. The it will use $threadNum threads to do 'bash $script $prog1arg', 'bash $script $prog2arg', 'bash $script $prog3arg' ...")

	flag.Parse()

	if help || flag.NArg() < 3 {
		flag.Usage()
		return
	}

	script := flag.Args()[0]
	thread,err := strconv.Atoi(flag.Args()[1])

	if err != nil || thread < 1{
		fmt.Printf("\x1b[31;1m%s%s\033[0m\n","The $threadNum must be a int value which > 0. But your input $threadNum is : ", flag.Args()[1])
		//fmt.Println("The $threadNum must be a int value which > 0. But your input $threadNum is :",flag.Args()[1])
		flag.Usage()
		return
	}

	// channel for the worker pool
	inputChan := make(chan string)

	// waitGroup
	wg := new(sync.WaitGroup)

	for i:= 0; i < thread; i++{
		wg.Add(1)
		go shellWorker(wg, script, inputChan)
	}

	// put all progargs into the inputChan and run them
	for i := 2; i < flag.NArg(); i++{
		inputChan <- flag.Args()[i]
	}

	// close channel
	close(inputChan)

	// wait all of the jobs finish
	wg.Wait()
}
